package com.arslanbey.depotakip.web.utility;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;

import com.arslanbey.depotakip.web.entity.Islem;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;

public class ArrayToTable {


public static NumberFormat formatter = new DecimalFormat("#,##0.00");
	
	public ArrayToTable(ArrayList<Islem> islemList, HashMap<String, String> map) throws Exception {
		// TODO Auto-generated constructor stub
		// String timeStamp = new
		// SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		// String fileName = DEST + "stopaj" + ".pdf";
	
		File file = new File(map.get("filePath"));

		if (!file.exists()) {
			file.createNewFile();
		}

		manipulatePdf(AppConstants.RAPOR_DEST_PDF, islemList, map);
        
	}

	public ArrayToTable(ArrayList<Islem> islemList) throws Exception {
		// TODO Auto-generated constructor stub
		// String timeStamp = new
		// SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

		File file = new File(AppConstants.RAPOR_DEST_PDF);
		file.getParentFile().mkdirs();
		manipulatePdf(AppConstants.RAPOR_DEST_PDF, islemList);
	}

	protected void manipulatePdf(String dest, ArrayList<Islem> islemList) throws Exception {
		PdfDocument pdfDoc = new PdfDocument(new PdfWriter(dest));
		Document doc = new Document(pdfDoc);

		PdfFont regular = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);
		PdfFont bold = PdfFontFactory.createFont(FontConstants.TIMES_BOLD);

		Paragraph paragraph = new Paragraph("TORAMANLAR AKARYAKIT OTO LASTİK VE TARIM ÜRÜNLERI ").setFont(bold);
		paragraph.setWidth(1500f);
		paragraph.setTextAlignment(TextAlignment.CENTER);
		doc.add(paragraph);
		doc.add(new Paragraph("TIC. ve SANAYI A.S.").setTextAlignment(TextAlignment.CENTER).setFont(bold));
		doc.add(new Paragraph("01.01.2017 - 01.09.2017 STOPAJ LISTESIDIR").setTextAlignment(TextAlignment.LEFT));

		doc.add(new Paragraph("FINDIK MUSTAHSIL MAKBUZUNUN .. ").setTextAlignment(TextAlignment.LEFT));

		doc.add(new Paragraph(" "));

		Table table1 = new Table(6);
		// table1.setBorder(Border.NO_BORDER);

		table1.setBorder(Border.NO_BORDER);
		table1.addHeaderCell("Tarih").setFont(bold).setFontColor(Color.RED);
		table1.addHeaderCell("Fis No").setFont(bold).setFontColor(Color.RED);
		table1.addHeaderCell("Adi").setFont(bold).setFontColor(Color.RED);
		table1.addHeaderCell("Dogum Yeri").setFont(bold).setFontColor(Color.RED);
		table1.addHeaderCell("Toplam (TL)").setFont(bold).setFontColor(Color.RED);
		table1.addHeaderCell("Vergi (TL)").setFont(bold).setFontColor(Color.RED);

		for (int i = 0; i < islemList.size(); i++) {
			Islem islem = islemList.get(i);

			table1.addCell(new Cell().setFont(regular).add(new Paragraph(islem.getTarih())));
			table1.addCell(new Cell().setFont(regular).add(new Paragraph(islem.getFisNo())));
			table1.addCell(new Cell().setFont(regular).add(new Paragraph(String.valueOf(islem.getMiktar()))));
			table1.addCell(new Cell().setFont(regular).add(new Paragraph(String.valueOf(islem.getMiktar()))));
			table1.addCell(new Cell().setFont(regular).add(new Paragraph(String.valueOf(islem.getTutar()))));
			table1.addCell(new Cell().setFont(regular).add(new Paragraph(String.valueOf(islem.getStopaj()))));

		}
		doc.add(table1);

		doc.close();
	}

	protected void manipulatePdf(String dest, ArrayList<Islem> islemList, HashMap<String, String> map)
			throws Exception {
		dest = map.get("filePath");
		PdfDocument pdfDoc = new PdfDocument(new PdfWriter(dest));
		Document doc = new Document(pdfDoc);

		PdfFont regular = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);
		PdfFont bold = PdfFontFactory.createFont(FontConstants.TIMES_BOLD);

		Paragraph paragraph = new Paragraph("TORAMANLAR AKARYAKIT OTO LASTiK VE TARIM ÜRÜNLERi ").setFont(bold);
		paragraph.setWidth(1500f);
		paragraph.setTextAlignment(TextAlignment.CENTER);
		paragraph.setFontColor(Color.BLUE);
		doc.add(paragraph);
		doc.add(new Paragraph("TIC. ve SANAYI A.S.").setTextAlignment(TextAlignment.CENTER).setFont(bold)
				.setFontColor(Color.BLUE));
		doc.add(new Paragraph(map.get("tarih1") + " - " + map.get("tarih2") + " STOPAJ LiSTESiDiR")
				.setTextAlignment(TextAlignment.CENTER).setFontColor(Color.BLUE));

		doc.add(new Paragraph(map.get("malzeme") + " MUSTAHSIL MAKBUZUNUN .. ").setTextAlignment(TextAlignment.CENTER)
				.setFontColor(Color.BLUE));

		doc.add(new Paragraph(" "));

		Table table1 = new Table(6);
		
		table1.setBorder(Border.NO_BORDER);
		table1.addHeaderCell(new Cell().setBorder(Border.NO_BORDER).setFont(bold).setFontColor(Color.RED).add(new Paragraph("Tarih")));
		table1.addHeaderCell(new Cell().setBorder(Border.NO_BORDER).setFont(bold).setFontColor(Color.RED).add(new Paragraph("Fis No")));
		table1.addHeaderCell(new Cell().setBorder(Border.NO_BORDER).setFont(bold).setFontColor(Color.RED).add(new Paragraph("Adi")));
		table1.addHeaderCell(new Cell().setBorder(Border.NO_BORDER).setFont(bold).setFontColor(Color.RED).add(new Paragraph("Dogum Yeri")));
		table1.addHeaderCell(new Cell().setBorder(Border.NO_BORDER).setFont(bold).setFontColor(Color.RED).add(new Paragraph("Toplam (TL)")));
		table1.addHeaderCell(new Cell().setBorder(Border.NO_BORDER).setFont(bold).setFontColor(Color.RED).add(new Paragraph("Vergi (TL)")));

		table1.addCell(new Cell().setBorder(Border.NO_BORDER).setFont(regular).setFontColor(Color.BLACK).add(new Paragraph(map.get("tarih1"))));
		table1.addCell(new Cell().setBorder(Border.NO_BORDER).setFont(regular).setFontColor(Color.BLACK).add(new Paragraph("DONEMBASI")));
		table1.addCell(new Cell().setBorder(Border.NO_BORDER).setFont(regular).setFontColor(Color.BLACK).add(new Paragraph("---")));
		table1.addCell(new Cell().setBorder(Border.NO_BORDER).setFont(regular).setFontColor(Color.BLACK).add(new Paragraph("---")));
		table1.addCell(new Cell().setBorder(Border.NO_BORDER).setFont(regular).setFontColor(Color.BLACK).add(new Paragraph(String.valueOf(formatter.format(Double.parseDouble(map.get("total")))))));
		table1.addCell(
				new Cell().setBorder(Border.NO_BORDER).setFont(regular).setFontColor(Color.BLACK).add(new Paragraph(String.valueOf(formatter.format(Double.parseDouble(map.get("totalStopaj")))))));

		 //for (int j = 0; j < 10; j++) {
		for (int i = 0; i < islemList.size(); i++) {
			Islem islem = islemList.get(i);

			table1.addCell(new Cell().setBorder(Border.NO_BORDER).setFont(regular).setFontColor(Color.BLACK).add(new Paragraph(islem.getTarih())));
			table1.addCell(new Cell().setBorder(Border.NO_BORDER).setFont(regular).setFontColor(Color.BLACK).add(new Paragraph(islem.getFisNo())));
			table1.addCell(new Cell().setBorder(Border.NO_BORDER).setFont(regular).setFontColor(Color.BLACK).add(new Paragraph(islem.getmAd())));
			table1.addCell(
					new Cell().setBorder(Border.NO_BORDER).setFont(regular).setFontColor(Color.BLACK).add(new Paragraph(islem.getmDogumYeri())));
			table1.addCell(new Cell().setBorder(Border.NO_BORDER).setFont(regular).setFontColor(Color.BLACK)
					.add(new Paragraph(String.valueOf(formatter.format(islem.getTutar())))));
			table1.addCell(new Cell().setBorder(Border.NO_BORDER).setFont(regular).setFontColor(Color.BLACK)
					.add(new Paragraph(String.valueOf(formatter.format(islem.getStopaj())))));

		}

		// }
		doc.add(table1);

		doc.close();
	}
}