/**
 *
 */
package com.arslanbey.depotakip.web.service;

 

import java.sql.SQLException;
import java.util.ArrayList;

import com.arslanbey.depotakip.web.entity.User;

/**
 * @author ARSLAN
 *
 */
public interface UserService {

	public User isValidUser(String username, String password)
			throws SQLException;

	public boolean insertUser(User user) throws SQLException;

	public ArrayList<User> getUserList() throws SQLException;
	
	
	public boolean deleteUser(int userId) throws SQLException;
	
	public boolean editUser(int userId, String username, String name , String surname , int roleid,int depertmanId) throws SQLException;
	
	public boolean editProfile(int userId, String username, String name , String surname ,int depertmanId) throws SQLException;

	public boolean changePassword(int userId,String password) throws SQLException;
	
	public User getUserByUserId(int userId)  throws SQLException;
	
	public ArrayList<User> searchDepartman(int departmentId) throws SQLException;
	
	
	
}
