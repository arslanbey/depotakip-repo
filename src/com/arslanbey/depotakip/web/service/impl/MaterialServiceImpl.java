package com.arslanbey.depotakip.web.service.impl;

 

import java.sql.SQLException;
import java.util.ArrayList;

import com.arslanbey.depotakip.web.dao.MaterialDao;
import com.arslanbey.depotakip.web.entity.Material;
import com.arslanbey.depotakip.web.service.MaterialService;

public class MaterialServiceImpl implements MaterialService {

	private MaterialDao materialDao;

	public MaterialDao getMaterialDao() {
		return materialDao;
	}

	public void setMaterialDao(MaterialDao materialDao) {
		this.materialDao = materialDao;
	}

	@Override
	public Material getMaterialById(int materialId) throws SQLException {
		// TODO Auto-generated method stub
		return materialDao.getMaterialById(materialId);
	}

	@Override
	public ArrayList<Material> getMaterialList() throws SQLException {
		// TODO Auto-generated method stub
		return materialDao.getMaterialList();
	}

	@Override
	public boolean insertMaterial(Material material) throws SQLException {
		// TODO Auto-generated method stub
		return materialDao.insertMaterial(material);
	}

	@Override
	public boolean deleteMaterial(int materialId) throws SQLException {
		// TODO Auto-generated method stub
		return materialDao.deleteMaterial(materialId);
	}

	@Override
	public boolean updateMaterial(Material material) throws SQLException {
		// TODO Auto-generated method stub
		return materialDao.updateMaterial(material);
	}

	
	
	
 
}
