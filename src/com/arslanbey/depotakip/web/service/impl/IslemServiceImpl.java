package com.arslanbey.depotakip.web.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;

import com.arslanbey.depotakip.web.dao.IslemDao;
import com.arslanbey.depotakip.web.entity.Islem;
import com.arslanbey.depotakip.web.service.IslemService;

public class IslemServiceImpl implements IslemService {

	IslemDao islemDao;

	public IslemDao getIslemDao() {
		return islemDao;
	}

	public void setIslemDao(IslemDao islemDao) {
		this.islemDao = islemDao;
	}

	@Override
	public boolean insertIslem(Islem islem) throws SQLException {
		// TODO Auto-generated method stub
		return islemDao.insertIslem(islem);
	}

	@Override
	public boolean deleteIslem(int id) throws SQLException {
		// TODO Auto-generated method stub
		return islemDao.deleteIslem(id);
	}

	@Override
	public boolean updateIslem(Islem islem) throws SQLException {
		// TODO Auto-generated method stub
		return islemDao.updateIslem(islem);
	}

	@Override
	public Islem getIslemById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return islemDao.getIslemById(id);
	}

	@Override
	public ArrayList<Islem> getAllIslem() throws SQLException {
		// TODO Auto-generated method stub
		return islemDao.getAllIslem();
	}

	@Override
	public ArrayList<Islem> getAllIslemByIslemTuru(int islemTuru) throws SQLException {
		// TODO Auto-generated method stub
		return islemDao.getAllIslemByIslemTuru(islemTuru);
	}

	@Override
	public ArrayList<Islem> searchIslem(String tarih1, String tarih2, int musteriId, int materialId, int companyId)
			throws SQLException {
		// TODO Auto-generated method stub
		return islemDao.searchIslem(tarih1, tarih2, musteriId, materialId, companyId);
	}

}
