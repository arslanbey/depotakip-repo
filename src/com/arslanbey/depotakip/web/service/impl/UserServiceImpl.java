package com.arslanbey.depotakip.web.service.impl;

 

import java.sql.SQLException;
import java.util.ArrayList;

import com.arslanbey.depotakip.web.dao.UserDao;
import com.arslanbey.depotakip.web.entity.User;
import com.arslanbey.depotakip.web.service.UserService;

public class UserServiceImpl implements UserService {

	private UserDao userDao;

	public UserDao getUserDao() {
		return this.userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public User isValidUser(String username, String password)
			throws SQLException {
		return userDao.isValidUser(username, password);
	}

	@Override
	public ArrayList<User> getUserList() throws SQLException {
		return userDao.getUserList();
	}
	
	
	@Override
	public boolean insertUser(User user)
			throws SQLException {
		return userDao.insertUser(user);
	}

	@Override
	public boolean deleteUser(int userId) throws SQLException {
		// TODO Auto-generated method stub
		return userDao.deleteUser(userId);
	}
	
	public boolean editUser(int userId, String username, String name , String surname , int roleid,int depertmanId) throws SQLException {
		// TODO Auto-generated method stub
		return userDao.editUser(userId, username, name, surname, roleid,depertmanId);
	}
	
	
	public boolean editProfile(int userId, String username, String name , String surname ,int depertmanId) throws SQLException {
		// TODO Auto-generated method stub
		return userDao.editProfile(userId, username, name, surname,depertmanId);
	}
	
	public boolean changePassword(int userId,String password) throws SQLException {
		// TODO Auto-generated method stub
		return userDao.changePassword(userId,password);
	}
	
	public User getUserByUserId(int userId)  throws SQLException{
		
		return userDao.getUserByUserId(userId);
	}
	
	public ArrayList<User> searchDepartman(int departmentId) throws SQLException{
		
		return userDao.searchDepartman(departmentId);
	}
}
