package com.arslanbey.depotakip.web.service.impl;

 

import java.sql.SQLException;
import java.util.ArrayList;

import com.arslanbey.depotakip.web.dao.FNCompanyDao;
import com.arslanbey.depotakip.web.entity.FNCompany;
import com.arslanbey.depotakip.web.service.FNCompanyService;

public class FNCompanyServiceImpl implements FNCompanyService {

	FNCompanyDao fnCompanyDao;
	
	
	
	public FNCompanyDao getFnCompanyDao() {
		return fnCompanyDao;
	}

	public void setFnCompanyDao(FNCompanyDao fnCompanyDao) {
		this.fnCompanyDao = fnCompanyDao;
	}

	
	@Override
	public FNCompany getCompanyById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return fnCompanyDao.getCompanyById(id);
	}

	@Override
	public ArrayList<FNCompany> getCompanyList() throws SQLException {
		// TODO Auto-generated method stub
		return fnCompanyDao.getCompanyList();
	}

	@Override
	public boolean insertCompany(FNCompany fNCompany) throws SQLException {
		// TODO Auto-generated method stub
		return fnCompanyDao.insertCompany(fNCompany);
	}

	@Override
	public boolean deleteCompany(int id) throws SQLException {
		// TODO Auto-generated method stub
		return fnCompanyDao.deleteCompany(id);
	}

	@Override
	public boolean updateCompany(FNCompany fNCompany) throws SQLException {
		// TODO Auto-generated method stub
		return fnCompanyDao.updateCompany(fNCompany);
	}

	 
	
	
	
 
}
