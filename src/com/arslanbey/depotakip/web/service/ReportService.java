/**
 *
 */
package com.arslanbey.depotakip.web.service;

 

import java.sql.SQLException;
import java.util.ArrayList;

import com.arslanbey.depotakip.web.entity.User;


/**
 * @author ARSLAN
 *
 */
public interface ReportService {

	public ArrayList<User> getLastFiveUser() throws SQLException;
	
	
}
