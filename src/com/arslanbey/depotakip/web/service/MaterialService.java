/**
 *
 */
package com.arslanbey.depotakip.web.service;

 

import java.sql.SQLException;
import java.util.ArrayList;

import com.arslanbey.depotakip.web.entity.Material;

/**
 * @author ARSLAN
 *
 */
public interface MaterialService {

	 
	public Material getMaterialById(int materialId) throws SQLException;
	
	public ArrayList<Material> getMaterialList() throws SQLException;
	
	public boolean insertMaterial(Material material) throws SQLException;
	
	public boolean deleteMaterial(int materialId) throws SQLException;
	
	public boolean updateMaterial(Material material) throws SQLException;
	
	
}
