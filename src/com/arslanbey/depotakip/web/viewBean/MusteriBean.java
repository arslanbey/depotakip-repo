package com.arslanbey.depotakip.web.viewBean;

public class MusteriBean {

	private int musteriId;

	private String ad;

	private String babaAdi;

	private String dogumYeri;

	private String koy;

	private String dogumTarihi;
	
	private String hesapNo;

	private String vergiNo;

	private String vergiDaire;

	public MusteriBean() {
		super();
	}

	public int getMusteriId() {
		return musteriId;
	}

	
	public String getAd() {
		return ad;
	}

	public String getBabaAdi() {
		return babaAdi;
	}

	public String getDogumYeri() {
		return dogumYeri;
	}

	public String getKoy() {
		return koy;
	}

	public String getDogumTarihi() {
		return dogumTarihi;
	}

	public String getHesapNo() {
		return hesapNo;
	}

	public String getVergiNo() {
		return vergiNo;
	}

	public String getVergiDaire() {
		return vergiDaire;
	}

	public void setMusteriId(int musteriId) {
		this.musteriId = musteriId;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public void setBabaAdi(String babaAdi) {
		this.babaAdi = babaAdi;
	}

	public void setDogumYeri(String dogumYeri) {
		this.dogumYeri = dogumYeri;
	}

	public void setKoy(String koy) {
		this.koy = koy;
	}

	public void setDogumTarihi(String dogumTarihi) {
		this.dogumTarihi = dogumTarihi;
	}

	public void setHesapNo(String hesapNo) {
		this.hesapNo = hesapNo;
	}

	public void setVergiNo(String vergiNo) {
		this.vergiNo = vergiNo;
	}

	public void setVergiDaire(String vergiDaire) {
		this.vergiDaire = vergiDaire;
	}

	@Override
	public String toString() {
		return "Musteri [id=" + musteriId + ", ad=" + ad + ", babaAdi=" + babaAdi + ", dogumYeri=" + dogumYeri + ", koy=" + koy
				+ ", dogumTarihi=" + dogumTarihi + ", hesapNo=" + hesapNo + ", vergiNo=" + vergiNo + ", vergiDaire="
				+ vergiDaire + "]";
	}

 
}