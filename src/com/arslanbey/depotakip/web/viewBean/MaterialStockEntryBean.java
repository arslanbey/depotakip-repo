package com.arslanbey.depotakip.web.viewBean;

public class MaterialStockEntryBean {
	
 
	private int id;
	
	private String irsaliyeNo;

	private int companyId;
	
	private int materialId;
	
	private double stokMiktari;
	
	private String tarih;
	
	private double randuman;
	
	private double randumanFarki;
	
	private double kilo;

	
	@Override
	public String toString() {
		return "MaterialStockEntryBean [id=" + id + ", irsaliyeNo=" + irsaliyeNo + ", companyId=" + companyId
				+ ", materialId=" + materialId + ", stokMiktari=" + stokMiktari + ", tarih=" + tarih + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIrsaliyeNo() {
		return irsaliyeNo;
	}

	public void setIrsaliyeNo(String irsaliyeNo) {
		this.irsaliyeNo = irsaliyeNo;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getMaterialId() {
		return materialId;
	}

	public void setMaterialId(int materialId) {
		this.materialId = materialId;
	}

	public double getStokMiktari() {
		return stokMiktari;
	}

	public void setStokMiktari(double stokMiktari) {
		this.stokMiktari = stokMiktari;
	}

	public String getTarih() {
		return tarih;
	}

	public void setTarih(String tarih) {
		this.tarih = tarih;
	}

	public double getRanduman() {
		return randuman;
	}

	public double getRandumanFarki() {
		return randumanFarki;
	}

	public double getKilo() {
		return kilo;
	}

	public void setRanduman(double randuman) {
		this.randuman = randuman;
	}

	public void setRandumanFarki(double randumanFarki) {
		this.randumanFarki = randumanFarki;
	}

	public void setKilo(double kilo) {
		this.kilo = kilo;
	}


	
	
	
   
}
