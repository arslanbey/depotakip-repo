package com.arslanbey.depotakip.web.viewBean;

public class WorkOrderBean {

	private int id;

private String tarih;
	
	private String description;

	private int workId;

	private int companyId;

	private int workstatus;

	private double metraj;
	
	



	@Override
	public String toString() {
		return "WorkOrderBean [id=" + id + ", tarih=" + tarih + ", description=" + description + ", workId=" + workId
				+ ", companyId=" + companyId + ", workstatus=" + workstatus + ", metraj=" + metraj + "]";
	}



	public String getTarih() {
		return tarih;
	}



	public void setTarih(String tarih) {
		this.tarih = tarih;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getWorkId() {
		return workId;
	}

	public void setWorkId(int workId) {
		this.workId = workId;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getWorkstatus() {
		return workstatus;
	}

	public void setWorkstatus(int workstatus) {
		this.workstatus = workstatus;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getMetraj() {
		return metraj;
	}

	public void setMetraj(double metraj) {
		this.metraj = metraj;
	}

}
