package com.arslanbey.depotakip.web.viewBean;

/**
 * @author emre.ertoprak
 *
 */

public class IslemSearchBean {
	
	private int id;
	 
	private String fisNo;

	private double miktar;
	
	private double birimFiyat;
	
	private double tutar;
	
	private double stopaj;
	
	private double yekun;
	
	private int islemTuru;
	
	private int materialId; 
	
	private int musteriId;
	
	private int companyId;
	
	private String tarih;
	
	private String tarih1;
	
	private String tarih2;

	
	public IslemSearchBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public String getFisNo() {
		return fisNo;
	}

	public double getMiktar() {
		return miktar;
	}

	public double getBirimFiyat() {
		return birimFiyat;
	}

	public double getTutar() {
		return tutar;
	}

	public double getStopaj() {
		return stopaj;
	}

	public double getYekun() {
		return yekun;
	}

	public int getIslemTuru() {
		return islemTuru;
	}

	public int getMaterialId() {
		return materialId;
	}

	public int getMusteriId() {
		return musteriId;
	}

	public String getTarih() {
		return tarih;
	}

	public String getTarih1() {
		return tarih1;
	}

	public String getTarih2() {
		return tarih2;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setFisNo(String fisNo) {
		this.fisNo = fisNo;
	}

	public void setMiktar(double miktar) {
		this.miktar = miktar;
	}

	public void setBirimFiyat(double birimFiyat) {
		this.birimFiyat = birimFiyat;
	}

	public void setTutar(double tutar) {
		this.tutar = tutar;
	}

	public void setStopaj(double stopaj) {
		this.stopaj = stopaj;
	}

	public void setYekun(double yekun) {
		this.yekun = yekun;
	}

	public void setIslemTuru(int islemTuru) {
		this.islemTuru = islemTuru;
	}

	public void setMaterialId(int materialId) {
		this.materialId = materialId;
	}

	public void setMusteriId(int musteriId) {
		this.musteriId = musteriId;
	}

	public void setTarih(String tarih) {
		this.tarih = tarih;
	}

	public void setTarih1(String tarih1) {
		this.tarih1 = tarih1;
	}

	public void setTarih2(String tarih2) {
		this.tarih2 = tarih2;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	
   
}
