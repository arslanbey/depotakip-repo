package com.arslanbey.depotakip.web.viewBean;

public class MaterialStockBean {
	
 
	private int id;
	
	private String irsaliyeNo;
	
	private String aciklama;
	
	private String mahalle;
	
	private String sokak;
	
	private String no;

	private int materialId;
	
	private double cikis;
	
	private int companyId;

	private String tarih;

	
	
	@Override
	public String toString() {
		return "MaterialStockBean [id=" + id + ", materialId=" + materialId + ", irsaliyeNo=" + irsaliyeNo + ", aciklama=" + aciklama + ", mahalle="
				+ mahalle + ", sokak=" + sokak + ", no=" + no + ",  cikis=" + cikis
				+ ", companyId=" + companyId + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIrsaliyeNo() {
		return irsaliyeNo;
	}

	public void setIrsaliyeNo(String irsaliyeNo) {
		this.irsaliyeNo = irsaliyeNo;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getMahalle() {
		return mahalle;
	}

	public void setMahalle(String mahalle) {
		this.mahalle = mahalle;
	}

	public String getSokak() {
		return sokak;
	}

	public void setSokak(String sokak) {
		this.sokak = sokak;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public int getMaterialId() {
		return materialId;
	}

	public void setMaterialId(int materialId) {
		this.materialId = materialId;
	}

	

	public double getCikis() {
		return cikis;
	}

	public void setCikis(double cikis) {
		this.cikis = cikis;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getTarih() {
		return tarih;
	}

	public void setTarih(String tarih) {
		this.tarih = tarih;
	}

	

   
}
