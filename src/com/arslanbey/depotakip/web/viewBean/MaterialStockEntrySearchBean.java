package com.arslanbey.depotakip.web.viewBean;

public class MaterialStockEntrySearchBean {
	
 
	private int id;
	
	private String irsaliyeNo;

	private int companyId;
	
	private int materialId;
	
	private double stokMiktari;
	
	private String tarih;
	
	private String tarih1;
	
	private String tarih2;

	


	@Override
	public String toString() {
		return "MaterialStockEntrySearchBean [id=" + id + ", irsaliyeNo=" + irsaliyeNo + ", companyId=" + companyId
				+ ", materialId=" + materialId + ", stokMiktari=" + stokMiktari + ", tarih=" + tarih + ", tarih1="
				+ tarih1 + ", tarih2=" + tarih2 + "]";
	}

	public String getTarih1() {
		return tarih1;
	}

	public void setTarih1(String tarih1) {
		this.tarih1 = tarih1;
	}

	public String getTarih2() {
		return tarih2;
	}

	public void setTarih2(String tarih2) {
		this.tarih2 = tarih2;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIrsaliyeNo() {
		return irsaliyeNo;
	}

	public void setIrsaliyeNo(String irsaliyeNo) {
		this.irsaliyeNo = irsaliyeNo;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getMaterialId() {
		return materialId;
	}

	public void setMaterialId(int materialId) {
		this.materialId = materialId;
	}

	public double getStokMiktari() {
		return stokMiktari;
	}

	public void setStokMiktari(double stokMiktari) {
		this.stokMiktari = stokMiktari;
	}

	public String getTarih() {
		return tarih;
	}

	public void setTarih(String tarih) {
		this.tarih = tarih;
	}


	
	
	
   
}
