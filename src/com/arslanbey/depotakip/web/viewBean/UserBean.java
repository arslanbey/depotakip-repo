package com.arslanbey.depotakip.web.viewBean;

public class UserBean {
	
	
	private int userId;

	private String name;
	
	private String surName;
	
	private String userName;
	
	private String password;
	
	private int departmentId;
	
	private int roleId;

	@Override
	public String toString() {
		return "UserBean [userId=" + userId + ", name=" + name + ", surName=" + surName + ", userName=" + userName
				+ ", password=" + password + ", departmentId=" + departmentId + ", roleId=" + roleId + "]";
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	
	
	
   
}
