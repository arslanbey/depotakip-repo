package com.arslanbey.depotakip.web.entity;

 

public class Material {

	private int materialId;

	private String malzeme;

	private String birim;

	//private double birimFiyat;

	private String aciklama;

	private double stok;
	
	private double kdvOran;
	
	private double stopajOran;


	public Material() {
		super();
	}

	public int getMaterialId() {
		return materialId;
	}

	public void setMaterialId(int materialId) {
		this.materialId = materialId;
	}

	public String getMalzeme() {
		return malzeme;
	}

	public void setMalzeme(String malzeme) {
		this.malzeme = malzeme;
	}

	public String getBirim() {
		return birim;
	}

	public void setBirim(String birim) {
		this.birim = birim;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public double getStok() {
		return stok;
	}

	public void setStok(double stok) {
		this.stok = stok;
	}

	public double getKdvOran() {
		return kdvOran;
	}

	public double getStopajOran() {
		return stopajOran;
	}

	public void setKdvOran(double kdvOran) {
		this.kdvOran = kdvOran;
	}

	public void setStopajOran(double stopajOran) {
		this.stopajOran = stopajOran;
	}

//	public double getBirimFiyat() {
//		return birimFiyat;
//	}
//
//	public void setBirimFiyat(double birimFiyat) {
//		this.birimFiyat = birimFiyat;
//	}

}