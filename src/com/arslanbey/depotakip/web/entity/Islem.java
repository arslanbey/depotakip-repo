package com.arslanbey.depotakip.web.entity;

public class Islem {

	private int islemId;
 
	private String fisNo;

	private double miktar;
	
	private double birimFiyat;
	
	private double tutar;
	
	private double stopaj;
	
	private double kdv;
	
	private double yekun;
	
	private int islemTuru;
	
	private int materialId; 
	
//	private int musteriId;
	
	private int companyId;
	
	private String tarih;
	
	private String mAd;
	private String mBabaAd;
	private String mDogumYeri;
	private String mDogumTarihi;
	private String mKoy;
	private String mHesapNo;
	private String mTel;
	
	
	
	 

	public Islem() {
		// TODO Auto-generated constructor stub
	}

	 
	public Islem(String fisNo, double miktar, double birimFiyat, double tutar, double stopaj, double yekun,
			int islemTuru, int materialId, String tarih, String mAd, String mBabaAd,
			String mDogumYeri, String mDogumTarihi, String mKoy, String mHesapNo, String mTel) {
		super();
		this.fisNo = fisNo;
		this.miktar = miktar;
		this.birimFiyat = birimFiyat;
		this.tutar = tutar;
		this.stopaj = stopaj;
		this.yekun = yekun;
		this.islemTuru = islemTuru;
		this.materialId = materialId;
//		this.musteriId = musteriId;
//		this.companyId = companyId;
		this.tarih = tarih;
		this.mAd = mAd;
		this.mBabaAd = mBabaAd;
		this.mDogumYeri = mDogumYeri;
		this.mDogumTarihi = mDogumTarihi;
		this.mKoy = mKoy;
		this.mHesapNo = mHesapNo;
		this.mTel = mTel;
	}



	public Islem(String fisNo, double miktar, double birimFiyat, double tutar, double stopaj, double yekun,
			int islemTuru, int materialId,String tarih) {
		super();
		this.fisNo = fisNo;
		this.miktar = miktar;
		this.birimFiyat = birimFiyat;
		this.tutar = tutar;
		this.stopaj = stopaj;
		this.yekun = yekun;
		this.islemTuru = islemTuru;
		this.materialId = materialId;
//		this.musteriId = musteriId;
//		this.companyId = companyId;
		this.tarih = tarih;
	}


	public int getIslemId() {
		return islemId;
	}

	public String getFisNo() {
		return fisNo;
	}

	public double getMiktar() {
		return miktar;
	}

	public double getBirimFiyat() {
		return birimFiyat;
	}

	public double getTutar() {
		return tutar;
	}

	public double getStopaj() {
		return stopaj;
	}

	public double getYekun() {
		return yekun;
	}

	public int getIslemTuru() {
		return islemTuru;
	}

	public int getMaterialId() {
		return materialId;
	}

//	public int getMusteriId() {
//		return musteriId;
//	}

	public String getTarih() {
		return tarih;
	}

	public void setIslemId(int islemId) {
		this.islemId = islemId;
	}

	public void setFisNo(String fisNo) {
		this.fisNo = fisNo;
	}

	public void setMiktar(double miktar) {
		this.miktar = miktar;
	}

	public void setBirimFiyat(double birimFiyat) {
		this.birimFiyat = birimFiyat;
	}

	public void setTutar(double tutar) {
		this.tutar = tutar;
	}

	public void setStopaj(double stopaj) {
		this.stopaj = stopaj;
	}

	public void setYekun(double yekun) {
		this.yekun = yekun;
	}

	public void setIslemTuru(int islemTuru) {
		this.islemTuru = islemTuru;
	}

	public void setMaterialId(int materialId) {
		this.materialId = materialId;
	}

//	public void setMusteriId(int musteriId) {
//		this.musteriId = musteriId;
//	}

	public void setTarih(String tarih) {
		this.tarih = tarih;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}



	public String getmAd() {
		return mAd;
	}



	public String getmBabaAd() {
		return mBabaAd;
	}



	public String getmDogumYeri() {
		return mDogumYeri;
	}



	public String getmDogumTarihi() {
		return mDogumTarihi;
	}



	public String getmKoy() {
		return mKoy;
	}



	public String getmHesapNo() {
		return mHesapNo;
	}



	public String getmTel() {
		return mTel;
	}



	public void setmAd(String mAd) {
		this.mAd = mAd;
	}



	public void setmBabaAd(String mBabaAd) {
		this.mBabaAd = mBabaAd;
	}



	public void setmDogumYeri(String mDogumYeri) {
		this.mDogumYeri = mDogumYeri;
	}



	public void setmDogumTarihi(String mDogumTarihi) {
		this.mDogumTarihi = mDogumTarihi;
	}



	public void setmKoy(String mKoy) {
		this.mKoy = mKoy;
	}



	public void setmHesapNo(String mHesapNo) {
		this.mHesapNo = mHesapNo;
	}



	public void setmTel(String mTel) {
		this.mTel = mTel;
	}


	public double getKdv() {
		return kdv;
	}


	public void setKdv(double kdv) {
		this.kdv = kdv;
	}


	@Override
	public String toString() {
 
		
		return "{\'islemId\':\'" + islemId + "\',\'companyId\':\'" + companyId + "\',\'kdv\':\'" + kdv + "\',\'fisNo\':\'" + fisNo + "\',\'miktar\':\'" + miktar + "\',\'birimFiyat\':\'" + birimFiyat
				+ "\',\'tutar\':\'" + tutar + "\',\'stopaj\':\'" + stopaj + "\',\'yekun\':\'" + yekun + "\',\'islemTuru\':\'" + islemTuru
				+ "\',\'materialId\':\'" + materialId + "\',\'tarih\':\'" + tarih + "\',\'mAd\':\'" + mAd + "\',\'mBabaAd\':\'" + mBabaAd
				+ "\',\'mDogumYeri\':\'" + mDogumYeri + "\',\'mDogumTarihi\':\'" + mDogumTarihi + "\',\'mKoy\':\'" + mKoy + "\',\'mHesapNo\':\'"
				+ mHesapNo + "\',\'mTel\':\'" + mTel + "\'}";
	}
 
}