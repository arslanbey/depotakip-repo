package com.arslanbey.depotakip.web.entity;

 

public class FNCompany {

	private int cId;

	private String name;

 
	public FNCompany() {
		super();
	}


	public int getcId() {
		return cId;
	}


	public void setcId(int id) {
		this.cId = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String toString() {
		return "FNCompany [id=" + cId + ", name=" + name + "]";
	}

 

}