package com.arslanbey.depotakip.web.entity;

 

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value="session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class User {

	String user; 

	private int id; 

	private String name; 
	
	private String surName; 
	
	private String userName; 
	
	private String password; 

	private int departmentId;
	
	private int roleId; 
	
	private String lastLoginDate;
	
	private int USERID;
	
	private int maxid;
	

	@Override
	public String toString() {
		return "User [user=" + user + ", id=" + id + ", name=" + name + ", surName=" + surName + ", userName="
				+ userName + ", password=" + password + ", departmentId=" + departmentId + ", roleId=" + roleId + "]";
	}
	
	public User(){}
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
	public String getlastLoginDate(){
		return lastLoginDate;
	}
	public void setlastLoginDate(String lastLoginDate){
		this.lastLoginDate = lastLoginDate;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	
	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getUSERID() {
		return USERID;
	}

	public void setUSERID(int uSERID) {
		USERID = uSERID;
	}

	public int getMaxid() {
		return maxid;
	}

	public void setMaxid(int maxid) {
		this.maxid = maxid;
	}
	
	

//	public Timestamp getiDate() {
//		return iDate;
//	}
//
//	public void setiDate(Timestamp iDate) {
//		this.iDate = iDate;
//	}
//
//	public Timestamp getuDate() {
//		return uDate;
//	}
//
//	public void setuDate(Timestamp uDate) {
//		this.uDate = uDate;
//	}

}