package com.arslanbey.depotakip.web.entity;

 

public class Mahalle {

	
	private int id;
	
	private String mahalle ;
	
	private int bolge;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMahalle() {
		return mahalle;
	}

	public void setMahalle(String mahalle) {
		this.mahalle = mahalle;
	}

	public int getBolge() {
		return bolge;
	}

	public void setBolge(int bolge) {
		this.bolge = bolge;
	}

	public Mahalle() {
		super();
	}
	
}
