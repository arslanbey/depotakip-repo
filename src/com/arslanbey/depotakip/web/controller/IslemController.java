package com.arslanbey.depotakip.web.controller;

import java.io.File;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.arslanbey.depotakip.web.delegate.FNCompanyDelegate;
import com.arslanbey.depotakip.web.delegate.IslemDelegate;
import com.arslanbey.depotakip.web.delegate.MaterialDelegate;
import com.arslanbey.depotakip.web.entity.FNCompany;
import com.arslanbey.depotakip.web.entity.Islem;
import com.arslanbey.depotakip.web.entity.Material;
import com.arslanbey.depotakip.web.entity.User;
import com.arslanbey.depotakip.web.utility.AppConstants;
import com.arslanbey.depotakip.web.utility.ArrayToTable;
import com.arslanbey.depotakip.web.viewBean.IslemSearchBean;
import com.arslanbey.depotakip.web.viewBean.LoginBean;

@Controller
public class IslemController {

	private static final Logger LOGGER = LoggerFactory.getLogger(IslemController.class);

	@Autowired
	private IslemDelegate islemDelegate;

	@Autowired
	private MaterialDelegate materialDelegate;

	@Autowired
	private FNCompanyDelegate companyDelegate;

	@Autowired
	ServletContext servletContext;

	public static NumberFormat formatter = new DecimalFormat("#,##0.00");

	@RequestMapping(value = "/islemList", method = RequestMethod.GET)
	public ModelAndView getIslem(HttpServletRequest request, HttpServletResponse response) {

		boolean sessionIsValid = AppConstants.validateSession(request);
		double totalAlisMiktari = 0;
		double totalSatisMiktari = 0;

		LOGGER.debug("getIslem method is started");

		if (!sessionIsValid) {
			ModelAndView model = new ModelAndView("login");
			LoginBean loginBean = new LoginBean();
			model.addObject("loginBean", loginBean);
			return model;

		}

		ArrayList<Islem> islemList = new ArrayList<Islem>();
		ArrayList<Material> materialList = new ArrayList<Material>();
		ArrayList<FNCompany> companyList = new ArrayList<FNCompany>();

		ArrayList<User> userList = new ArrayList<User>();
		try {
			// userList = userDelegate.getUserList();

			islemList = islemDelegate.getAllIslem();

			companyList = companyDelegate.getCompanyList();

			materialList = materialDelegate.getMaterialList();

			System.out.println("getIslem size : " + islemList.size() + " bitti");

			for (Islem islem : islemList) {
				// System.out.println("islem : " + islem.toString());
				if (islem.getIslemTuru() == AppConstants.ISLEM_TURU_ALIS) {
					totalAlisMiktari += islem.getMiktar();

				} else if (islem.getIslemTuru() == AppConstants.ISLEM_TURU_SATIS) {
					totalSatisMiktari += islem.getMiktar();

				}

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(" IslemController getIslem()  SQLException : " + e.getMessage());

		}

		ModelAndView model = new ModelAndView("islem");
		model.addObject("islemList", islemList);
		model.addObject("companyList", companyList);
		model.addObject("materialList", materialList);
		model.addObject("islemBean", new Islem());
		model.addObject("userList", userList);
		model.addObject("totalAlisMiktari", formatter.format(totalAlisMiktari));
		model.addObject("totalSatisMiktari", formatter.format(totalSatisMiktari));
		model.addObject("islemSearchBean", new IslemSearchBean());

		return model;
	}

	@RequestMapping(value = "/createNewIslem", method = RequestMethod.POST)
	public ModelAndView createNewIslem(HttpServletRequest request, HttpServletResponse response, Islem islemBean) {
		boolean sessionIsValid = AppConstants.validateSession(request);

		if (!sessionIsValid) {
			ModelAndView model = new ModelAndView("login");
			LoginBean loginBean = new LoginBean();
			model.addObject("loginBean", loginBean);
			return model;

		}
		System.out.println("Islem Bean : " + islemBean.toString());
		ArrayList<Islem> islemList = new ArrayList<Islem>();
		ArrayList<FNCompany> companyList = new ArrayList<FNCompany>();
		ArrayList<Material> materialList = new ArrayList<Material>();

		boolean isCreatedIslem = false;

		boolean isUpdated = false;

		double totalAlisMiktari = 0;
		double totalSatisMiktari = 0;

		try {

			System.out.println("Islem id : " + islemBean.getIslemId());

			calculateVergi(islemBean);

			if (islemBean.getIslemId() == 0) {

				Material material = materialDelegate.getMaterialById(islemBean.getMaterialId());

				isCreatedIslem = islemDelegate.insertIslem(islemBean);

				if (isCreatedIslem) {
					System.out.println("Islem is created . ");

					if (islemBean.getIslemTuru() == 1) { // Alış İşlemi

						double guncelstok = material.getStok() + islemBean.getMiktar();
						double guncelstok2f = AppConstants.round(guncelstok, 2);

						material.setStok(guncelstok2f);

						boolean isUpdatedStock = materialDelegate.updateMaterial(material);

						if (isUpdatedStock) {

							System.out.println("Stok güncellendi . ");

						}

					} else if (islemBean.getIslemTuru() == 2) { // Satış işlemi

						double guncelstok = material.getStok() - islemBean.getMiktar();
						double guncelstok2f = AppConstants.round(guncelstok, 2);

						material.setStok(guncelstok2f);

						boolean isUpdatedStock = materialDelegate.updateMaterial(material);

						if (isUpdatedStock) {

							System.out.println("Stok güncellendi . ");

						}

					} else {

						System.out.println("Alış Satış bilgisi yok. Stok güncellenemedi . ");

					}

				} else {
					System.out.println("Islem cannot be created. ");
				}
			} else {

				try {

					Islem oldIslem = islemDelegate.getIslemById(islemBean.getIslemId());

					double eskistok = oldIslem.getMiktar();

					isUpdated = islemDelegate.updateIslem(islemBean);

					materialList = materialDelegate.getMaterialList();

					companyList = companyDelegate.getCompanyList();

					islemList = islemDelegate.getAllIslem();

					if (isUpdated) {
						System.out.println("material stock entry is updated.");

						Material material = materialDelegate.getMaterialById(islemBean.getMaterialId());

						if (islemBean.getIslemTuru() == AppConstants.ISLEM_TURU_ALIS) {
							double guncelstok = material.getStok() - eskistok + islemBean.getMiktar();
							double guncelstok2f = AppConstants.round(guncelstok, 2);
							material.setStok(guncelstok2f);

						} else if (islemBean.getIslemTuru() == AppConstants.ISLEM_TURU_SATIS) {
							double guncelstok = material.getStok() + eskistok - islemBean.getMiktar();
							double guncelstok2f = AppConstants.round(guncelstok, 2);
							material.setStok(guncelstok2f);

						}

						boolean isUpdatedStock = materialDelegate.updateMaterial(material);

						if (isUpdatedStock) {

							System.out.println("Stok güncellendi . ");

						} else {

							System.out.println("Stok güncellenemedi . ");
						}

						for (Islem islem : islemList) {

							if (islem.getIslemTuru() == AppConstants.ISLEM_TURU_ALIS) {
								totalAlisMiktari += islem.getMiktar();

							} else if (islem.getIslemTuru() == AppConstants.ISLEM_TURU_SATIS) {
								totalSatisMiktari += islem.getMiktar();

							}

						}
					} else {
						System.out.println("updateIslem cannot be updated. ");

					}

				} catch (SQLException e) {
					// TODO Auto-generated catch block

					System.out.println("updateIslem cannot be updated.");

				}

			}
			islemList = islemDelegate.getAllIslem();
			companyList = companyDelegate.getCompanyList();
			materialList = materialDelegate.getMaterialList();

			for (Islem islem : islemList) {

				if (islem.getIslemTuru() == AppConstants.ISLEM_TURU_ALIS) {
					totalAlisMiktari += islem.getMiktar();

				} else if (islem.getIslemTuru() == AppConstants.ISLEM_TURU_SATIS) {
					totalSatisMiktari += islem.getMiktar();

				}

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block

			System.out.println("Islem cannot be created.");

		}

		ModelAndView model = new ModelAndView("islem");
		model.addObject("islemList", islemList);
		model.addObject("companyList", companyList);
		model.addObject("materialList", materialList);
		model.addObject("islemBean", new Islem());
		model.addObject("totalAlisMiktari", formatter.format(totalAlisMiktari));
		model.addObject("totalSatisMiktari", formatter.format(totalSatisMiktari));
		model.addObject("islemSearchBean", new IslemSearchBean());
		return model;
	}

	private void calculateVergi(Islem islemBean) throws SQLException {
		// kdv or stopaj hesaplama
		Material material = materialDelegate.getMaterialById(islemBean.getMaterialId());

		if (islemBean.getIslemTuru() == 1) { // Alış İşlemi

			double stopaj = islemBean.getTutar() * material.getStopajOran() / 100;

			islemBean.setStopaj(stopaj);
			islemBean.setYekun(islemBean.getTutar() + stopaj);

		} else if (islemBean.getIslemTuru() == 2) { // Satış işlemi

			double kdv = islemBean.getTutar() * material.getKdvOran() / 100;

			islemBean.setKdv(kdv);
			islemBean.setYekun(islemBean.getTutar() + kdv);
		}
	}
	//
	// @RequestMapping(value = "/updateIslem", method = RequestMethod.POST)
	// @ResponseBody
	// public ModelAndView updateIslem(HttpServletRequest request,
	// HttpServletResponse response, Islem islemBean) {
	// System.out.println("Güncelleme işlemi başladı. " +
	// islemBean.getMiktar());
	//
	// boolean sessionIsValid = AppConstants.validateSession(request);
	//
	// if (!sessionIsValid) {
	// ModelAndView model = new ModelAndView("login");
	// LoginBean loginBean = new LoginBean();
	// model.addObject("loginBean", loginBean);
	// return model;
	//
	// }
	//
	// // LOGGER.debug("updateWorkOrder() is started with materialId : " +
	// // materialBean.getMaterialId());
	// // System.out.println("updateWorkOrder : " + materialBean.getMalzeme());
	// boolean isUpdated = false;
	// ArrayList<Islem> islemList = new ArrayList<Islem>();
	// ArrayList<FNCompany> companyList = new ArrayList<FNCompany>();
	// ArrayList<Material> materialList = new ArrayList<Material>();
	//
	// double totalAlisMiktari = 0;
	// double totalSatisMiktari = 0;
	//
	// try {
	//
	// Islem oldIslem = islemDelegate.getIslemById(islemBean.getIslemId());
	//
	// double eskistok = oldIslem.getMiktar();
	//
	// calculateVergi(islemBean);
	// isUpdated = islemDelegate.updateIslem(islemBean);
	//
	// materialList = materialDelegate.getMaterialList();
	//
	// companyList = companyDelegate.getCompanyList();
	//
	// islemList = islemDelegate.getAllIslem();
	//
	// if (isUpdated) {
	// System.out.println("material stock entry is updated.");
	//
	// Material material =
	// materialDelegate.getMaterialById(islemBean.getMaterialId());
	//
	// if (islemBean.getIslemTuru() == AppConstants.ISLEM_TURU_ALIS) {
	// double guncelstok = material.getStok() - eskistok +
	// islemBean.getMiktar();
	// double guncelstok2f = AppConstants.round(guncelstok, 2);
	// material.setStok(guncelstok2f);
	//
	// } else if (islemBean.getIslemTuru() == AppConstants.ISLEM_TURU_SATIS) {
	// double guncelstok = material.getStok() + eskistok -
	// islemBean.getMiktar();
	// double guncelstok2f = AppConstants.round(guncelstok, 2);
	// material.setStok(guncelstok2f);
	//
	// }
	//
	// boolean isUpdatedStock = materialDelegate.updateMaterial(material);
	//
	// if (isUpdatedStock) {
	//
	// System.out.println("Stok güncellendi . ");
	//
	// } else {
	//
	// System.out.println("Stok güncellenemedi . ");
	// }
	//
	// for (Islem islem : islemList) {
	//
	// if (islem.getIslemTuru() == AppConstants.ISLEM_TURU_ALIS) {
	// totalAlisMiktari += islem.getMiktar();
	//
	// } else if (islem.getIslemTuru() == AppConstants.ISLEM_TURU_SATIS) {
	// totalSatisMiktari += islem.getMiktar();
	//
	// }
	//
	// }
	// } else {
	// System.out.println("updateIslem cannot be updated. ");
	//
	// }
	//
	// } catch (SQLException e) {
	// // TODO Auto-generated catch block
	//
	// System.out.println("updateIslem cannot be updated.");
	//
	// }
	//
	// // materialBean = new Material();
	//
	// ModelAndView model = new ModelAndView("islem");
	// model.addObject("islemList", islemList);
	// model.addObject("companyList", companyList);
	// model.addObject("materialList", materialList);
	// model.addObject("islemBean", new Islem());
	// model.addObject("totalAlisMiktari", formatter.format(totalAlisMiktari));
	// model.addObject("totalSatisMiktari", formatter.format(totalSatisMiktari));
	// model.addObject("islemSearchBean", new IslemSearchBean());
	//
	// return model;
	// }

	@RequestMapping(value = "/deleteIslem/{IslemId}", method = RequestMethod.POST)
	public ModelAndView deleteIslem(@PathVariable(value = "IslemId") int IslemId, HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("Silme işlemi başladı. ");
		boolean sessionIsValid = AppConstants.validateSession(request);

		if (!sessionIsValid) {
			ModelAndView model = new ModelAndView("login");
			LoginBean loginBean = new LoginBean();
			model.addObject("loginBean", loginBean);
			return model;

		}
		LOGGER.debug("Islem user is started with userId : " + IslemId);
		boolean isDeletedIslem = false;
		boolean fromDeletedIslem = true;
		ArrayList<Islem> islemList = new ArrayList<Islem>();

		ArrayList<FNCompany> companyList = new ArrayList<FNCompany>();
		ArrayList<Material> materialList = new ArrayList<Material>();

		double totalAlisMiktari = 0;
		double totalSatisMiktari = 0;

		try {
			Islem oldIslem = islemDelegate.getIslemById(IslemId);
			double eskistok = oldIslem.getMiktar();
			int materialId = oldIslem.getMaterialId();
			System.out.println("eski Stok  hali : " + eskistok);

			isDeletedIslem = islemDelegate.deleteIslem(IslemId);

			if (isDeletedIslem) {
				System.out.println("İslem is deleted. ");

				companyList = companyDelegate.getCompanyList();
				islemList = islemDelegate.getAllIslem();
				materialList = materialDelegate.getMaterialList();
				System.out.println("islemList size  : " + islemList.size());

				Material material = materialDelegate.getMaterialById(materialId);

				if (oldIslem.getIslemTuru() == AppConstants.ISLEM_TURU_ALIS) {
					double guncelstok = material.getStok() - oldIslem.getMiktar();
					double guncelstok2f = AppConstants.round(guncelstok, 2);
					material.setStok(guncelstok2f);

				} else if (oldIslem.getIslemTuru() == AppConstants.ISLEM_TURU_SATIS) {
					double guncelstok = material.getStok() + oldIslem.getMiktar();
					double guncelstok2f = AppConstants.round(guncelstok, 2);
					material.setStok(guncelstok2f);

				}

				boolean isDeletedStock = materialDelegate.updateMaterial(material);

				if (isDeletedStock) {

					System.out.println("Stok güncellendi . ");
				}
				for (Islem islem : islemList) {

					if (islem.getIslemTuru() == AppConstants.ISLEM_TURU_ALIS) {
						totalAlisMiktari += islem.getMiktar();

					} else if (islem.getIslemTuru() == AppConstants.ISLEM_TURU_SATIS) {
						totalSatisMiktari += islem.getMiktar();

					}

				}

			} else {

				System.out.println("Islem cannot be deleted. ");

				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block

			System.out.println("Islem cannot be deleted.");

			LOGGER.error("Islem cannot be deleted. " + e.getMessage(), e);

			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

		}

		ModelAndView model = new ModelAndView("islem");
		model.addObject("islemList", islemList);
		model.addObject("companyList", companyList);
		model.addObject("materialList", materialList);
		model.addObject("islemBean", new Islem());
		model.addObject("totalAlisMiktari", formatter.format(totalAlisMiktari));
		model.addObject("totalSatisMiktari", formatter.format(totalSatisMiktari));
		model.addObject("islemSearchBean", new IslemSearchBean());
		model.addObject("fromDeletedIslem", fromDeletedIslem);
		model.addObject("isDeletedIslem", isDeletedIslem);

		return model;
	}

	@RequestMapping(value = "/searchIslem/{islemTuru}", method = RequestMethod.GET)
	public ModelAndView searchMaterialEntry(@PathVariable(value = "islemTuru") int islemTuru,
			HttpServletRequest request, HttpServletResponse response) {
		boolean sessionIsValid = AppConstants.validateSession(request);
		// String responseText = "Fail";
		if (!sessionIsValid) {
			ModelAndView model = new ModelAndView("login");
			LoginBean loginBean = new LoginBean();
			model.addObject("loginBean", loginBean);
			return model;

		}
		ArrayList<Islem> islemList = new ArrayList<Islem>();
		ArrayList<FNCompany> companyList = new ArrayList<FNCompany>();
		ArrayList<Material> materialList = new ArrayList<Material>();
		try {
			islemList = islemDelegate.getAllIslemByIslemTuru(islemTuru);
			companyList = companyDelegate.getCompanyList();
			materialList = materialDelegate.getMaterialList();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Departman cannot be searched. " + e.getMessage());
		}

		ModelAndView model = new ModelAndView("islem");
		model.addObject("islemList", islemList);
		model.addObject("companyList", companyList);
		model.addObject("materialList", materialList);
		model.addObject("islemBean", new Islem());

		return model;
	}

	@RequestMapping(value = "/searchIslem", method = RequestMethod.POST)
	public ModelAndView searchIslem(HttpServletRequest request, HttpServletResponse response,
			IslemSearchBean islemSearchBean) {
		boolean sessionIsValid = AppConstants.validateSession(request);
		System.out.println("islemSearchBean.getMaterialId() : " + islemSearchBean.getMaterialId());

		double totalAlisMiktari = 0;
		double totalSatisMiktari = 0;

		if (!sessionIsValid) {
			ModelAndView model = new ModelAndView("login");
			LoginBean loginBean = new LoginBean();
			model.addObject("loginBean", loginBean);
			return model;

		}

		ArrayList<Islem> islemList = new ArrayList<Islem>();

		ArrayList<FNCompany> companyList = new ArrayList<FNCompany>();
		ArrayList<Material> materialList = new ArrayList<Material>();

		try {

			islemList = islemDelegate.searchIslem(islemSearchBean.getTarih1(), islemSearchBean.getTarih2(),
					islemSearchBean.getMusteriId(), islemSearchBean.getMaterialId(), islemSearchBean.getIslemTuru());

			companyList = companyDelegate.getCompanyList();

			materialList = materialDelegate.getMaterialList();

			for (Islem islem : islemList) {

				if (islem.getIslemTuru() == AppConstants.ISLEM_TURU_ALIS) {
					totalAlisMiktari += islem.getMiktar();

				} else if (islem.getIslemTuru() == AppConstants.ISLEM_TURU_SATIS) {
					totalSatisMiktari += islem.getMiktar();

				}

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block

			System.out.println("Islem cannot be created.");

		}

		ModelAndView model = new ModelAndView("islem");
		model.addObject("islemList", islemList);
		model.addObject("companyList", companyList);

		model.addObject("materialList", materialList);
		model.addObject("islemBean", new Islem());
		model.addObject("totalAlisMiktari", formatter.format(totalAlisMiktari));
		model.addObject("totalSatisMiktari", formatter.format(totalSatisMiktari));
		model.addObject("islemSearchBean", new IslemSearchBean());
		model.addObject("selectedMaterial", islemSearchBean.getMaterialId());
		model.addObject("selectedIslemTuru", islemSearchBean.getIslemTuru());
		model.addObject("selectedTarih1", islemSearchBean.getTarih1());
		model.addObject("selectedTarih2", islemSearchBean.getTarih2());

		return model;
	}

	@ResponseBody
	@RequestMapping(value = "/createStopajReport", method = RequestMethod.POST)
	public String createStopajReport(@RequestParam("materialId") String materialId,
			@RequestParam("islemTuru") String islemTuru, @RequestParam("tarih1") String tarih1,
			@RequestParam("tarih2") String tarih2, HttpServletRequest request, HttpServletResponse response) {

		boolean sessionIsValid = AppConstants.validateSession(request);

		System.out.println("createStopajReport.getMaterialId() : " + materialId);

		if (!sessionIsValid) {

			return "Session invalid.";
		}

		ArrayList<Islem> islemList = new ArrayList<Islem>();

		try {

			islemList = islemDelegate.searchIslem(tarih1, tarih2, 0, Integer.parseInt(materialId),
					Integer.parseInt(islemTuru));

			try {
				HashMap<String, String> map = new HashMap<String, String>();

				Material material = materialDelegate.getMaterialById(Integer.parseInt(materialId));

				Date t1 = AppConstants.stringFormatter(tarih1);
				tarih1 = AppConstants.dateFormatterForDateß(t1);

				Date t2 = AppConstants.stringFormatter(tarih2);
				tarih2 = AppConstants.dateFormatterForDateß(t2);
				double totalStopaj = 0;
				double total = 0;

				for (int i = 0; i < islemList.size(); i++) {
					Islem islem = islemList.get(i);
					totalStopaj += islem.getStopaj();
					total += islem.getTutar();

				}

				map.put("malzeme", material.getMalzeme());
				map.put("islemTuru", "Alış");
				map.put("tarih1", tarih1);
				map.put("tarih2", tarih2);
				map.put("totalStopaj", String.valueOf(totalStopaj));
				map.put("total", String.valueOf(total));

				File file = new File(servletContext.getRealPath(AppConstants.RAPOR_DEST_PDF));

				map.put("filePath", file.getPath());

				new ArrayToTable(islemList, map);

				return "Stopaj dosyasi olusturulmustur. " + AppConstants.RAPOR_DEST_PDF;

			} catch (Exception e) {
				// TODO Auto-generated catch block
				LOGGER.error("Error at export to PDF : ", e);
				e.printStackTrace();
				return "İşlem Başarısız";
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Error at export to PDF : ", e);
			e.printStackTrace();
			System.out.println("Stopaj file cannot be created.");
			return "İşlem Başarısız";
		}

	}

}
