package com.arslanbey.depotakip.web.controller;

 

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.arslanbey.depotakip.web.delegate.UserDelegate;
import com.arslanbey.depotakip.web.entity.User;
import com.arslanbey.depotakip.web.utility.AppConstants;
import com.arslanbey.depotakip.web.viewBean.LoginBean;
import com.arslanbey.depotakip.web.viewBean.UserBean;

@Controller
public class ProfileController {
	
	@Autowired
	private UserDelegate userDelegate;

	

	private static final Logger LOGGER = LoggerFactory.getLogger(ProfileController.class);

	

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView displayHomePage(HttpServletRequest request, HttpServletResponse response) {
		boolean sessionIsValid = AppConstants.validateSession(request);
		
		if(!sessionIsValid){
			ModelAndView model = new ModelAndView("login");
			LoginBean loginBean = new LoginBean();
			model.addObject("loginBean", loginBean);
			return model;
			
		}
//		ArrayList<User> userList = new ArrayList<User>();
//		try {
//			userList = userDelegate.getUserList();
//
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//
//		}
	
		//userDelegate.getUserByUserId(userId);
		User user =  (User) request.getSession().getAttribute("user");
		
		try {
			//dbdeb cekilen son user bilgileri
		 user = userDelegate.getUserByUserId(user.getId());
		 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("User user.getDepartmentId( : " +user.getDepartmentId());
		UserBean userBean = new UserBean();
		ModelAndView model = new ModelAndView("profile");
		//model.addObject("userList", userList);
		model.addObject("user",user);
		model.addObject("userBean", userBean);
		return model;
	}
	
	
	@RequestMapping(value = "/editProfile", method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView editProfile( HttpServletRequest request,
			HttpServletResponse response, UserBean userBean) {
		boolean sessionIsValid = AppConstants.validateSession(request);
		if(!sessionIsValid){
			ModelAndView model = new ModelAndView("login");
			LoginBean loginBean = new LoginBean();
			model.addObject("loginBean", loginBean);
			return model;	
		}
		LOGGER.debug("Edit user is started with userId : " + userBean.getUserId());
		boolean fromEditProfile = true;
		boolean isEdited = false;

		ArrayList<User> userList = null;
		try {
				isEdited = userDelegate.editProfile(userBean.getUserId(),userBean.getUserName(),userBean.getName(),userBean.getSurName(),userBean.getRoleId(),userBean.getDepartmentId());
				User editedProfile = userDelegate.getUserByUserId(userBean.getUserId());
				request.getSession().setAttribute("user", editedProfile);
				 userList = userDelegate.getUserList();
				 
				 System.out.println("Edit oldu" + userBean.toString());
				 System.out.println("EditProfilesayfasından oldu" + fromEditProfile);
		} catch (SQLException e) {
			System.out.println("User cannot be Edited.");
			LOGGER.error("User cannot be Edited. " + e.getMessage(), e);
		}
	User user =  (User) request.getSession().getAttribute("user");
		
		try {
			//dbdeb cekilen son user bilgileri
		 user = userDelegate.getUserByUserId(user.getId());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		ModelAndView model = new ModelAndView("profile");
		model.addObject("userList", userList);
		model.addObject("userBean", userBean);
		model.addObject("fromEditProfile", fromEditProfile);
		model.addObject("isEdited", isEdited);
		
		model.addObject("user",user);
		return model;
		
	}
	
	

}
