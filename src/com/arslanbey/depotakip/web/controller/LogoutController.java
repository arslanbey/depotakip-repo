package com.arslanbey.depotakip.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.arslanbey.depotakip.web.viewBean.LoginBean;

@Controller
public class LogoutController {

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView executeLogin(HttpServletRequest request, HttpServletResponse response, HttpSession session) {

		try {
			request.getSession().setAttribute("loggedInUser", null);
			request.getSession().setAttribute("user", null);
			request.getSession().setAttribute("companyAuth", null);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception : " + e.getMessage());
		}

		ModelAndView model = new ModelAndView("login");

		model.addObject("loginBean", new LoginBean());

		return model;

	}
}
