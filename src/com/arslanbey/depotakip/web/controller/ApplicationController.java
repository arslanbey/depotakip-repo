package com.arslanbey.depotakip.web.controller;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.arslanbey.depotakip.web.delegate.MaterialDelegate;
import com.arslanbey.depotakip.web.delegate.UserDelegate;
import com.arslanbey.depotakip.web.entity.Material;
import com.arslanbey.depotakip.web.entity.User;
import com.arslanbey.depotakip.web.utility.AppConstants;
import com.arslanbey.depotakip.web.viewBean.LoginBean;
import com.arslanbey.depotakip.web.viewBean.UserBean;
import com.google.gson.Gson;

@Controller
public class ApplicationController {

	@Autowired
	private UserDelegate userDelegate;

	@Autowired
	private MaterialDelegate materialDelegate;

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public ModelAndView displayFnDashboard(HttpServletRequest request, HttpServletResponse response) {
		User loggedInUser = (User) request.getSession().getAttribute("loggedInUser");
		boolean sessionIsValid = AppConstants.validateSession(request);

		if (!sessionIsValid) {
			ModelAndView model = new ModelAndView("login");
			LoginBean loginBean = new LoginBean();
			model.addObject("loginBean", loginBean);
			return model;

		}

		try {

			// System.out.println("User before : " + loggedInUser.toString());
			loggedInUser = userDelegate.getUserByUserId(loggedInUser.getId());

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("SQLException : " + e.getMessage());
		}
		UserBean userBean = new UserBean();
		ModelAndView model = new ModelAndView("dashboard");
		model.addObject("loggedInUser", loggedInUser);
		model.addObject("userBean", userBean);

		return model;
	}

	@RequestMapping(value = "/materialReport", method = RequestMethod.GET)
	@ResponseBody
	public String userReport(HttpServletRequest request, HttpServletResponse response) {
		// User loggedInUser = (User)
		// request.getSession().getAttribute("loggedInUser");
		String responseText = "Fail";

		try {
			ArrayList<Material> mList = materialDelegate.getMaterialList();

			Gson gson = new Gson();

			String json = gson.toJson(mList);

			System.out.println("Material List JSON : " + json);
			responseText = json;
			response.setStatus(HttpServletResponse.SC_ACCEPTED);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("SQLException : " + e.getMessage());
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}

		return responseText;
	}

}
