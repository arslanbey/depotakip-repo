package com.arslanbey.depotakip.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.arslanbey.depotakip.web.delegate.UserDelegate;
import com.arslanbey.depotakip.web.entity.User;
import com.arslanbey.depotakip.web.viewBean.LoginBean;

@Controller
public class LoginController {

	@Autowired
	private UserDelegate userDelegate;

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView displayLogin(HttpServletRequest request, HttpServletResponse response, LoginBean loginBean) {

		ModelAndView model = new ModelAndView("login");
		// LoginBean loginBean = new LoginBean();
		model.addObject("loginBean", loginBean);

		return model;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView executeLogin(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			@ModelAttribute("loginBean") LoginBean loginBean) {
		ModelAndView model = null;
		LOGGER.debug("login method is started.");

		try {

			User isValidUser = userDelegate.isValidUser(loginBean.getUsername(), loginBean.getPassword());

			if (isValidUser.getId() != 0) {

				System.out.println("User Login Successfull");

				request.getSession().setAttribute("loggedInUser", isValidUser);
				request.getSession().setAttribute("user", isValidUser);

				System.out.println("User set Session : " + loginBean.getUsername());

				model = new ModelAndView("dashboard");
				model.addObject("user", isValidUser);

				return model;

			} else {
				System.out.println("User id 0 ");
				model = new ModelAndView("login");
				request.setAttribute("message", "Invalid credentials!!");
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception : " + e.getMessage());
			// System.out.println("Exception : " + userList);
		}

		return model;
	}
}
