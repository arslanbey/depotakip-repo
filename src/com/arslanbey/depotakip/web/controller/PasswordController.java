package com.arslanbey.depotakip.web.controller;

 

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.arslanbey.depotakip.web.delegate.UserDelegate;
import com.arslanbey.depotakip.web.entity.User;
import com.arslanbey.depotakip.web.utility.AppConstants;
import com.arslanbey.depotakip.web.viewBean.LoginBean;

@Controller
public class PasswordController {
	
	@Autowired
	private UserDelegate userDelegate;

	private static final Logger LOGGER = LoggerFactory.getLogger(PasswordController.class);

	

	@RequestMapping(value = "/password", method = RequestMethod.GET)
	public ModelAndView displayHomePage(HttpServletRequest request, HttpServletResponse response) {
		boolean sessionIsValid = AppConstants.validateSession(request);
		
		if(!sessionIsValid){
			ModelAndView model = new ModelAndView("login");
			LoginBean loginBean = new LoginBean();
			model.addObject("loginBean", loginBean);
			return model;
			
		}

		
		ModelAndView model = new ModelAndView("password");

		return model;
	}
	
	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)
	@ResponseBody
	public String changePasswordString( @RequestParam("oldpassword") String oldpassword,@RequestParam("newpassword") String newpassword,@RequestParam("renewpassword") String renewpassword,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		System.out.println("changePassword is started with params : " + oldpassword);
		Response res =null;
		boolean sessionIsValid = AppConstants.validateSession(request);
String responseText = "Fail";
		if (!sessionIsValid) {
			
			 res = Response.notModified("Password  cannot be change. Session is not valid.").build();
			 return  "Fail";
//			return "Password  cannot be change. Session is not valid.";

		}
		//UserDevice userDevice =  userDeviceDelegate.getUserDevice(Integer.parseInt(userId));
		User sessionUser = (User) request.getSession().getAttribute("user");
		int userId = sessionUser.getId();
		//TODO new - retype  eslestırme 
		if(!newpassword.equals(renewpassword))
		{
			 res = Response.notModified("Sifreniz degistirilemedi,yeni sifreleriniz ayni degil.").build();
			 return  "Fail";
//			return "Sifreniz degistirilemedi,yeni sifreleriniz ayni degil.";
		}
		
		
		// TODO old password kontrol db den
		try {
			User isValidUser = userDelegate.isValidUser(sessionUser.getUserName(),oldpassword);
			
			if (isValidUser.getId() != 0){
				System.out.println("User Control Success");
	
				userDelegate.changePassword(userId, newpassword);
				
				System.out.println("User ID : " + sessionUser.getId());
				
			}else{
				 res = Response.notModified("Sifreniz degistirilemedi,eski sifreniz Sisteme kayitli degil.").build();
				 return  "Fail";
//				return "Sifreniz degistirilemedi,eski sifreniz Sisteme kayitli degil.";
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			responseText = "Sifreniz degistirilemedi."+e.getMessage();
			 res = Response.notModified(responseText).build();
			 return  "Fail";
			
//			return responseText;
		}
			
		//responseText = "Kullanici sifreniz degistirildi.";
		//res=Response.ok().build();
		
		 return  "Success";
	}
	
	
	
	

}
