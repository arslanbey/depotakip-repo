package com.arslanbey.depotakip.web.controller;

 

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.arslanbey.depotakip.web.delegate.MaterialDelegate;
import com.arslanbey.depotakip.web.delegate.UserDelegate;
import com.arslanbey.depotakip.web.entity.Material;
import com.arslanbey.depotakip.web.entity.User;
import com.arslanbey.depotakip.web.utility.AppConstants;
import com.arslanbey.depotakip.web.viewBean.LoginBean;

@Controller
public class MaterialController {

	@Autowired
	private MaterialDelegate materialDelegate;
	
	@Autowired
	private UserDelegate userDelegate;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MaterialController.class);

	@RequestMapping(value = "/fn_MaterialList", method = RequestMethod.GET)
	public ModelAndView displayDashboard(HttpServletRequest request, HttpServletResponse response) {
		
		boolean sessionIsValid = AppConstants.validateSession(request);
		
		Material materialBean = new Material();
		
		if (!sessionIsValid) {
			ModelAndView model = new ModelAndView("login");
			LoginBean loginBean = new LoginBean();
			model.addObject("loginBean", loginBean);
			return model;

		}
		
		ArrayList<Material> materialList = new ArrayList<Material>();
		ArrayList<User> userList = new ArrayList<User>();
		try {
			userList = userDelegate.getUserList();
			
			materialList = materialDelegate.getMaterialList();
			LOGGER.debug("getMaterialList : " + materialList.size() + "bitti");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			LOGGER.debug(" displayDashboard materialList SQLException : " + e.getMessage());

		}

		ModelAndView model = new ModelAndView("fn_MaterialList");
		model.addObject("materialList", materialList);
		model.addObject("materialBean", materialBean);
		model.addObject("userList", userList);

		return model;
	}
	
	
	@RequestMapping(value = "/createNewMaterial", method = RequestMethod.POST)
	public ModelAndView createNewMaterial(HttpServletRequest request, HttpServletResponse response, Material materialBean) {
		boolean sessionIsValid = AppConstants.validateSession(request);
		
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String malzeme = request.getParameter("malzeme");
		String aciklama = request.getParameter("aciklama");
		LOGGER.debug(" malzeme : " +  malzeme + "  ; aciklama  : " +aciklama);
		if(!sessionIsValid){
			ModelAndView model = new ModelAndView("login");
			LoginBean loginBean = new LoginBean();
			model.addObject("loginBean", loginBean);
			return model;
			
		}
		
		ArrayList<Material> materialList = new ArrayList<Material>();
		Material material = new Material();
		try {
			LOGGER.debug(" materialBean.getMalzeme()   : " + materialBean.getMalzeme() );
			material.setMalzeme(materialBean.getMalzeme());
			material.setBirim(materialBean.getBirim());
			material.setKdvOran(materialBean.getKdvOran());
			material.setStopajOran(materialBean.getStopajOran());
			material.setAciklama(materialBean.getAciklama());
			material.setStok(materialBean.getStok());
//			material.setBirimFiyat(materialBean.getBirimFiyat());
			
			boolean isCreate = materialDelegate.insertMaterial(material);
			if (isCreate) {
				LOGGER.debug("Material is created. ");
				materialList = materialDelegate.getMaterialList();
			} else {
				LOGGER.debug("Material cannot be created. ");
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block

			LOGGER.debug("Material cannot be created.");

		}
		
		ModelAndView model = new ModelAndView("fn_MaterialList");
		model.addObject("materialList", materialList);
		model.addObject("materialBean", materialBean);

		return model;
	}
	
	@RequestMapping(value = "/deleteMaterial", method = RequestMethod.POST)
	@ResponseBody
	public String deleteMaterial(@RequestParam("materialId") int materialId, HttpServletRequest request,
			HttpServletResponse response) {
		LOGGER.debug("Silme işlemi başladı. ");
		boolean sessionIsValid = AppConstants.validateSession(request);
		
		 
		
		if(!sessionIsValid){
			LOGGER.debug("Material cannot be deleted. ");

			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			
			return "Material can not be deleted. Session is not valid.";
			
		}
		 
		boolean isDeleted = false;
		
		try {

			isDeleted = materialDelegate.deleteMaterial(materialId);
		
			if (isDeleted) {
				LOGGER.debug("Material is deleted. ");
			} else {
				LOGGER.debug("Material cannot be deleted. ");

				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

				return "Company cannot be deleted.";
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block

			LOGGER.debug("Material cannot be deleted.");
 
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

			return "Failed: " + e.getMessage();
		}

		return "Successfuly deleted.";
	}
	
	@RequestMapping(value = "/updateMaterial", method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView updateMaterial(HttpServletRequest request,
			HttpServletResponse response,Material materialBean) {
		LOGGER.debug("Güncelleme işlemi başladı. ");
		boolean sessionIsValid = AppConstants.validateSession(request);
		
		if(!sessionIsValid){
			ModelAndView model = new ModelAndView("login");
			LoginBean loginBean = new LoginBean();
			model.addObject("loginBean", loginBean);
			return model;
			
		}

		LOGGER.debug("updateMaterial() is started with materialId : " + materialBean.getMaterialId());
		LOGGER.debug("malzeme  : " + materialBean.getMalzeme() +"kdv  : " + materialBean.getKdvOran());
		boolean isUpdated = false;
		ArrayList<Material> materialList = null;
		try {

			isUpdated = materialDelegate.updateMaterial(materialBean);
	
	 
			if (isUpdated) {
				LOGGER.debug("Material is updated. ");
				materialList = materialDelegate.getMaterialList();
			} else {
				LOGGER.debug("Material cannot be updated. ");

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block

			LOGGER.error("Material cannot be updated." + e);

		}
		
		materialBean = new Material();
		
		ModelAndView model = new ModelAndView("fn_MaterialList");
		model.addObject("materialList", materialList);
		model.addObject("materialBean", materialBean);
			
 
		return model;
	}


}
