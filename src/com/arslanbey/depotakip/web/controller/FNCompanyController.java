package com.arslanbey.depotakip.web.controller;

 

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.arslanbey.depotakip.web.delegate.FNCompanyDelegate;
import com.arslanbey.depotakip.web.entity.FNCompany;
import com.arslanbey.depotakip.web.utility.AppConstants;
import com.arslanbey.depotakip.web.viewBean.LoginBean;

@Controller
public class FNCompanyController {

	private static final Logger LOGGER = LoggerFactory.getLogger(FNCompanyController.class);

	@Autowired
	private FNCompanyDelegate fnCompanyDelegate;

	@RequestMapping(value = "/fn_CompanyManagement", method = RequestMethod.GET)
	public ModelAndView companyManagement(HttpServletRequest request, HttpServletResponse response) {

		boolean sessionIsValid = AppConstants.validateSession(request);

		FNCompany company = new FNCompany();

		if (!sessionIsValid) {
			ModelAndView model = new ModelAndView("login");
			LoginBean loginBean = new LoginBean();
			model.addObject("loginBean", loginBean);
			return model;

		}

		ArrayList<FNCompany> companyList = new ArrayList<FNCompany>();
	 
		try {

			companyList = fnCompanyDelegate.getCompanyList();
			System.out.println("getCompanyList : " + companyList + "bitti");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(" displayDashboard companyList SQLException : " + e.getMessage());

		}

		ModelAndView model = new ModelAndView("fn_CompanyManagement");
		model.addObject("companyList", companyList);
		model.addObject("companyBean", company); 
		
		

		return model;
	}

	@RequestMapping(value = "/addFnCompany", method = RequestMethod.POST)
	public ModelAndView addFnCompany(HttpServletRequest request, HttpServletResponse response, FNCompany companyBean) {
		boolean sessionIsValid = AppConstants.validateSession(request);

		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String name = request.getParameter("name");
		System.out.println("şirket  adı  : " + name);
		if (!sessionIsValid) {
			ModelAndView model = new ModelAndView("login");
			LoginBean loginBean = new LoginBean();
			model.addObject("loginBean", loginBean);
			return model;

		}

		ArrayList<FNCompany> companyList = new ArrayList<FNCompany>();

		FNCompany company = new FNCompany();

		try {
			System.out.println(" companyBean.getName()   : " + companyBean.getName());
			company.setName(companyBean.getName());

			boolean isCreate = fnCompanyDelegate.insertCompany(company);
			if (isCreate) {
				System.out.println("Company is created. ");
				companyList = fnCompanyDelegate.getCompanyList();
			} else {
				System.out.println("Company cannot be created. ");
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block

			System.out.println("Company cannot be created.");

		}

		ModelAndView model = new ModelAndView("fn_CompanyManagement");
		model.addObject("companyList", companyList);
		model.addObject("companyBean", company);

		return model;
	}

	@RequestMapping(value = "/deleteCompany", method = RequestMethod.POST)
	@ResponseBody
	public String deleteCompany(@RequestParam("companyId") int companyId, HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("Silme işlemi başladı. ");
		boolean sessionIsValid = AppConstants.validateSession(request);

		if (!sessionIsValid) {
			System.out.println("Company cannot be deleted. ");

			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

			return "Company can not be deleted. Session is not valid.";

		}

		boolean isDeleted = false;
		try {

			isDeleted = fnCompanyDelegate.deleteCompany(companyId);
			if (isDeleted) {
				System.out.println("Company is deleted. ");
			} else {
				System.out.println("Company cannot be deleted. ");

				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

				return "Company cannot be deleted.";
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block

			System.out.println("User cannot be deleted.");

			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

			return "Failed: " + e.getMessage();
		}

		return "Successfuly deleted.";
	}

	@RequestMapping(value = "/updateCompany", method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView updateCompany(HttpServletRequest request, HttpServletResponse response, FNCompany companyBean) {

		boolean sessionIsValid = AppConstants.validateSession(request);

		if (!sessionIsValid) {
		
			ModelAndView model = new ModelAndView("login");
			
			LoginBean loginBean = new LoginBean();
			
			model.addObject("loginBean", loginBean);
			
			return model;

		}
		LOGGER.debug("updateCompany is started with id : " + companyBean.getcId());
		boolean isEdited = false;
		ArrayList<FNCompany> companyList = null;

		FNCompany company = null;
		try {

			isEdited = fnCompanyDelegate.updateCompany(companyBean);

			company = fnCompanyDelegate.getCompanyById(companyBean.getcId());

			companyList = fnCompanyDelegate.getCompanyList();
			System.out.println("Company can be Edited. ");

		} catch (SQLException e) {
			// TODO Auto-generated catch block

			System.out.println("User cannot be Edited.");

			LOGGER.error("User can be Edited. " + e.getMessage(), e);

		}

		ModelAndView model = new ModelAndView("fn_CompanyManagement");

		model.addObject("companyList", companyList);
		model.addObject("companyBean", company);
		model.addObject("isEdited", isEdited);
		return model;

	}

}
