package com.arslanbey.depotakip.web.controller;

 

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.arslanbey.depotakip.web.delegate.UserDelegate;
import com.arslanbey.depotakip.web.entity.User;
import com.arslanbey.depotakip.web.utility.AppConstants;
import com.arslanbey.depotakip.web.viewBean.LoginBean;
import com.arslanbey.depotakip.web.viewBean.UserBean;
import com.google.gson.Gson;

@Controller
public class UserController {

	@Autowired
	private UserDelegate userDelegate;

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ModelAndView displayHomePage(HttpServletRequest request, HttpServletResponse response) {
		boolean sessionIsValid = AppConstants.validateSession(request);
		
		if(!sessionIsValid){
			ModelAndView model = new ModelAndView("login");
			LoginBean loginBean = new LoginBean();
			model.addObject("loginBean", loginBean);
			return model;
			
		}
		ArrayList<User> userList = new ArrayList<User>();
		try {
			userList = userDelegate.getUserList();
			System.out.println("getUserList : " + userList +"bitti");
			

		} catch (SQLException e) {
			// TODO Auto-generated catch block

		}
		UserBean userBean = new UserBean();
		ModelAndView model = new ModelAndView("users");
		model.addObject("userList", userList);
		model.addObject("userBean", userBean);
		return model;
	}

	@RequestMapping(value = "/createNewUser", method = RequestMethod.POST)
	public ModelAndView createNewUser(HttpServletRequest request, HttpServletResponse response, UserBean userBean) {
		boolean sessionIsValid = AppConstants.validateSession(request);
		
		if(!sessionIsValid){
			ModelAndView model = new ModelAndView("login");
			LoginBean loginBean = new LoginBean();
			model.addObject("loginBean", loginBean);
			return model;
			
		}
		
		ArrayList<User> userList = new ArrayList<User>();
		User user = new User();
		try {
			System.out.println("userBean : " + userBean.getName() );
			user.setName(userBean.getName());
			user.setSurName(userBean.getSurName());
			user.setUserName(userBean.getUserName());
			user.setPassword(userBean.getPassword());
			// user.setiDate(new Timestamp(new Date().getTime()));
			user.setDepartmentId(userBean.getDepartmentId());
			user.setRoleId(userBean.getRoleId());

			boolean isCreate = userDelegate.insertUser(user);
			if (isCreate) {
				System.out.println("User is created. ");
				userList = userDelegate.getUserList();
			} else {
				System.out.println("User cannot be created. ");
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block

			System.out.println("User cannot be created.");

		}

		ModelAndView model = new ModelAndView("users");
		model.addObject("userList", userList);
		model.addObject("userBean", userBean);
		return model;
	}

	
	
	
	
	@RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
	@ResponseBody
	public String deleteUser(@RequestParam("userId") int userId, HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("Silme işlemi başladı. ");
		boolean sessionIsValid = AppConstants.validateSession(request);
		
		if(!sessionIsValid){
			System.out.println("User cannot be deleted. ");

			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			
			return "User can not be deleted. Session is not valid.";
			
		}
		LOGGER.debug("Delete user is started with userId : " + userId);
		boolean isDeleted = false;
		try {

			isDeleted = userDelegate.deleteUser(userId);
			if (isDeleted) {
				System.out.println("User is deleted. ");
			} else {
				System.out.println("User cannot be deleted. ");

				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

				return "User cannot be deleted.";
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block

			System.out.println("User cannot be deleted.");

			LOGGER.error("User cannot be deleted. " + e.getMessage(), e);

			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

			return "Failed: " + e.getMessage();
		}

		return "Successfuly deleted.";
	}
	
	
	@RequestMapping(value = "/editUser", method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView editUser( HttpServletRequest request,
			HttpServletResponse response, UserBean userBean) {
		
		boolean sessionIsValid = AppConstants.validateSession(request);
		
		if(!sessionIsValid){
			ModelAndView model = new ModelAndView("login");
			LoginBean loginBean = new LoginBean();
			model.addObject("loginBean", loginBean);
			return model;
			
		}
		LOGGER.debug("Edit user is started with userId : " + userBean.getUserId());
		boolean isEdited = false;
		ArrayList<User> userList = null;
		try {
			
			isEdited = userDelegate.editUser(userBean.getUserId(),userBean.getUserName(),userBean.getName(),userBean.getSurName(),userBean.getRoleId(),userBean.getDepartmentId());
			User editedUser = userDelegate.getUserByUserId(userBean.getUserId());
			request.getSession().setAttribute("user",editedUser);
				 userList = userDelegate.getUserList();
				 System.out.println("User can be Edited. ");

		} catch (SQLException e) {
			// TODO Auto-generated catch block

			System.out.println("User cannot be Edited." );

			LOGGER.error("User can be Edited. " + e.getMessage(), e);

		}
		
		ModelAndView model = new ModelAndView("users");
		
		model.addObject("userBean", userBean);
		model.addObject("isEdited", isEdited);
		model.addObject("userList", userList);
		return model;
		
	}
	
	
	
	
	@RequestMapping(value = "/searchDepartman", method = RequestMethod.POST)
	@ResponseBody
	public String searchDepartman(@RequestParam("departmentId") int departmentId, HttpServletRequest request,
			HttpServletResponse response) {
		
		boolean sessionIsValid = AppConstants.validateSession(request);
		String responseText = "Fail";
		if(!sessionIsValid){
			System.out.println("Departman cannot be searched. ");

			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			
			return "Departman cannot be searched.  Session is not valid.";
			
		}
		LOGGER.debug("Change user is started with departmentId : " + departmentId);
		// boolean isDeleted = false;
		ArrayList<User> userList = new ArrayList<User>();
		try {
			userList = userDelegate.searchDepartman(departmentId);

			
			Gson gson = new Gson();
			// convert java object to JSON format,
			// and returned as JSON formatted string
			String json = gson.toJson(userList);
			System.out.println(" JSON : " + json);
			responseText = json;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Departman cannot be searched. "+ e.getMessage());
		}
	

		return responseText;
	}
	
	
	
	
	

}
