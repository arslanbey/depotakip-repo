package com.arslanbey.depotakip.web.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arslanbey.depotakip.web.dao.FNCompanyDao;
import com.arslanbey.depotakip.web.entity.FNCompany;

/**
 * @author arslan
 */

public class FNCompanyDaoImpl implements FNCompanyDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(FNCompanyDaoImpl.class);

	DataSource dataSource;

	

	public DataSource getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	private Connection conn = null;
	
	private Connection getConnection() {

		try {

			conn = this.dataSource.getConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Connection ERROR : " + e);

		}

		return conn;

	}

	@Override
	public FNCompany getCompanyById(int id) throws SQLException {
		// TODO Auto-generated method stub
		FNCompany company = new FNCompany();
		String query = "Select * from company where id = ? ";
		PreparedStatement pstmt = getConnection().prepareStatement(query);
		pstmt.setInt(1, id);

		try {
			ResultSet resultSet = pstmt.executeQuery();
			if (resultSet.next()) {
				company.setcId(resultSet.getInt("id"));
				company.setName(resultSet.getString("name"));

				return company;
			} else
				return company;
		} catch (Exception e) {
			System.out.println("isValidFnCompany()  query exception  : " + e.getMessage());
			LOGGER.error("isValidFnCompany()  query exception  :" + e.getMessage());
			return company;

		} finally {
			conn.close();

			if (pstmt != null) {
				pstmt.close();
			}

		}

	}

	@Override
	public ArrayList<FNCompany> getCompanyList() throws SQLException {
		// TODO Auto-generated method stub
		ArrayList<FNCompany> companyList = new ArrayList<FNCompany>();

		String query = "Select * from company ORDER BY id DESC";
		PreparedStatement pstmt = getConnection().prepareStatement(query);

		try {
			ResultSet resultSet = pstmt.executeQuery();
			while (resultSet.next()) {
				FNCompany company = new FNCompany();
				company.setcId(resultSet.getInt("id"));
				company.setName(resultSet.getString("name"));

				companyList.add(company);
			}

		} catch (Exception e) {
			System.out.println("getFnCompanyList()  query exception  : " + e.getMessage());
			LOGGER.error("getFnCompanyList()  query exception  :" + e.getMessage());
			return (ArrayList<FNCompany>) companyList;

		} finally {
			conn.close();

			if (pstmt != null) {
				pstmt.close();
			}

		}
		return companyList;
	}

	@Override
	public boolean insertCompany(FNCompany fNCompany) throws SQLException {
		// TODO Auto-generated method stub
		String insertTableSQL = "INSERT INTO  company(name) VALUES (?)";

		PreparedStatement pstmt = getConnection().prepareStatement(insertTableSQL);
		try {
			pstmt.setString(1, fNCompany.getName());

			// execute insert SQL stetement
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("FnCompany can not be inserted." + e.getMessage());
			LOGGER.error("FnCompany can not be inserted.  " + e.getMessage());
			return false;

		} finally {
			conn.close();

			if (pstmt != null) {
				pstmt.close();
			}

		}

		return true;
	}

	@Override
	public boolean deleteCompany(int id) throws SQLException {
		// TODO Auto-generated method stub
		String deleteSQL = "DELETE FROM company WHERE id = ? ";
		PreparedStatement pstmt = getConnection().prepareStatement(deleteSQL);
		pstmt.setInt(1, id);
		try {
			int resultSet = pstmt.executeUpdate();
			System.out.println("FnCompany record is deleted!: " + resultSet);

		} catch (Exception e) {
			System.out.println("FnCompany can not be deleted. : " + e.getMessage());
			LOGGER.error("FnCompany can not be deleted. :" + e.getMessage());
			return false;
		} finally {
			conn.close();

			if (pstmt != null) {
				pstmt.close();
			}

		}

		return true;
	}

	@Override
	public boolean updateCompany(FNCompany company) throws SQLException {
		// TODO Auto-generated method stub
		String updateSQL = "UPDATE  company set name = ?  WHERE id = ? ";
		PreparedStatement pstmt = getConnection().prepareStatement(updateSQL);
		pstmt.setString(1, company.getName());
		pstmt.setInt(2, company.getcId());
		try {
			int resultSet = pstmt.executeUpdate();
			System.out.println("FnCompany record is updated!: " + resultSet);

		} catch (Exception e) {
			System.out.println("FnCompany can not be updated. : " + e.getMessage());
			LOGGER.error("FnCompany can not be updated. :" + e.getMessage());
			return false;
		} finally {
			conn.close();

			if (pstmt != null) {
				pstmt.close();
			}

		}

		return true;
	}

}