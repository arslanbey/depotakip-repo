package com.arslanbey.depotakip.web.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arslanbey.depotakip.web.dao.UserDao;
import com.arslanbey.depotakip.web.entity.User;

/**
 * @author arslan
 */
public class UserDaoImpl implements UserDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

	DataSource dataSource;

	

	public DataSource getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	private Connection conn = null;
	
	private Connection getConnection() {

		try {

			conn = this.dataSource.getConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Connection ERROR : " + e);

		}

		return conn;

	}

	@Override
	public User isValidUser(String userName, String password) throws SQLException {
		User user = new User();
		user.setId(0);

		String query = "Select * from app_user where username = ? and password = ?     ORDER BY id DESC limit 1";
		PreparedStatement pstmt = getConnection().prepareStatement(query);
		pstmt.setString(1, userName);
		pstmt.setString(2, password);

		try {
			ResultSet resultSet = pstmt.executeQuery();
			if (resultSet.next()) {
				user.setId(resultSet.getInt("id"));
				user.setName(resultSet.getString("name"));
				user.setSurName(resultSet.getString("surname"));
				user.setUserName(resultSet.getString("username"));
				user.setPassword(resultSet.getString("password"));
				user.setRoleId(resultSet.getInt("role_id"));
				user.setDepartmentId(resultSet.getInt("department_id"));
				// user.setiDate(resultSet.getTimestamp("idate"));
				// user.setuDate(resultSet.getTimestamp("udate"));

				return user;
			} else
				return user;
		} catch (Exception e) {
			System.out.println("isValidUser()  query exception  : " + e.getMessage());
			LOGGER.error("isValidUser()  query exception  :" + e.getMessage());
			return user;

		} finally {
			conn.close();
 
			if (pstmt != null) {
				pstmt.close();
			}

		}
	}

	@Override
	public boolean insertUser(User user) throws SQLException {

		String insertTableSQL = "INSERT INTO app_user(name, surname , username, password , role_id,department_id) VALUES (?,?,?,?,?,?)";

		PreparedStatement pstmt = getConnection().prepareStatement(insertTableSQL);

		try {

			pstmt.setString(1, user.getName());
			pstmt.setString(2, user.getSurName());
			pstmt.setString(3, user.getUserName());
			pstmt.setString(4, user.getPassword());
			// pstmt.setTimestamp(5, new Timestamp(new Date().getTime()));
			pstmt.setInt(5, user.getRoleId());
			pstmt.setInt(6, user.getDepartmentId());

			// execute insert SQL stetement
			pstmt.executeUpdate();

		} catch (Exception e) {
			System.out.println("User can not be inserted." + e.getMessage());
			LOGGER.error("User can not be inserted.  " + e.getMessage());
			return false;

		} finally {
			conn.close();
 
			if (pstmt != null) {
				pstmt.close();
			}

		}

		return true;

	}

	@Override
	public User getUserByUserName(String userName) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	public User getUserByUserId(int userId) throws SQLException {
		User user = new User();
		String query = "Select * from app_user where ID = ? ";
		PreparedStatement pstmt = getConnection().prepareStatement(query);
		pstmt.setInt(1, userId);

		try {
			ResultSet resultSet = pstmt.executeQuery();
			if (resultSet.next()) {
				user.setId(resultSet.getInt("id"));
				user.setName(resultSet.getString("name"));
				user.setSurName(resultSet.getString("surname"));
				user.setUserName(resultSet.getString("username"));
				user.setPassword(resultSet.getString("password"));
				user.setRoleId(resultSet.getInt("role_id"));
				user.setDepartmentId(resultSet.getInt("department_id"));
				// user.setiDate(resultSet.getTimestamp("idate"));
				// user.setuDate(resultSet.getTimestamp("udate"));

				return user;
			} else
				return user;
		} catch (Exception e) {
			System.out.println("isValidUser()  query exception  : " + e.getMessage());
			LOGGER.error("isValidUser()  query exception  :" + e.getMessage());
			return user;

		} finally {
			conn.close();
 
			if (pstmt != null) {
				pstmt.close();
			}

		}

	}

	@Override
	public boolean updateUser(User user) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateUserStateByEmail(String email, int state) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ArrayList<User> getUserList() throws SQLException {

		ArrayList<User> userList = new ArrayList<User>();

		String query = "Select * from app_user ORDER BY id DESC";
		PreparedStatement pstmt = getConnection().prepareStatement(query);

		try {
			ResultSet resultSet = pstmt.executeQuery();
			while (resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setName(resultSet.getString("name"));
				user.setSurName(resultSet.getString("surname"));
				user.setUserName(resultSet.getString("username"));
				user.setPassword(resultSet.getString("password"));
				user.setRoleId(resultSet.getInt("role_id"));
				user.setDepartmentId(resultSet.getInt("department_id"));

				// user.setiDate(resultSet.getTimestamp("idate"));
				// user.setuDate(resultSet.getTimestamp("udate"));

				userList.add(user);
			}

		} catch (Exception e) {
			System.out.println("isValidUser()  query exception  : " + e.getMessage());
			LOGGER.error("isValidUser()  query exception  :" + e.getMessage());
			return (ArrayList<User>) userList;

		} finally {
	 
			conn.close();
			if (pstmt != null) {
				pstmt.close();
			}

		}
		return userList;
	}

	@Override
	public boolean deleteUser(int userId) throws SQLException {
		// TODO Auto-generated method stub

		String deleteSQL = "DELETE FROM app_user WHERE id = ? ";
		PreparedStatement pstmt = getConnection().prepareStatement(deleteSQL);
		pstmt.setInt(1, userId);
		try {
			int resultSet = pstmt.executeUpdate();
			System.out.println("User record is deleted!: " + resultSet);

		} catch (Exception e) {
			System.out.println("User can not be deleted. : " + e.getMessage());
			LOGGER.error("User can not be deleted. :" + e.getMessage());
			return false;
		} finally { 
			conn.close();
			if (pstmt != null) {
				pstmt.close();
			}

		}

		return true;
	}

	public boolean editUser(int userId, String username, String name, String surname, int roleid, int depertmanId)
			throws SQLException {
		// TODO Auto-generated method stub

		String updateSQL = "UPDATE app_user set USERNAME = ? , NAME = ?, SURNAME = ? ,ROLE_ID = ? ,DEPARTMENT_ID = ? WHERE id = ? ";
		PreparedStatement pstmt = getConnection().prepareStatement(updateSQL);
		pstmt.setString(1, username);
		pstmt.setString(2, name);
		pstmt.setString(3, surname);
		pstmt.setInt(4, roleid);

		pstmt.setInt(5, depertmanId);
		pstmt.setInt(6, userId);
		try {
			int resultSet = pstmt.executeUpdate();
			System.out.println("User record is updated!: " + resultSet);

		} catch (Exception e) {
			System.out.println("User can not be updated. : " + e.getMessage());
			LOGGER.error("User can not be updated. :" + e.getMessage());
			return false;
		} finally { 
			conn.close();
			if (pstmt != null) {
				pstmt.close();
			}

		}

		return true;
	}

	public boolean editProfile(int userId, String username, String name, String surname, int depertmanId)
			throws SQLException {
		// TODO Auto-generated method stub

		String updateSQL = "UPDATE app_user set USERNAME = ? , NAME = ?, SURNAME = ?  ,DEPARTMENT_ID = ? WHERE id = ? ";
		PreparedStatement pstmt = getConnection().prepareStatement(updateSQL);
		pstmt.setString(1, username);
		pstmt.setString(2, name);
		pstmt.setString(3, surname);
		pstmt.setInt(4, depertmanId);
		pstmt.setInt(5, userId);
		try {
			int resultSet = pstmt.executeUpdate();
			System.out.println("User record is updated!: " + resultSet);

		} catch (Exception e) {
			System.out.println("User can not be updated. : " + e.getMessage());
			LOGGER.error("User can not be updated. :" + e.getMessage());
			return false;
		} finally { 

			conn.close();
			if (pstmt != null) {
				pstmt.close();
			}

		}

		return true;
	}

	public boolean changePassword(int userId, String password) throws SQLException {
		// TODO Auto-generated method stub

		String updateSQL = "UPDATE app_user set PASSWORD = ? WHERE id = ? ";
		PreparedStatement pstmt = getConnection().prepareStatement(updateSQL);
		pstmt.setString(1, password);
		pstmt.setInt(2, userId);

		try {
			int resultSet = pstmt.executeUpdate();
			System.out.println("User record is updated!: " + resultSet);

		} catch (Exception e) {
			System.out.println("User can not be updated. : " + e.getMessage());
			LOGGER.error("User can not be updated. :" + e.getMessage());
			return false;
		} finally { 

			conn.close();
			if (pstmt != null) {
				pstmt.close();
			}

		}

		return true;
	}

	public ArrayList<User> searchDepartman(int departmentId) throws SQLException {

		ArrayList<User> userList = new ArrayList<User>();

		String query = "Select * from app_user where department_id = ? ORDER BY id DESC";
		PreparedStatement pstmt = getConnection().prepareStatement(query);
		pstmt.setInt(1, departmentId);

		try {
			ResultSet resultSet = pstmt.executeQuery();
			while (resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setName(resultSet.getString("name"));
				user.setSurName(resultSet.getString("surname"));
				user.setUserName(resultSet.getString("username"));
				user.setPassword(resultSet.getString("password"));
				user.setRoleId(resultSet.getInt("role_id"));
				user.setDepartmentId(resultSet.getInt("department_id"));

				// user.setiDate(resultSet.getTimestamp("idate"));
				// user.setuDate(resultSet.getTimestamp("udate"));

				userList.add(user);
			}

		} catch (Exception e) {
			System.out.println("isValidUser()  query exception  : " + e.getMessage());
			LOGGER.error("isValidUser()  query exception  :" + e.getMessage());
			return (ArrayList<User>) userList;

		} finally {
		  
			pstmt.close();

		}
		return userList;
	}

}