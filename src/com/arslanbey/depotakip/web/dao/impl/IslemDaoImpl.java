package com.arslanbey.depotakip.web.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.arslanbey.depotakip.web.dao.IslemDao;
import com.arslanbey.depotakip.web.entity.Islem;
import com.arslanbey.depotakip.web.utility.AppConstants;

public class IslemDaoImpl implements IslemDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(IslemDaoImpl.class);

	DataSource dataSource;

	public DataSource getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	private Connection conn = null;

	private Connection getConnection() {

		try {

			conn = this.dataSource.getConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Connection ERROR : " + e);

		}

		return conn;

	}

	@Override
	public Islem getIslemById(int id) throws SQLException {
		// TODO Auto-generated method stub
		Islem islem = new Islem();
		String query = "Select * from islem where id = ? ";

		PreparedStatement pstmt = getConnection().prepareStatement(query);
		pstmt.setInt(1, id);

		try {
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				String tarih = AppConstants.dateFormatterForDateß(rs.getDate("tarih"));

				islem.setIslemId(rs.getInt("id"));
				islem.setFisNo(rs.getString("fisNo"));
				islem.setMiktar(rs.getDouble("miktar"));
				islem.setBirimFiyat(rs.getDouble("birimFiyat"));
				islem.setTutar(rs.getDouble("tutar"));
				islem.setStopaj(rs.getDouble("stopaj"));
				islem.setYekun(rs.getDouble("yekun"));
				islem.setIslemTuru(rs.getInt("islemTuru"));
				islem.setMaterialId(rs.getInt("materialId"));
				islem.setmAd(rs.getString("mAd"));
				islem.setmBabaAd(rs.getString("mBabaAd"));
				islem.setmDogumYeri(rs.getString("mDogumYeri"));
				islem.setmDogumTarihi(rs.getString("mDogumTarihi"));
				islem.setmKoy(rs.getString("mKoy"));
				islem.setmHesapNo(rs.getString("mHesapNo"));
				islem.setmTel(rs.getString("mTel"));
//				islem.setMusteriId(rs.getInt("musteriId"));
				islem.setCompanyId(rs.getInt("companyId"));
				islem.setKdv(rs.getDouble("kdv"));
				islem.setTarih(tarih);

				return islem;
			} else
				return islem;

		} catch (Exception e) {

			System.out.println("getIslemById()  query exception  : " + e.getMessage());

			LOGGER.error("getIslemById()  query exception  :" + e.getMessage());

			return islem;

		} finally {
			conn.close();

			if (pstmt != null) {
				pstmt.close();
			}

		}
	}

	@Override
	public ArrayList<Islem> getAllIslem() throws SQLException {

		ArrayList<Islem> islemList = new ArrayList<Islem>();

		String query = "Select * from islem ORDER BY id DESC";

		PreparedStatement pstmt = getConnection().prepareStatement(query);

		try {
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {

				String tarih = AppConstants.dateFormatterForDateß(rs.getDate("tarih"));

				LOGGER.debug("tarih = " + tarih);

				Islem islem = new Islem();
				islem.setIslemId(rs.getInt("id"));
				islem.setFisNo(rs.getString("fisNo"));
				islem.setMiktar(rs.getDouble("miktar"));
				islem.setBirimFiyat(rs.getDouble("birimFiyat"));
				islem.setTutar(rs.getDouble("tutar"));
				islem.setStopaj(rs.getDouble("stopaj"));
				islem.setYekun(rs.getDouble("yekun"));
				islem.setIslemTuru(rs.getInt("islemTuru"));
				islem.setMaterialId(rs.getInt("materialId"));
				islem.setmAd(rs.getString("mAd"));
				islem.setmBabaAd(rs.getString("mBabaAd"));
				islem.setmDogumYeri(rs.getString("mDogumYeri"));
				islem.setmDogumTarihi(rs.getString("mDogumTarihi"));
				islem.setmKoy(rs.getString("mKoy"));
				islem.setmHesapNo(rs.getString("mHesapNo"));
				islem.setmTel(rs.getString("mTel"));
//				islem.setMusteriId(rs.getInt("musteriId"));
				islem.setCompanyId(rs.getInt("companyId"));
				islem.setTarih(tarih);
				islem.setKdv(rs.getDouble("kdv"));
				islemList.add(islem);
			}
		} catch (Exception e) {
			System.out.println("getAllIslem()  query exception  : " + e.getMessage());
			LOGGER.error("getAllIslem()  query exception  :" + e.getMessage());
			return (ArrayList<Islem>) islemList;

		} finally {
			conn.close();

			if (pstmt != null) {
				pstmt.close();
			}
		}
		return islemList;
	}

	@Override
	public ArrayList<Islem> getAllIslemByIslemTuru(int islemTuru) throws SQLException {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		ArrayList<Islem> islemList = new ArrayList<Islem>();

		String query = "Select * from islem where islemTuru=?  ORDER BY id DESC";

		PreparedStatement pstmt = getConnection().prepareStatement(query);
		pstmt.setInt(1, islemTuru);

		try {
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {

				String tarih = AppConstants.dateFormatterForDateß(rs.getDate("tarih"));

				LOGGER.debug("tarih = " + tarih);

				Islem islem = new Islem();
				islem.setIslemId(rs.getInt("id"));
				islem.setFisNo(rs.getString("fisNo"));
				islem.setMiktar(rs.getDouble("miktar"));
				islem.setBirimFiyat(rs.getDouble("birimFiyat"));
				islem.setTutar(rs.getDouble("tutar"));
				islem.setStopaj(rs.getDouble("stopaj"));
				islem.setKdv(rs.getDouble("kdv"));
				islem.setYekun(rs.getDouble("yekun"));
				islem.setIslemTuru(rs.getInt("islemTuru"));
				islem.setMaterialId(rs.getInt("materialId"));
				islem.setmAd(rs.getString("mAd"));
				islem.setmBabaAd(rs.getString("mBabaAd"));
				islem.setmDogumYeri(rs.getString("mDogumYeri"));
				islem.setmDogumTarihi(rs.getString("mDogumTarihi"));
				islem.setmKoy(rs.getString("mKoy"));
				islem.setmHesapNo(rs.getString("mHesapNo"));
				islem.setmTel(rs.getString("mTel"));
//				islem.setMusteriId(rs.getInt("musteriId"));
				islem.setCompanyId(rs.getInt("companyId"));
				islem.setTarih(tarih);
				islemList.add(islem);
			}
		} catch (Exception e) {
			System.out.println("getAllIslemByMaterialID()  query exception  : " + e.getMessage());
			LOGGER.error("getAllIslemByMaterialID()  query exception  :" + e.getMessage());
			return (ArrayList<Islem>) islemList;

		} finally {
			conn.close();

			if (pstmt != null) {
				pstmt.close();
			}
		}
		return islemList;
	}

	@Override
	public boolean insertIslem(Islem islem) throws SQLException {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		String insertTableSQL = "INSERT INTO  islem(fisNo,miktar,birimFiyat,tutar,stopaj,yekun,islemTuru,materialId,tarih,mAd,mBabaAd,mDogumYeri,mDogumTarihi,mKoy,mHesapNo,mTel,companyId,kdv)"
				+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement pstmt = getConnection().prepareStatement(insertTableSQL);

		try {

			// AppConstants.dateFormatterForDateß(islem.getTarih());
			Date date = AppConstants.stringFormatter(islem.getTarih());
			pstmt.setString(1, islem.getFisNo());
			pstmt.setDouble(2, islem.getMiktar());
			pstmt.setDouble(3, islem.getBirimFiyat());
			pstmt.setDouble(4, islem.getTutar());
			pstmt.setDouble(5, islem.getStopaj());
			pstmt.setDouble(6, islem.getYekun());
			pstmt.setInt(7, islem.getIslemTuru());
			pstmt.setInt(8, islem.getMaterialId());
			pstmt.setTimestamp(9, new Timestamp(date.getTime()));
			pstmt.setString(10, islem.getmAd());
			pstmt.setString(11, islem.getmBabaAd());
			pstmt.setString(12, islem.getmDogumYeri());
			pstmt.setString(13, islem.getmDogumTarihi());
			pstmt.setString(14, islem.getmKoy());
			pstmt.setString(15, islem.getmHesapNo());
			pstmt.setString(16, islem.getmTel());
			pstmt.setInt(17, islem.getCompanyId());
			pstmt.setDouble(18, islem.getKdv());
			
			// execute insert SQL stetement
			pstmt.executeUpdate();

		} catch (Exception e) {
			System.out.println("Islem can not be inserted." + e );
			LOGGER.error("Islem can not be inserted.  " + e.getMessage());
			return false;

		} finally {
			conn.close();

			if (pstmt != null) {
				pstmt.close();
			}

		}

		return true;
	}

	@Override
	public boolean deleteIslem(int id) throws SQLException {
		// TODO Auto-generated method stub
		String deleteSQL = "DELETE FROM islem WHERE id = ? ";
		PreparedStatement pstmt = getConnection().prepareStatement(deleteSQL);
		pstmt.setInt(1, id);
		try {
			int resultSet = pstmt.executeUpdate();
			System.out.println("deleteIslem record is deleted!: " + resultSet);

		} catch (Exception e) {
			System.out.println("deleteIslem can not be deleted. : " + e.getMessage());
			LOGGER.error("deleteIslem can not be deleted. :" + e.getMessage());
			return false;
		} finally {
			conn.close();

			if (pstmt != null) {
				pstmt.close();
			}

		}

		return true;
	}

	@Override
	public boolean updateIslem(Islem islem) throws SQLException {
		// Date date = AppConstants.stringFormatter(islem.getTarih());

		String updateSQL = "UPDATE islem SET fisNo  = ?,miktar = ?,birimFiyat = ?,tutar = ?,stopaj = ?,yekun = ?,islemTuru = ?,materialId = ?,tarih = ?,mAd = ?, mBabaAd = ?, mDogumYeri = ?"
				+ ", mDogumTarihi = ?, mKoy = ?, mHesapNo = ?, mTel = ? , companyId = ?,kdv = ? WHERE id = ? ";

		PreparedStatement pstmt = getConnection().prepareStatement(updateSQL);

		Date date = AppConstants.stringFormatter(islem.getTarih());

		pstmt.setString(1, islem.getFisNo());
		pstmt.setDouble(2, islem.getMiktar());
		pstmt.setDouble(3, islem.getBirimFiyat());
		pstmt.setDouble(4, islem.getTutar());
		pstmt.setDouble(5, islem.getStopaj());
		pstmt.setDouble(6, islem.getYekun());
		pstmt.setInt(7, islem.getIslemTuru());
		pstmt.setInt(8, islem.getMaterialId());
		pstmt.setTimestamp(9, new Timestamp(date.getTime()));
		pstmt.setString(10, islem.getmAd());
		pstmt.setString(11, islem.getmBabaAd());
		pstmt.setString(12, islem.getmDogumYeri());
		pstmt.setString(13, islem.getmDogumTarihi());
		pstmt.setString(14, islem.getmKoy());
		pstmt.setString(15, islem.getmHesapNo());
		pstmt.setString(16, islem.getmTel());
		pstmt.setInt(17, islem.getCompanyId());
		pstmt.setDouble(18, islem.getKdv());
		pstmt.setInt(19, islem.getIslemId());

		try {
			int resultSet = pstmt.executeUpdate();
			System.out.println("Islem record is updated!: " + resultSet);

		} catch (Exception e) {
			System.out.println("Islem can not be updated. : " + e.getMessage());
			LOGGER.error("Islem can not be updated. :" + e.getMessage());
			return false;
		} finally {
			conn.close();

			if (pstmt != null) {
				pstmt.close();
			}

		}

		return true;
	}

	@Override
	public ArrayList<Islem> searchIslem(String tarih1, String tarih2, int musteriId, int materialId,int islemTuru)
			throws SQLException {
		ArrayList<Islem> islemList = new ArrayList<Islem>();
		// String query = "Select * from islem where tarih >= ?
		// and companyId = ? and islemId=? ";

		NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);

		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("tarih1", tarih1);
		paramSource.addValue("tarih2", tarih2);
		paramSource.addValue("musteriId", musteriId);
		paramSource.addValue("materialId", materialId);
		paramSource.addValue("islemTuru", islemTuru);
		String query = "SELECT * FROM islem WHERE 1=1 ";

		if (tarih1 != "") {

			query += "AND tarih >= :tarih1 ";
		}
		if (tarih2 != "") {

			query += " AND tarih <= :tarih2 ";
		}

		if (musteriId != 0) {

			query += " AND musteriId = :musteriId  ";
		}

		if (materialId != 0) {

			query += " AND materialId= :materialId ";
		}

		if (islemTuru != 0) {

			query += " AND islemTuru= :islemTuru ";
		}

		System.out.println("Query : " + query);

		SqlRowSet rs = jdbcTemplate.queryForRowSet(query, paramSource);

		try {
			// ResultSet resultSet = pstmt.executeQuery();
			while (rs.next()) {

				String tarih = AppConstants.dateFormatterForDateß(rs.getDate("tarih"));

				LOGGER.debug("tarih = " + tarih);

				Islem islem = new Islem();
				islem.setIslemId(rs.getInt("id"));
				islem.setFisNo(rs.getString("fisNo"));
				islem.setMiktar(rs.getDouble("miktar"));
				islem.setBirimFiyat(rs.getDouble("birimFiyat"));
				islem.setTutar(rs.getDouble("tutar"));
				islem.setStopaj(rs.getDouble("stopaj"));
				islem.setYekun(rs.getDouble("yekun"));
				islem.setIslemTuru(rs.getInt("islemTuru"));
				islem.setMaterialId(rs.getInt("materialId"));
				islem.setmAd(rs.getString("mAd"));
				islem.setmBabaAd(rs.getString("mBabaAd"));
				islem.setmDogumYeri(rs.getString("mDogumYeri"));
				islem.setmDogumTarihi(rs.getString("mDogumTarihi"));
				islem.setmKoy(rs.getString("mKoy"));
				islem.setmHesapNo(rs.getString("mHesapNo"));
				islem.setmTel(rs.getString("mTel"));
//				islem.setMusteriId(rs.getInt("musteriId"));
				islem.setCompanyId(rs.getInt("companyId"));
				islem.setKdv(rs.getDouble("kdv"));
				islem.setTarih(tarih);

				islemList.add(islem); // Added to List
			}
			
		} catch (Exception e) {
			System.out.println("getAllIslemByMaterialID()  query exception  : " + e);
			LOGGER.error("getAllIslemByMaterialID()  query exception  :" + e.getMessage());
			return (ArrayList<Islem>) islemList;

		} finally {
			conn.close();

		}
		return islemList;
	}

}
