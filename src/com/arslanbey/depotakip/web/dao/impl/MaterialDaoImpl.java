package com.arslanbey.depotakip.web.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arslanbey.depotakip.web.dao.MaterialDao;
import com.arslanbey.depotakip.web.entity.Material;

/**
 * @author arslan
 */

public class MaterialDaoImpl implements MaterialDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(MaterialDaoImpl.class);

	DataSource dataSource;

	private Connection conn = null;

	public DataSource getDataSource() {
		return this.dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	private Connection getConnection() {

		try {

			conn = this.dataSource.getConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			LOGGER.error("Connection ERROR : " + e);

		}

		return conn;

	}

	@Override
	public boolean insertMaterial(Material material) throws SQLException {

		String insertTableSQL = "INSERT INTO  material(malzeme, birim ,kdvOran,stopajOran, aciklama,stok) VALUES (?,?,?,?,?,?)";

		PreparedStatement pstmt = getConnection().prepareStatement(insertTableSQL);
		try {
			pstmt.setString(1, material.getMalzeme());
			pstmt.setString(2, material.getBirim());
			pstmt.setDouble(3, material.getKdvOran());
			pstmt.setDouble(4, material.getStopajOran());
			pstmt.setString(5, material.getAciklama());
			pstmt.setDouble(6, material.getStok()); 

			// execute insert SQL stetement
			pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("Material can not be inserted." + e.getMessage());
			LOGGER.error("Material can not be inserted.  " + e.getMessage());
			return false;

		} finally {

			conn.close();

			if (pstmt != null) {
				pstmt.close();
			}

		}

		return true;

	}

	public Material getMaterialById(int materialId) throws SQLException {
		Material material = new Material();
		String query = "Select * from material where materialId = ? ";
		PreparedStatement pstmt = getConnection().prepareStatement(query);
		pstmt.setInt(1, materialId);

		try {
			ResultSet resultSet = pstmt.executeQuery();
			if (resultSet.next()) {
				material.setMaterialId(resultSet.getInt("materialId"));
				material.setMalzeme(resultSet.getString("malzeme"));
				material.setBirim(resultSet.getString("birim"));
				material.setAciklama(resultSet.getString("aciklama"));
				material.setStok(resultSet.getDouble("stok"));
				material.setKdvOran(resultSet.getDouble("kdvOran"));
				material.setStopajOran(resultSet.getDouble("stopajOran"));
//				material.setBirimFiyat(resultSet.getDouble("birimFiyat"));
				return material;
			} else
				return material;
		} catch (Exception e) {
			System.out.println("isValidMaterial()  query exception  : " + e.getMessage());
			LOGGER.error("isValidMaterial()  query exception  :" + e.getMessage());
			return material;

		} finally {

			conn.close();

			if (pstmt != null) {
				pstmt.close();
			}

		}

	}

	@Override
	public ArrayList<Material> getMaterialList() throws SQLException {

		ArrayList<Material> materialList = new ArrayList<Material>();

		String query = "Select * from material ORDER BY materialId DESC";

		PreparedStatement pstmt = getConnection().prepareStatement(query);

		try {
			ResultSet resultSet = pstmt.executeQuery();
			while (resultSet.next()) {
				Material material = new Material();
				material.setMaterialId(resultSet.getInt("materialId"));
				material.setMalzeme(resultSet.getString("malzeme"));
				material.setBirim(resultSet.getString("birim"));
				material.setAciklama(resultSet.getString("aciklama"));
				material.setStok(resultSet.getDouble("stok"));
				material.setKdvOran(resultSet.getDouble("kdvOran"));
				material.setStopajOran(resultSet.getDouble("stopajOran"));
//				material.setBirimFiyat(resultSet.getDouble("birimFiyat"));
				materialList.add(material);
			}

		} catch (Exception e) {
			System.out.println("getMaterialList()  query exception  : " + e.getMessage());
			LOGGER.error("getMaterialList()  query exception  :" + e.getMessage());
			return (ArrayList<Material>) materialList;

		} finally {

			conn.close();

			if (pstmt != null) {
				pstmt.close();
			}

		}
		return materialList;
	}

	@Override
	public boolean deleteMaterial(int materialId) throws SQLException {
		// TODO Auto-generated method stub

		String deleteSQL = "DELETE FROM material WHERE materialId = ? ";
		PreparedStatement pstmt = getConnection().prepareStatement(deleteSQL);
		pstmt.setInt(1, materialId);
		try {
			int resultSet = pstmt.executeUpdate();
			System.out.println("Material record is deleted!: " + resultSet);

		} catch (Exception e) {
			System.out.println("Material can not be deleted. : " + e.getMessage());
			LOGGER.error("Material can not be deleted. :" + e.getMessage());
			return false;
		} finally {

			conn.close();

			if (pstmt != null) {
				pstmt.close();
			}

		}

		return true;
	}

	@Override
	public boolean updateMaterial(Material material) throws SQLException {
		// TODO Auto-generated method stub

		String updateSQL = "UPDATE  material set malzeme = ? , birim = ?,kdvOran = ?,stopajOran=?, aciklama = ?,stok = ?  WHERE materialId = ? ";
		PreparedStatement pstmt = getConnection().prepareStatement(updateSQL);
		pstmt.setString(1, material.getMalzeme());
		pstmt.setString(2, material.getBirim());
		pstmt.setDouble(3, material.getKdvOran());
		pstmt.setDouble(4, material.getStopajOran());
		pstmt.setString(5, material.getAciklama());
		pstmt.setDouble(6, material.getStok());
//		pstmt.setDouble(5, material.getBirimFiyat());
		pstmt.setInt(7, material.getMaterialId());

		try {
			int resultSet = pstmt.executeUpdate();
			System.out.println("Material record is updated!: " + resultSet);

		} catch (Exception e) {
			System.out.println("Material can not be updated. : " + e.getMessage());
			LOGGER.error("Material can not be updated. :" + e.getMessage());
			return false;
		} finally {

			conn.close();

			if (pstmt != null) {
				pstmt.close();
			}

		}

		return true;
	}

}