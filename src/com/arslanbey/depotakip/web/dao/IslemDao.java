package com.arslanbey.depotakip.web.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.arslanbey.depotakip.web.entity.Islem;

/**
 * @author arslan This interface will be used to communicate with the Database
 */
public interface IslemDao {
	
	public boolean insertIslem(Islem islem) throws SQLException;

	public boolean deleteIslem(int id) throws SQLException;

	public boolean updateIslem(Islem islem) throws SQLException;

	public Islem getIslemById(int id) throws SQLException;

	public ArrayList<Islem> getAllIslem() throws SQLException;

	public  ArrayList<Islem> getAllIslemByIslemTuru(int islemTuru) throws SQLException;

	public ArrayList<Islem> searchIslem(String tarih1, String tarih2, int musteriId, int materialId,int companyId) throws SQLException;

}
