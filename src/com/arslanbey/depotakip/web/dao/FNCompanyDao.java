package com.arslanbey.depotakip.web.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.arslanbey.depotakip.web.entity.FNCompany;

/**
 * @author arslan This interface will be used to communicate with the Database
 */
public interface FNCompanyDao {

	public FNCompany getCompanyById(int id) throws SQLException;

	public ArrayList<FNCompany> getCompanyList() throws SQLException;

	public boolean insertCompany(FNCompany fNCompany) throws SQLException;

	public boolean deleteCompany(int id) throws SQLException;

	public boolean updateCompany(FNCompany fNCompany) throws SQLException;

}
