package com.arslanbey.depotakip.web.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.arslanbey.depotakip.web.entity.Material;

/**
 * @author arslan
 * This interface will be used to communicate with the
 * Database
 */
public interface MaterialDao
{
	 
	public Material getMaterialById(int materialId) throws SQLException;
	
	public ArrayList<Material> getMaterialList() throws SQLException;
 	
	public boolean insertMaterial(Material material) throws SQLException;
	
	public boolean deleteMaterial(int materialId) throws SQLException;
	
	public boolean updateMaterial(Material material) throws SQLException;
	 
  
}
