package com.arslanbey.depotakip.web.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import com.arslanbey.depotakip.web.entity.User;

/**
 * @author arslan
 * This interface will be used to communicate with the
 * Database
 */
public interface UserDao
{
	public User getUserByUserName(String userName) throws SQLException;
	
	public User getUserByUserId(int userId) throws SQLException;
	
	public ArrayList<User> getUserList() throws SQLException;
	
	public User isValidUser(String userName, String password) throws SQLException;
	
	public boolean insertUser(User user) throws SQLException;
	
	public boolean deleteUser(int userId) throws SQLException;
	
	public boolean editUser(int userId, String username , String name , String surname , int roleid,int DepertmanId) throws SQLException;
	
	public boolean updateUser(User user) throws SQLException;
	
	public boolean updateUserStateByEmail(String email,int state) throws SQLException;
	
	public boolean editProfile(int userId, String username , String name , String surname , int DepertmanId) throws SQLException;

	public boolean changePassword(int userId, String password) throws SQLException;
	
	public ArrayList<User> searchDepartman(int departmentId) throws SQLException;


}
