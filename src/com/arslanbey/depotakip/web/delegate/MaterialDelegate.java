package com.arslanbey.depotakip.web.delegate;



import java.sql.SQLException;
import java.util.ArrayList;

import com.arslanbey.depotakip.web.entity.Material;
import com.arslanbey.depotakip.web.service.MaterialService;

public class MaterialDelegate {
	
	
	private MaterialService materialService;

	public MaterialService getMaterialService() {
		return materialService;
	}

	public void setMaterialService(MaterialService materialService) {
		this.materialService = materialService;
	}
	
	 
	public Material getMaterialById(int materialId) throws SQLException {
		// TODO Auto-generated method stub
		return materialService.getMaterialById(materialId);
	}
 
	public ArrayList<Material> getMaterialList() throws SQLException {
		// TODO Auto-generated method stub
		return materialService.getMaterialList();
	}
 
	public boolean insertMaterial(Material material) throws SQLException {
		// TODO Auto-generated method stub
		return materialService.insertMaterial(material);
	}

 
	public boolean deleteMaterial(int materialId) throws SQLException {
		// TODO Auto-generated method stub
		return materialService.deleteMaterial(materialId);
	}

 
	public boolean updateMaterial(Material material) throws SQLException {
		// TODO Auto-generated method stub
		return materialService.updateMaterial(material);
	}

	 


}
