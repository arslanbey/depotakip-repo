package com.arslanbey.depotakip.web.delegate;



import java.sql.SQLException;
import java.util.ArrayList;

import com.arslanbey.depotakip.web.entity.FNCompany;
import com.arslanbey.depotakip.web.service.FNCompanyService;

public class FNCompanyDelegate {

	private FNCompanyService fnCompanyService;

	public FNCompanyService getFnCompanyService() {
		return fnCompanyService;
	}

	public void setFnCompanyService(FNCompanyService fnCompanyService) {
		this.fnCompanyService = fnCompanyService;
	}

	public FNCompany getCompanyById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return fnCompanyService.getCompanyById(id);
	}

	public ArrayList<FNCompany> getCompanyList() throws SQLException {
		// TODO Auto-generated method stub
		return fnCompanyService.getCompanyList();
	}

	public boolean insertCompany(FNCompany fNCompany) throws SQLException {
		// TODO Auto-generated method stub
		return fnCompanyService.insertCompany(fNCompany);
	}

	public boolean deleteCompany(int id) throws SQLException {
		// TODO Auto-generated method stub
		return fnCompanyService.deleteCompany(id);
	}

	public boolean updateCompany(FNCompany fNCompany) throws SQLException {
		// TODO Auto-generated method stub
		return fnCompanyService.updateCompany(fNCompany);
	}

}
