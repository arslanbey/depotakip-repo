package com.arslanbey.depotakip.web.delegate;



import java.sql.SQLException;
import java.util.ArrayList;

import com.arslanbey.depotakip.web.entity.User;
import com.arslanbey.depotakip.web.service.UserService;

public class UserDelegate {
	private UserService userService;

	public UserService getUserService() {
		return this.userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	

	public User isValidUser(String username, String password)
			throws SQLException {
		return userService.isValidUser(username, password );
	}
	
	public ArrayList<User> getUserList()
			throws SQLException {
		return userService.getUserList();
	}
	public boolean insertUser(User user)
			throws SQLException {
		return userService.insertUser(user);
	}
	
	public boolean deleteUser(int userId) throws SQLException{
		
		return userService.deleteUser(userId);	
	}
	
public boolean editUser(int userId, String username , String name , String surname , int roleid ,int depertmanId) throws SQLException{
		
		return userService.editUser(userId, username, name , surname ,roleid , depertmanId);	
	}

public boolean editProfile(int userId, String username , String name , String surname , int roleid ,int depertmanId) throws SQLException{
	
	return userService.editProfile(userId, username, name , surname , depertmanId);	
}

public boolean changePassword(int userId, String password) throws SQLException{
	
	return userService.changePassword(userId, password);	
}
public User getUserByUserId(int userId) throws SQLException{
	
	return userService.getUserByUserId(userId);
}

public ArrayList<User> searchDepartman(int departmentId) throws SQLException{
	
	return userService.searchDepartman(departmentId);
}


}
