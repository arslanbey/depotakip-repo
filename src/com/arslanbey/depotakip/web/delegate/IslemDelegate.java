package com.arslanbey.depotakip.web.delegate;



import java.sql.SQLException;
import java.util.ArrayList;

import com.arslanbey.depotakip.web.entity.Islem;
import com.arslanbey.depotakip.web.service.IslemService;

public class IslemDelegate {
	
	private IslemService islemService;
	 
	public IslemService getIslemService() {
		return islemService;
	}

	public void setIslemService(IslemService islemService) {
		this.islemService = islemService;
	}
	
	public Islem getIslemById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return islemService.getIslemById(id);
	}

	public ArrayList<Islem> getAllIslemByIslemTuru(int islemTuru) throws SQLException {
		// TODO Auto-generated method stub
		return islemService.getAllIslemByIslemTuru(islemTuru);
	}
	
	public ArrayList<Islem> searchIslem(String tarih1, String tarih2, int musteriId,int materialId, int islemTuru) throws SQLException {
		// TODO Auto-generated method stub
		return islemService.searchIslem(tarih1, tarih2, musteriId, materialId, islemTuru);
	}

	public ArrayList<Islem> getAllIslem() throws SQLException {
		// TODO Auto-generated method stub
		return islemService.getAllIslem();
	}

	public boolean insertIslem(Islem islem) throws SQLException {
		// TODO Auto-generated method stub
		return islemService.insertIslem(islem);
	}

	public boolean deleteIslem(int id) throws SQLException {
		// TODO Auto-generated method stub
		return islemService.deleteIslem(id);
	}

	public boolean updateIslem(Islem islem) throws SQLException {
		// TODO Auto-generated method stub
		return islemService.updateIslem(islem);
	}
	

	 


}
