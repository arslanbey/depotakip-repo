function getCoordinates(address, callback){
    var coordinates;
    geocoder.geocode({address: address}, function(results, status){
        for(var i=0; i< results.length ;i++ ){
            coords_obj = results[i].geometry.location;
            coordinates.push([coords_obj.nb, coords_obj.ob]);
        }
        callback(coordinates);
    });
}

function initializeGoogleMap() {

//    $("#map-canvas").empty();
//    $('#map-canvas').attr('style','');
    console.log("initializeGoogleMap() is started." );
//   console.log("searching address :." +searchingAdress );
//    var ruhsatAdres = getUrlVars()["ruhsatAdres"];
//    ruhsatAdres = decodeURIComponent(ruhsatAdres);
    //console.log("adres="+adres);

//    console.log("ruhsatAdres1="+ruhsatAdres);
    var geocoder = new google.maps.Geocoder();
    var mapProp = {
        center:new google.maps.LatLng(40.987284, 28.908360),
        zoom:10,
        mapTypeId:google.maps.MapTypeId.ROADMAP
    };
    var map=new google.maps.Map(document.getElementById("map-canvas"), mapProp);

//    if(searchingAdress !==""){
//    geocoder.geocode( { 'address': "Zeytinburnu "}, function(results, status) {
//        if (status == google.maps.GeocoderStatus.OK) {
//        console.log("map results  length : " +results.length );
//        for(var i =0; i<results.length ;i++){
//
//                        var marker = new google.maps.Marker({
//                            map: map,
//                            position: results[0].geometry.location
//                        });
//                         map.setCenter(marker.getPosition());
//                var infoWindow = new google.maps.InfoWindow({
//                content: ruhsatAdres
//              });
//              marker.addListener('click', function() {
//                infoWindow.open(marker.get('map'), marker);
//              });
//        }
//              console.log('map succesfully initalized!');
//         } else {
////           alert('Adres bulunamadi. : ' + searchingAdress);
//          console.log('Geocode aranan adres bulunamadi! Durum= ' + status);
//        }
//    });

//}else{

//alert("Ruhsat kaydinin adres bilgisi bulunmamaktadir.");


//}

	navigator.geolocation.getCurrentPosition(function(position) {
		var geolocate = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		var marker = new google.maps.Marker({
			map: map,
			position: geolocate,
			icon:"http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
		});
		var infoWindow = new google.maps.InfoWindow({
			content: "Buradasiniz"
		});
		marker.addListener('click', function() {
			infoWindow.open(marker.get('map'), marker);
		});
	},function (error){

		console.log("failedGeolocation");
		console.log(error);
	});


    $('#map-canvas').css('height', getMapPageHeight()-50 );
    $('#map-canvas').css('width', getMapPageWidth());

    google.maps.event.trigger(map, 'resize');

    console.log('map resized!!!!');
}
//$('map-page').on("pageshow", function() {
//    var latLng = map.getCenter();
//    google.maps.event.trigger(map,'resize');
//    map.setCenter(latLng);
//});

function getMapPageHeight(){
    return  $(window).height();
}
function getMapPageWidth(){
    return  $(window).width();
}

google.maps.event.addDomListener(window, 'load', initializeGoogleMap);

//google.maps.event.addDomListener(window, 'load', initialize);