
<%@include file="include.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<%@include file="header.jsp"%>

<div class="page-content-main" id="idPageContent">

	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<%@include file="sidebar.jsp"%>
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<!-- BEGIN PAGE HEADER-->
				<!-- BEGIN PAGE HEAD -->
				<div class="page-head">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>
							Ürün Yönetimi <small> Ürün Alış-Satış İşlemleri</small>
						</h1>
					</div>
					<!-- END PAGE TITLE -->
					<!-- BEGIN PAGE TOOLBAR -->

					<!-- END PAGE TOOLBAR -->
				</div>
				<!-- END PAGE HEAD -->
				<!-- BEGIN PAGE BREADCRUMB -->
				<ul class="page-breadcrumb breadcrumb">
					<!-- 					<li><a href="/depotakip/dashboard">Ana Sayfa</a> <i -->
					<!-- 						class="fa fa-circle"></i></li> -->
					<li><a href="/depotakip/islemList">Ürün Alış-Satış
							Yönetimi</a> <i class="fa fa-circle"></i></li>

				</ul>
				<!-- END PAGE BREADCRUMB -->
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-edit"></i>Ürün Alış-Satış Tablosu
								</div>
								<div class="tools">
									<!-- 									<a href="javascript:;" class="collapse" data-original-title="" -->
									<!-- 										title=""> </a> -->
									<a href="javascript:;" class="reload" data-original-title=""
										title=""> </a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="table-toolbar">
									<div class="row">


										<c:choose>
											<c:when test="${ fromDeletedIslem }">

												<c:if test="${ isDeletedIslem}">
													<div class="alert alert-Success fade in">
														<i class="fa-lg fa fa-check"></i>
														<p style="display: inline-block;">Ürün Alış
															Bilgileriniz Başarıyla Silindi.</p>
														<button type="button" class="close" data-dismiss="alert"
															aria-hidden="true"></button>
													</div>
												</c:if>

												<c:if test="${ !isDeletedIslem }">
													<div class="alert alert-Danger fade in">
														<i class="fa-lg fa fa-warning"></i>
														<p style="display: inline-block;">İşlem Başarısız</p>
														<button type="button" class="close" data-dismiss="alert"
															aria-hidden="true"></button>
													</div>
												</c:if>
											</c:when>



										</c:choose>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-10"></div>
												<!-- 										<div class="col-md-2 "></div> -->


												<div class="col-md-2">
													<div class="btn-group">

														<button type="button" class="btn green createIslemButton"
															data-toggle="modal">YENİ İŞLEM</button>

													</div>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-12">
												<form:form class="create-material-form" method="post"
													action="searchIslem" modelAttribute="islemSearchBean">
													<div class="form-body">
														<div
															class="form-group form-md-line-input form-md-floating-label has-success col-md-2">
															<select class="form-control" id="materialId"
																name="materialId" required="required">

																<option value="0">Ürün Seçiniz..</option>

																<c:forEach var="material" items="${materialList}">
																	<option value="${material.materialId}"
																		${material.materialId == selectedMaterial ? 'selected="selected"' : ''}>${material.malzeme}</option>

																	<%-- 																	<option value="${material.materialId}">${material.malzeme}</option> --%>

																</c:forEach>
															</select>
														</div>


														<div
															class="form-group form-md-line-input form-md-floating-label has-success col-md-2">
															<select class="form-control" id="islemTuru"
																name="islemTuru" required="required">
																<option value="0">İşlem Türü Seçiniz..</option>
																<option value="1"
																	${selectedIslemTuru == 1 ? 'selected="selected"' : ''}>ALIŞ</option>
																<option value="2"
																	${selectedIslemTuru == 2 ? 'selected="selected"' : ''}>SATIŞ</option>
															</select>
														</div>


														<div
															class="form-group form-md-line-input form-md-floating-label has-success col-md-2">
															<input type="date" class="form-control" id="tarih1"
																name="tarih1" required="required"
																value=${empty selectedTarih1 ? '' : selectedTarih1} />
															<label for="tarih1">Başlangıç Tarihi</label>
														</div>

														<div
															class="form-group form-md-line-input form-md-floating-label has-success col-md-2">
															<input type="date" class="form-control" id="tarih2"
																name="tarih2" required="required"
																value=${empty selectedTarih2 ? '' : selectedTarih2} />
															<label for="tarih2">Bitiş Tarihi</label>
														</div>

														<div class="form-actions col-md-2"
															style="margin-top: 20px;">
															<button id="stopajButton" type="button"
																class="btn red stopajButton">Stopaj Yazdır</button>
														</div>

														<div class="form-actions col-md-2"
															style="margin-top: 20px;">

															<button type="submit" class="btn blue">Arama Yap</button>
														</div>

													</div>


												</form:form>
											</div>


										</div>
									</div>

									<div class="row">
										<div class="col-md-10">
											<div
												class="form-group form-md-line-input form-md-floating-label has-success col-md-2">
												<input type="text" class="form-control"
													id="totalAlisMiktari" name="totalAlisMiktari"
													value="${totalAlisMiktari}" /> <label
													for="totalAlisMiktari">Toplam Alış</label>
											</div>

											<div
												class="form-group form-md-line-input form-md-floating-label has-success col-md-2">
												<input type="text" class="form-control"
													id="totalSatisMiktari" name="totalSatisMiktari"
													value="${totalSatisMiktari}" /> <label
													for="totalSatisMiktari">Toplam Satis</label>
											</div>

										</div>


										<%-- 										<form:form class="export-pdf-form" method="post" --%>
										<%-- 											action="exportToPdf" modelAttribute="islemList"> --%>
										<!-- 											<div class="form-body"> -->

										<!-- 												<div class="form-actions col-md-2" style="margin-top: 20px;"> -->

										<!-- 													<button id="exportToPdfButton" type="submit" -->
										<!-- 														class="btn blue">PDF Oluştur</button> -->
										<!-- 												</div> -->

										<!-- 											</div> -->


										<%-- 										</form:form> --%>

									</div>

								</div>
								<div id="islemTableDiv" class="dataTables_wrapper no-footer">

									<div class="table-scrollable">
										<table
											class="table table-striped table-hover table-bordered dataTable no-footer"
											id="islemTable" role="grid"
											aria-describedby="islemTable_info">
											<thead>
												<tr role="row">
													<th class="sorting" tabindex="0" aria-controls="islemTable"
														rowspan="1" colspan="1"
														aria-label="
									 tarih
								: activate to sort column ascending"
														style="width: 150px;">Tarih</th>
													<th class="sorting" tabindex="0" aria-controls="islemTable"
														rowspan="1" colspan="1"
														aria-label="
									 sirket
								: activate to sort column ascending"
														style="width: 120px;">Ürün Adı</th>
													<th class="sorting" tabindex="0" aria-controls="islemTable"
														rowspan="1" colspan="1"
														aria-label="
									randumanFarki
								: activate to sort column ascending"
														style="width: 120px;">İşlem Türü</th>
													<th class="sorting_asc" tabindex="0"
														aria-controls="islemTable" rowspan="1" colspan="1"
														aria-sort="ascending"
														aria-label="
									 malzeme
								: activate to sort column ascending"
														style="width: 274px;">Fiş No</th>

													<th class="sorting_asc" tabindex="0"
														aria-controls="islemTable" rowspan="1" colspan="1"
														aria-sort="ascending"
														aria-label="
									 tarih
								: activate to sort column ascending"
														style="width: 274px;">Miktar (Kg)</th>

													<th class="sorting_asc" tabindex="0"
														aria-controls="islemTable" rowspan="1" colspan="1"
														aria-sort="ascending"
														aria-label="
									 irsaliye
								: activate to sort column ascending"
														style="width: 274px;">Birim Fiyat (TL)</th>
													<th class="sorting" tabindex="0" aria-controls="islemTable"
														rowspan="1" colspan="1"
														aria-label="
									 kilo
								: activate to sort column ascending"
														style="width: 120px;">Toplam</th>
													<th class="sorting" tabindex="0" aria-controls="islemTable"
														rowspan="1" colspan="1"
														aria-label="
									 miktar
								: activate to sort column ascending"
														style="width: 120px;">Stopaj </th>
															<th class="sorting" tabindex="0" aria-controls="islemTable"
														rowspan="1" colspan="1"
														aria-label="
									 miktar
								: activate to sort column ascending"
														style="width: 120px;">Kdv </th>
													<th class="sorting" tabindex="0" aria-controls="islemTable"
														rowspan="1" colspan="1"
														aria-label="
									 randuman
								: activate to sort column ascending"
														style="width: 120px;">Yekun</th>

													<th class="sorting" tabindex="0" aria-controls="islemTable"
														rowspan="1" colspan="1"
														aria-label="randumanFarki
													: activate to sort column ascending"
														style="width: 120px;">Şirket Adı</th>

													<c:if test="${user.roleId == 1}">

														<th class="sorting " tabindex="0"
															aria-controls="islemTable" rowspan="1" colspan="1"
															aria-label="
									 Edit
								: activate to sort column ascending"
															style="width: 83px;">Düzenle</th>
													</c:if>
													<c:if test="${user.roleId == 1}">
														<th class="sorting " tabindex="0"
															aria-controls="islemTable" rowspan="1" colspan="1"
															aria-label="
									 Delete
								: activate to sort column ascending"
															style="width: 121px;">Sil</th>
													</c:if>
												</tr>
											</thead>
											<tbody id="tbodyid">

												<c:set var="i" scope="page" value="0" />
												<c:forEach var="islem" items="${islemList}">
													<c:choose>
														<c:when test="${i mod 2 eq 0}">
															<tr id="${islem.islemId}" class="odd">
														</c:when>
														<c:otherwise>
															<tr id="${islem.islemId}" class="even">
														</c:otherwise>
													</c:choose>
													<td>${islem.tarih}</td>
													<td><c:forEach var="material" items="${materialList}">
															<c:if test="${islem.materialId == material.materialId}">
															${material.malzeme}
															</c:if>
														</c:forEach></td>
													<td><c:if test="${islem.islemTuru == 1}">Alış</c:if> <c:if
															test="${islem.islemTuru == 2}">Satış</c:if></td>
													<td>${islem.fisNo}</td>
													<td>${islem.miktar}</td>
													<td>${islem.birimFiyat}</td>
													<td>${islem.tutar}</td>
													<td>${islem.stopaj}</td>
													<td>${islem.kdv}</td>
													<td>${islem.yekun}</td>
													<td><c:forEach var="company" items="${companyList}">
															<c:if test="${company.cId == islem.companyId}">
															${company.name}
															</c:if>
														</c:forEach></td>
													<c:if test="${user.roleId == 1}">
														<td class="center"><input type="button"
															class="btn btn-success  updateIslem"
															data-islemobj="${islem}" data-tarih="${islem.tarih}"
															value="Düzenle" /></td>
													</c:if>
													<c:if test="${user.roleId == 1}">
														<td class="center "><input type="button"
															class="btn btn-danger  deleteIslem"
															data-islemid="${islem.islemId}" value="Sil" /></td>
													</c:if>
													</tr>
													<c:set var="i" scope="page" value="${i+1}" />
												</c:forEach>
											</tbody>
										</table>
									</div>

								</div>


								<div id="createIslemModal" class="modal fade" tabindex="-1"
									data-width="400">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true"></button>
												<h3 class="modal-title">Ürün Alış - Satış Ekle</h3>
											</div>
											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
														<form:form class="create-material-form" method="post"
															action="createNewIslem" modelAttribute="islemBean">
															<div class="form-body">

																<input type="number" style="display: none"
																	class="form-control" id="islemId" name="islemId"
																	required="required" /> <label for="islemId"></label>

																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<select class="form-control materialSelect" required="required"
																		name="materialId" id="materialId">
																		<option value="0">Ürün Seçiniz..</option>
																		<c:forEach var="material" items="${materialList}">

																			<option value="${material.materialId}">${material.malzeme}</option>

																		</c:forEach>


																	</select>
																</div>

																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<select class="form-control islemTuruSelect" id="islemTuru"
																		name="islemTuru" onchange="showCompany(this);" required="required">

																		<option value="0">İşlem Türü Seçiniz..</option>
																		<option value="1">ALIŞ</option>
																		<option value="2">SATIŞ</option>

																	</select>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success companySelect" style="display:none">
																	<select class="form-control companySelect" id="companyId"
																		name="companyId" required="required">

																		<option value="0">Şirket Seçiniz..</option>
																		<c:forEach var="companyList" items="${companyList}">

																			<option value="${companyList.cId}">${companyList.name}</option>

																		</c:forEach>
																	</select>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="date" class="form-control" id="tarih"
																		name="tarih" required="required" /> <label
																		for="tarih">Tarih</label>
																</div>
																
																<!-- 																<div -->
																<!-- 																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
																<!-- 																	<select class="form-control" id="musteriId" -->
																<!-- 																		name="musteriId" required="required"> -->

																<!-- 																		<option value="0">Müşteri Seçiniz..</option> -->
																<%-- 																		<c:forEach var="musteri" items="${musteriList}"> --%>

																<%-- 																			<option value="${musteri.musteriId}">${musteri.ad}</option> --%>

																<%-- 																		</c:forEach> --%>
																<!-- 																	</select> -->
																<!-- 																</div> -->

																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" class="form-control" id="fisNo"
																		name="fisNo" required="required" /> <label
																		for="fisNo">Fiş No</label>
																</div>

																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="number" step="0.01"
																		placeholder="Miktar giriniz. Örn 12.34"
																		class="form-control" id="miktar" name="miktar"
																		required="required" /> <label for="miktar">Miktar</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="number" step="0.01"
																		placeholder="Birim Fiyat giriniz. Örn 12.30"
																		class="form-control" id="birimFiyat" name="birimFiyat"
																		required="required" /> <label for="birimFiyat">Birim
																		Fiyat</label>
																</div>

																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="number" step="0.01"
																		placeholder="Tutar giriniz. Örn 12.34"
																		class="form-control" id="tutar" name="tutar"
																		required="required" /> <label for="tutar">Tutar</label>
																</div>

<!-- 																<div -->
<!-- 																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
<!-- 																	<input type="number" step="0.01" -->
<!-- 																		placeholder="Stopaj giriniz. Örn 12.34" -->
<!-- 																		class="form-control" id="stopaj" name="stopaj" -->
<!-- 																		required="required" /> <label for="stopaj">Stopaj</label> -->
<!-- 																</div> -->

<!-- 																<div -->
<!-- 																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
<!-- 																	<input type="number" step="0.01" -->
<!-- 																		placeholder="Yekun miktarı giriniz. Örn 12.34" -->
<!-- 																		class="form-control" id="yekun" name="yekun" -->
<!-- 																		required="required" /> <label for="yekun">Yekun</label> -->
<!-- 																</div> -->

																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" placeholder="Müşteri adı giriniz."
																		class="form-control" id="mAd" name="mAd"
																		required="required" /> <label for="mAd">Müşteri
																		Ad</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" placeholder="Baba adı giriniz."
																		class="form-control" id="mBabaAd" name="mBabaAd" /> <label
																		for="mBabaAd">Baba Adı</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text"
																		placeholder="Doğum yeri adı giriniz."
																		class="form-control" id="mDogumYeri" name="mDogumYeri"
																		required="required" /> <label for="mDogumYeri">Doğum
																		Yeri</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" placeholder="Doğum tarihi giriniz."
																		class="form-control" id="mDogumTarihi"
																		name="mDogumTarihi" /> <label for="mDogumTarihi">Doğum
																		Tarihi</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" placeholder="Köy giriniz."
																		class="form-control" id="mKoy" name="mKoy" /> <label
																		for="mKoy">Köy</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type=text placeholder="Hesap No giriniz."
																		class="form-control" id="mHesapNo" name="mHesapNo" />
																	<label for="mHesapNo">Hesap No</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" placeholder="Tel no giriniz."
																		class="form-control" id="mTel" name="mTel" /> <label
																		for="mTel">Telefon</label>
																</div>



															</div>
															<div class="form-actions noborder"
																style="margin-top: 25px;">
																<button type="button" data-dismiss="modal"
																	class="btn default">İptal</button>
																<button type="submit" class="btn blue">Kaydet</button>
															</div>
														</form:form>
													</div>
												</div>

											</div>
											<!-- 											<div class="modal-footer"> -->
											<!-- 												<button type="button" data-dismiss="modal" class="btn">Close</button> -->
											<!-- 												<button type="button" class="btn blue">Create</button> -->
											<!-- 											</div> -->
										</div>
									</div>
								</div>





								<!-- 								<div id="updateIslemModal" class="modal fade" tabindex="-1" -->
								<!-- 									data-width="400"> -->
								<!-- 									<div class="modal-dialog"> -->
								<!-- 										<div class="modal-content"> -->
								<!-- 											<div class="modal-header"> -->
								<!-- 												<button type="button" class="close" data-dismiss="modal" -->
								<!-- 													aria-hidden="true"></button> -->
								<!-- 												<h3 class="modal-title">DÜZENLE</h3> -->
								<!-- 											</div> -->
								<!-- 											<div class="modal-body"> -->
								<!-- 												<div class="row"> -->
								<!-- 													<div class="col-md-12"> -->
								<%-- 														<form:form class="create-material-form" method="post" --%>
								<%-- 															action="updateIslem" modelAttribute="islemBean"> --%>
								<!-- 															<div class="form-body"> -->

								<%-- 																																<form:input type="hidden" class="form-control" --%>
								<%-- 																																	style="display:none;" id="islemId" name="id" --%>
								<%-- 																																	hidden="true" path="" /> --%>

								<!-- 																<input type="hidden" class="form-control" id="islemId" -->
								<!-- 																	name="id" required="required" /> <label for="islemId"></label> -->

								<!-- 																<div -->
								<!-- 																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
								<!-- 																	<select class="form-control" id="islemTuruU" -->
								<!-- 																		name="islemTuru" required="required"> -->

								<!-- 																		<option value="0">İşlem Türü Seçiniz..</option> -->
								<!-- 																		<option value="1">Alış</option> -->
								<!-- 																		<option value="2">Satış</option> -->

								<!-- 																	</select> -->
								<!-- 																</div> -->
								<!-- 																<div -->
								<!-- 																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
								<!-- 																	<select class="form-control" required="required" -->
								<!-- 																		name="materialId" id="materialIdU"> -->
								<!-- 																		<option value="0">Ürün Seçiniz..</option> -->
								<%-- 																		<c:forEach var="material" items="${materialList}"> --%>

								<%-- 																			<option value="${material.materialId}">${material.malzeme}</option> --%>

								<%-- 																		</c:forEach> --%>


								<!-- 																	</select> -->
								<!-- 																</div> -->


								<!-- 																<div -->
								<!-- 																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
								<!-- 																	<input type="date" class="form-control" id="tarihU" -->
								<!-- 																		name="tarih" required="required" /> <label -->
								<!-- 																		for="tarihU">Tarih</label> -->
								<!-- 																</div> -->
								<!-- 																																<div -->
								<!-- 																																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
								<!-- 																																	<select class="form-control" id="companyIdU" -->
								<!-- 																																		name="companyId" required="required"> -->

								<!-- 																																		<option value="0">Şirket Seçiniz..</option> -->
								<%-- 																																		<c:forEach var="companyList" items="${companyList}"> --%>

								<%-- 																																			<option value="${companyList.id}">${companyList.name}</option> --%>

								<%-- 																																		</c:forEach> --%>
								<!-- 																																	</select> -->
								<!-- 																																</div> -->
								<!-- 																																<div -->
								<!-- 																																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
								<!-- 																																	<select class="form-control" id="musteriIdU" -->
								<!-- 																																		name="musteriId" required="required"> -->

								<!-- 																																		<option value="0">Müşteri Seçiniz..</option> -->
								<%-- 																																		<c:forEach var="musteri" items="${musteriList}"> --%>

								<%-- 																																			<option value="${musteri.musteriId}">${musteri.ad}</option> --%>

								<%-- 																																		</c:forEach> --%>
								<!-- 																																	</select> -->
								<!-- 																																</div> -->

								<!-- 																<div -->
								<!-- 																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
								<!-- 																	<input type="text" class="form-control" id="fisNoU" -->
								<!-- 																		name="fisNo" required="required" /> <label -->
								<!-- 																		for="fisNoU">Fiş No</label> -->
								<!-- 																</div> -->

								<!-- 																<div -->
								<!-- 																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
								<!-- 																	<input type="number" step="0.01" -->
								<!-- 																		placeholder="Miktar giriniz. Örn 12.34" -->
								<!-- 																		class="form-control" id="miktarU" name="miktar" -->
								<!-- 																		required="required" /> <label for="miktar">Miktar</label> -->
								<!-- 																</div> -->
								<!-- 																<div -->
								<!-- 																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
								<!-- 																	<input type="number" step="0.01" -->
								<!-- 																		placeholder="Birim Fiyat giriniz. Örn 12.34" -->
								<!-- 																		class="form-control" id="birimFiyatU" -->
								<!-- 																		name="birimFiyat" required="required" /> <label -->
								<!-- 																		for="birimFiyatU">Birim Fiyat</label> -->
								<!-- 																</div> -->

								<!-- 																<div -->
								<!-- 																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
								<!-- 																	<input type="number" step="0.01" -->
								<!-- 																		placeholder="Tutar giriniz. Örn 12.34" -->
								<!-- 																		class="form-control" id="tutarU" name="tutar" -->
								<!-- 																		required="required" /> <label for="tutarU">Tutar</label> -->
								<!-- 																</div> -->

								<!-- 																<div -->
								<!-- 																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
								<!-- 																	<input type="number" step="0.01" -->
								<!-- 																		placeholder="Miktar giriniz. Örn 12.34" -->
								<!-- 																		class="form-control" id="stopajU" name="stopaj" -->
								<!-- 																		required="required" /> <label for="stopajU">Stopaj</label> -->
								<!-- 																</div> -->

								<!-- 																<div -->
								<!-- 																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
								<!-- 																	<input type="number" step="0.01" -->
								<!-- 																		placeholder="Toplam miktarı giriniz. Örn 12.34" -->
								<!-- 																		class="form-control" id="yekunU" name="yekun" -->
								<!-- 																		required="required" /> <label for="yekunU">Yekun</label> -->
								<!-- 																</div> -->


								<!-- 																																<div -->
								<!-- 																																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
								<!-- 																																	<input type="number" step="0.01" -->
								<!-- 																																		placeholder="Randuman giriniz. Örn 1.50.." -->
								<!-- 																																		class="form-control" id="randumanU" name="randuman" /> -->
								<!-- 																																	<label for="randuman">Randuman</label> -->
								<!-- 																																</div> -->
								<!-- 																																<div -->
								<!-- 																																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
								<!-- 																																	<input type="number" step="0.01" -->
								<!-- 																																		placeholder=" Randuman Farki" class="form-control" -->
								<!-- 																																		id="randumanFarkiU" name="randumanFarki" /> <label -->
								<!-- 																																		for="randumanFarkiU">Randuman Farkı</label> -->
								<!-- 																																</div> -->

								<!-- 																																<div -->
								<!-- 																																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
								<!-- 																																	<input type="number" step="0.01" -->
								<!-- 																																		placeholder="Toplam kilo.." class="form-control" -->
								<!-- 																																		id="kiloU" name="kilo" required="required" /> <label -->
								<!-- 																																		for="kiloU">Kilo</label> -->
								<!-- 																																</div> -->

								<!-- 															</div> -->
								<!-- 															<div class="form-actions noborder" -->
								<!-- 																style="margin-top: 25px;"> -->
								<!-- 																<button type="button" data-dismiss="modal" -->
								<!-- 																	class="btn default">İptal</button> -->
								<!-- 																<button type="submit" class="btn blue">Kaydet</button> -->
								<!-- 															</div> -->
								<%-- 														</form:form> --%>
								<!-- 													</div> -->
								<!-- 												</div> -->

								<!-- 											</div> -->

								<!-- 										</div> -->
								<!-- 									</div> -->
								<!-- 								</div> -->





								<div id="deleteIslemModal" class="modal fade" tabindex="-1"
									data-backdrop="static" data-keyboard="false">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true"></button>
												<h4 class="modal-title">Uyarı</h4>
											</div>
											<div class="modal-body">
												<p id="deleteIslemText">
													<!-- 												 Would you like to continue with some arbitrary task? -->
												</p>
											</div>
											<div class="modal-footer">
												<button type="button" data-dismiss="modal"
													class="btn default">İptal</button>
												<button id="deleteIslemButton" type="button"
													data-dismiss="modal" class="btn green">Devam et</button>
											</div>
										</div>
									</div>
								</div>




								<!-- /.modal -->
								<div id="showStopajModal" class="modal fade" tabindex="-1"
									data-backdrop="static" data-keyboard="false">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true"></button>
												<h4 class="modal-title">Bilgilendirme</h4>
											</div>
											<div class="modal-body">
												<p id="downloadFileText">

													Dosyanız oluşturulmuştur.
													<!-- 												 Would you like to continue with some arbitrary task? -->
												</p>
											</div>
											<div class="modal-footer">
												<button type="button" data-dismiss="modal" class="btn red">İptal</button>
												<button id="downloadStopajButton" type="button"
													class="btn green downloadStopajButton">İndir</button>
											</div>
										</div>
									</div>
								</div>


							</div>
							<!-- END EXAMPLE TABLE PORTLET-->
						</div>
					</div>
					<!-- END PAGE CONTENT -->
				</div>

			</div>
			<!-- END CONTENT -->
		</div>
	</div>
	<!-- END CONTAINER -->
</div>

<%@include file="footer.jsp"%>



