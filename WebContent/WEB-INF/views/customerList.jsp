
<%@include file="include.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="header.jsp"%>

<div class="page-content-main" id="idPageContent">

	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<%@include file="sidebar.jsp"%>
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<!-- BEGIN PAGE HEADER-->
				<!-- BEGIN PAGE HEAD -->
				<div class="page-head">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>
							Müşteri Yönetimi <small> Müşteri Listesi</small>
						</h1>
					</div>
					<!-- END PAGE TITLE -->
					<!-- BEGIN PAGE TOOLBAR -->

					<!-- END PAGE TOOLBAR -->
				</div>
				<!-- END PAGE HEAD -->
				<!-- BEGIN PAGE BREADCRUMB -->
				<ul class="page-breadcrumb breadcrumb">
					<!-- 					<li><a href="/depotakip/dashboard">Ana Sayfa</a> <i -->
					<!-- 						class="fa fa-circle"></i></li> -->
					<li><a href="/depotakip/fn_MaterialList">Müşteri Yönetimi</a>
						<i class="fa fa-circle"></i></li>

				</ul>
				<!-- END PAGE BREADCRUMB -->
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-edit"></i>Müşteri Listesi
								</div>
								<div class="tools">
									<a href="javascript:;" class="collapse" data-original-title=""
										title=""> </a> <a href="javascript:;" class="reload"
										data-original-title="" title=""> </a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="table-toolbar">
									<div class="row">
										<div class="col-md-3 col-sm-4 col-xs-6">
											<!-- 											<div -->
											<!-- 												class="form-group form-md-line-input form-md-floating-label has-info comboboxbirim"> -->
											<!-- 												<select class="form-control comboform" id="selectdepartmentId" -->
											<!-- 													name="departmentId" required="required"> -->
											<!-- 													<option value="">Birim Seciniz</option> -->
											<!-- 													<option value="1">BELEDIYE BASKANLIGI</option> -->
											<!-- 													<option value="2">BASKAN YARDIMCILIGI</option> -->
											<!-- 													<option value="3">BILGI ISLEM</option> -->
											<!-- 													<option value="4">ISYERI RUHSAT</option> -->
											<!-- 													<option value="5">ZABITA</option> -->
											<!-- 													<option value="6">SOSYAL ISLER</option> -->
											<!-- 													<option value="7">IMAR - YAPI RUHSAT</option> -->
											<!-- 													<option value="8">FEN ISLERI</option>													 -->
											<!-- 													<option value="0">DIGER</option> -->
											<!-- 												</select> -->
											<!-- 											</div> -->
										</div>
										<div class="col-md-6 col-sm-4 "></div>
										<div class="col-md-3 col-sm-4 col-xs-6">
											<div class="btn-group pull-right">

												<a class=" btn green" data-target="#createMusteriModal"
													data-toggle="modal"> Müşteri Ekle </a>

											</div>
										</div>

									</div>
								</div>
								<div id="sample_editable_1_wrapper"
									class="dataTables_wrapper no-footer">

									<div class="table-scrollable">
										<table
											class="table table-striped table-hover table-bordered dataTable no-footer"
											id="sample_editable_1" role="grid"
											aria-describedby="sample_editable_1_info">
											<thead>


												<tr role="row">
													<th class="sorting_asc" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-sort="ascending"
														aria-label="
									 ad
								: activate to sort column ascending"
														style="width: 274px;">Müşteri Adı</th>
													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 babaAdi
								: activate to sort column ascending"
														style="width: 120px;">Baba Adı</th>
													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 dogumYeri
								: activate to sort column ascending"
														style="width: 120px;">Doğum Yeri</th>
													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 koy
								: activate to sort column ascending"
														style="width: 120px;">Köy</th>
													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 dogumTarihi
								: activate to sort column ascending"
														style="width: 120px;">Doğum Tarihi</th>
													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 hesapNo
								: activate to sort column ascending"
														style="width: 120px;">Hesap No</th>
													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 vergiNo
								: activate to sort column ascending"
														style="width: 120px;">Vergi No</th>
													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 vergiDaire
								: activate to sort column ascending"
														style="width: 120px;">Vergi Dairesi</th>

													<c:if test="${user.roleId == 1}">


														<th class="sorting updateMusteri" tabindex="0"
															aria-controls="sample_editable_1" rowspan="1" colspan="1"
															aria-label="
									 Edit
								: activate to sort column ascending"
															style="width: 83px;">Düzenle</th>
													</c:if>
													<c:if test="${user.roleId == 1}">
														<th class="sorting deleteMusteri" tabindex="0"
															aria-controls="sample_editable_1" rowspan="1" colspan="1"
															aria-label="
									 Delete
								: activate to sort column ascending"
															style="width: 121px;">Sil</th>
													</c:if>

												</tr>

											</thead>
											<tbody id="tbodyid">

												<c:set var="i" scope="page" value="0" />
												<c:forEach var="musteri" items="${musteriList}">
													<c:choose>
														<c:when test="${i mod 2 eq 0}">
															<tr id="${musteri.musteriId}" class="odd">
														</c:when>
														<c:otherwise>
															<tr id="${musteri.musteriId}" class="even">
														</c:otherwise>
													</c:choose>
													<td>${musteri.ad}</td>
													<td>${musteri.babaAdi}</td>
													<td>${musteri.dogumYeri}</td>
													<td>${musteri.koy}</td>
													<td>${musteri.dogumTarihi}</td>
													<td>${musteri.hesapNo}</td>
													<td>${musteri.vergiNo}</td>
													<td>${musteri.vergiDaire}</td>
													<c:if test="${user.roleId == 1}">
														<td class="center"><input type="button"
															class="btn btn-success  updateMusteri"
															data-musteriid="${musteri.musteriId}"
															data-ad="${musteri.ad}" data-babaadi="${musteri.babaAdi}"
															data-dogumyeri="${musteri.dogumYeri}"
															data-koy="${musteri.koy}"
															data-dogumtarihi="${musteri.dogumTarihi}"
															data-hesapno="${musteri.hesapNo}"
															data-vergino="${musteri.vergiNo}"
															data-vergidaire="${musteri.vergiDaire}" value="Düzenle" /></td>
													</c:if>

													<c:if test="${user.roleId == 1}">

														<td class="center "><input type="button"
															class="btn btn-danger  deleteMusteri"
															data-musteriid="${musteri.musteriId}" value="Sil" /></td>
													</c:if>

													</tr>
													<c:set var="i" scope="page" value="${i+1}" />
												</c:forEach>
											</tbody>
										</table>
									</div>

								</div>
								<!-- /.modal -->
								<div id="createMusteriModal" class="modal fade" tabindex="-1"
									data-width="400">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true"></button>
												<h3 class="modal-title">Yeni Müşteri Ekle</h3>
											</div>
											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
														<form:form class="create-musteri-form" method="post"
															action="createNewMusteri" modelAttribute="musteriBean">
															<div class="form-body">

																<!-- 																<input type="hidden" class="form-control" id="musteriId" -->
																<!-- 																		name="musteriId" required="required" /> <label -->
																<!-- 																		for="musteriId"></label> -->

																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" class="form-control" id="ad"
																		name="ad" /> <label for="ad">Müşteri Adı</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" class="form-control" id="babaAdi"
																		name="babaAdi" /> <label for="babaAdi">Baba
																		Adı</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" class="form-control" id="dogumYeri"
																		name="dogumYeri" /> <label for="dogumYeri">Doğum
																		Yeri</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" placeholder="Köy Giriniz"
																		class="form-control" id="koy" name="koy" /> <label
																		for="koy">Köy</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" placeholder="yyyy-MM-dd"
																		class="form-control" id="dogumTarihi"
																		name="dogumTarihi" pattern="\d{4}-\d{1,2}-\d{1,2}" />
																	<label for="dogumTarihi">Doğum Tarihi </label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" placeholder="Hesap No Giriniz"
																		class="form-control" id="hesapNo" name="hesapNo" /> <label
																		for="hesapNo">Hesap No</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" placeholder="Vergi numarası Giriniz"
																		class="form-control" id="vergiNo" name="vergiNo" /> <label
																		for="vergiNo">Vergi No</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" placeholder="Vergi Dairesi Giriniz"
																		class="form-control" id="vergiDaire" name="vergiDaire" />
																	<label for="vergiDaire">Vergi Dairesi</label>
																</div>
															</div>
															<div class="form-actions noborder"
																style="margin-top: 25px;">
																<button type="button" data-dismiss="modal"
																	class="btn default">İptal</button>
																<button type="submit" class="btn blue">Kaydet</button>
															</div>
														</form:form>
													</div>
												</div>

											</div>
											<!-- 											<div class="modal-footer"> -->
											<!-- 												<button type="button" data-dismiss="modal" class="btn">Close</button> -->
											<!-- 												<button type="button" class="btn blue">Create</button> -->
											<!-- 											</div> -->
										</div>
									</div>
								</div>
								<div id="deleteMusteriModal" class="modal fade" tabindex="-1"
									data-backdrop="static" data-keyboard="false">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true"></button>
												<h4 class="modal-title">Uyarı</h4>
											</div>
											<div class="modal-body">
												<p id="deleteMusteriText">
													<!-- 												 Would you like to continue with some arbitrary task? -->
												</p>
											</div>
											<div class="modal-footer">
												<button type="button" data-dismiss="modal"
													class="btn default">İptal</button>
												<button id="deleteMusteriButton" type="button"
													data-dismiss="modal" class="btn green">Devam et</button>
											</div>
										</div>
									</div>
								</div>

								<div id="updateMusteriModal" class="modal fade" tabindex="-1"
									data-width="400">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true"></button>
												<h3 class="modal-title">DÜZENLE</h3>
											</div>

											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
														<form:form class="update-musteri-form" method="post"
															action="updateMusteri" modelAttribute="musteriBean">
															<div class="form-body">
															
																<input type="hidden" class="form-control"
																	id="musteriIdU" name="musteriId" required="required" />
																<label for="musteriIdU"></label>

																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" class="form-control" id="adU"
																		name="ad" /> <label for="ad">Müşteri Adı</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" class="form-control" id="babaAdiU"
																		name="babaAdi" /> <label for="babaAdiU">Baba
																		Adı</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" class="form-control" id="dogumYeriU"
																		name="dogumYeri" /> <label for="dogumYeriU">Doğum
																		Yeri</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" placeholder="Köy Giriniz"
																		class="form-control" id="koyU" name="koy" /> <label
																		for="koyU">Köy</label>
																</div>

																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text"
																		placeholder="Dogum Tarihi giriniz. Format yyyy-MM-dd"
																		class="form-control" id="dogumTarihiU"
																		name="dogumTarihi" /> <label for="dogumTarihiU">Doğum
																		Tarihi </label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" placeholder="Hesap No Giriniz"
																		class="form-control" id="hesapNoU" name="hesapNo" />
																	<label for="hesapNoU">Hesap No</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" placeholder="Vergi numarası Giriniz"
																		class="form-control" id="vergiNoU" name="vergiNo" />
																	<label for="vergiNoU">Vergi No</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" placeholder="Vergi Dairesi Giriniz"
																		class="form-control" id="vergiDaireU"
																		name="vergiDaire" /> <label for="vergiDaireU">Vergi
																		Dairesi</label>
																</div>

															</div>
															<div class="form-actions noborder"
																style="margin-top: 25px;">
																<button type="button" data-dismiss="modal"
																	class="btn default">İptal</button>
																<button type="submit" class="btn blue">Güncelle</button>
															</div>
														</form:form>
													</div>
												</div>

											</div>

										</div>
									</div>
								</div>



							</div>
							<!-- END EXAMPLE TABLE PORTLET-->
						</div>
					</div>
					<!-- END PAGE CONTENT -->
				</div>
			</div>
			<!-- END CONTENT -->
		</div>
	</div>
	<!-- END CONTAINER -->
</div>

<%@include file="footer.jsp"%>



