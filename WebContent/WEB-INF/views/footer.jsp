
<!-- BEGIN FOOTER -->
<!-- 	<div class="page-footer" style="position:fixed;"> -->
<!-- 		<div class="page-footer-inner"> -->
<!-- 			 2015 &copy; depotakip - Zeytinburnu Belediyesi <a -->
<!-- 				href="http://www.zeytinburnu.bel.tr" -->
<!-- 				title="" -->
<!-- 				target="_blank">Bilgi Islem Mudurlugu</a> -->
<!-- 		</div> -->
<!-- 		<div class="scroll-to-top"> -->
<!-- 			<i class="icon-arrow-up"></i> -->
<!-- 		</div> -->
<!-- 	</div> -->
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<c:url value="/resources/adminpanel/global/plugins/respond.min.js"/>" ></script>
<script src="<c:url value="/resources/adminpanel/global/plugins/excanvas.min.js"/>" ></script> 
<![endif]-->

<script
	src="<c:url value="/resources/adminpanel/global/plugins/jquery.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/global/plugins/jquery-migrate.min.js"/>"
	type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script
	src="<c:url value="/resources/adminpanel/global/plugins/jquery-ui/jquery-ui.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/global/plugins/bootstrap/js/bootstrap.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/global/plugins/jquery.blockui.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/global/plugins/jquery.cokie.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/global/plugins/uniform/jquery.uniform.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"/>"
	type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script
	src="<c:url value="/resources/adminpanel/global/plugins/jqvmap/jqvmap/jquery.vmap.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js"/>"
	type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
<script
	src="<c:url value="/resources/adminpanel/global/plugins/morris/morris.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/global/plugins/morris/raphael-min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/global/plugins/jquery.sparkline.min.js"/>"
	type="text/javascript"></script>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnq8hT9D82IPDAsdNuioNr1GxWG_uFcts"></script>
<!-- <script -->
<%-- 	src="<c:url value="/resources/adminpanel/global/scripts/googlemapsapi.js"/>" --%>
<!-- 	type="text/javascript"></script> -->
<script
	src="<c:url value="/resources/adminpanel/global/plugins/gmaps/gmaps.min.js"/>"
	type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script
	src="<c:url value="/resources/adminpanel/global/scripts/metronic.js"/>"
	type="text/javascript"></script>

<script
	src="<c:url value="/resources/adminpanel/admin/layout4/scripts/layout.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/admin/layout4/scripts/demo.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/admin/pages/scripts/index3.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/admin/pages/scripts/tasks.js"/>"
	type="text/javascript"></script>
<%-- <script
	src="<c:url value="/resources/adminpanel/global/scripts/adminpanel.js"/>"
	type="text/javascript"></script> --%>
	<script
	src="<c:url value="/resources/webapp/js/adminpanel.js"/>"
	type="text/javascript"></script>
	<script
	src="<c:url value="/resources/webapp/js/jquery.table2excel.js"/>"
	type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!--  <script src="../js/adminpanel.js" type="text/javascript"></script> -->

<script
	src="<c:url value="/resources/adminpanel/admin/pages/scripts/table-editable.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/adminpanel/admin/pages/scripts/maps-google.js"/>"
	type="text/javascript"></script>
<!-- END JAVASCRIPTS -->
<!-- <script -->
<%-- 	src="<c:url value="/resources/adminpanel/global/scripts/googlemaps.js"/>" --%>
<!-- 	type="text/javascript"></script> -->
	
	<script
	src="<c:url value="/resources/adminpanel/global/scripts/notification.js"/>"
	type="text/javascript"></script>
	
	<script
		src="<c:url value="/resources/adminpanel/global/scripts/password.js"/>"
		type="text/javascript"></script>
		
		<script
		src="<c:url value="/resources/adminpanel/global/scripts/birimarama.js"/>"
		type="text/javascript"></script>
		
		<script
		src="<c:url value="/resources/adminpanel/global/scripts/beskullanici.js"/>"
		type="text/javascript"></script>

</body>
<!-- END BODY -->
</html>