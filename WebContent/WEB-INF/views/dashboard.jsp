
<%@include file="include.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="header.jsp"%>


<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>


<style type="text/css">
.ortala {
	padding: 5px 0 10px 0;
	text-align: center;
}

.ortala h3, h5 {
	color: blue;
}
}
</style>

<div class="page-content-main" id="idPageContent">

	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<%@include file="sidebar.jsp"%>
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEAD -->
				<div class="page-head">
					<!-- BEGIN PAGE TITLE -->
					<div class="ortala">
						<h3>
							TORAMANLAR AKARYAKIT OTO LASTİK VE TARIM ÜRÜNLERİ TİC. ve SANAYİ
							A.Ş.
							<h3>
								<h5>ÜRÜN TAKİP PROGRAMI</h5>



								<!-- 	                     <h3> -->
								<%-- 							Hosgeldiniz <small>${loggedInUser.name} </small> <small> ${loggedInUser.surName} </small>    --%>
								<!-- 						</h3> -->
					</div>
					<div class=row>
						<div class="col-md-6">


							<div id="container1"
								style="min-width: 310px; height: 400px; max-width: 500px; margin: 0 auto"></div>
						</div>
						
					 <div class="col-md-6">
 
							<div id="container2"
								style="min-width: 310px; height: 400px; max-width: 500px; margin: 0 auto"></div>
						</div>
					</div>
					<!-- END PAGE TITLE -->
					<!-- BEGIN PAGE TOOLBAR -->

					<!-- END PAGE TOOLBAR -->
				</div>
				<!-- END PAGE HEAD -->
				<!-- BEGIN PAGE BREADCRUMB -->

				<!-- END PAGE BREADCRUMB -->
				<!-- BEGIN PAGE CONTENT INNER -->
				<!-- END PAGE CONTENT INNER -->
			</div>
			<!-- END CONTENT -->
		</div>
	</div>
	<!-- END CONTAINER -->
</div>

<%@include file="footer.jsp"%>



