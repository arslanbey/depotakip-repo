<%@include file="include.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.4
Version: 3.8.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>depotakip | Login Options</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/> -->
<link href="<c:url value="/resources/adminpanel/global/plugins/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css"/>
<link href="<c:url value="/resources/adminpanel/global/plugins/simple-line-icons/simple-line-icons.min.css"/>" rel="stylesheet" type="text/css"/>
<link href="<c:url value="/resources/adminpanel/global/plugins/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css"/>
<link href="<c:url value="/resources/adminpanel/global/plugins/uniform/css/uniform.default.css"/>" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<c:url value="/resources/adminpanel/global/plugins/select2/select2.css"/>" rel="stylesheet" type="text/css"/>
<link href="<c:url value="/resources/adminpanel/admin/pages/css/login-soft.css"/>" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="<c:url value="/resources/adminpanel/global/css/components-rounded.css"/>" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<c:url value="/resources/adminpanel/global/css/plugins.css"/>" rel="stylesheet" type="text/css"/>
<link href="<c:url value="/resources/adminpanel/admin/layout/css/layout.css"/>" rel="stylesheet" type="text/css"/>
<link id="style_color" href="<c:url value="/resources/adminpanel/admin/layout/css/themes/default.css"/>" rel="stylesheet" type="text/css"/>
<link href="<c:url value="/resources/adminpanel/admin/layout/css/custom.css"/>" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
<!-- 	<a href="index.html"> -->
<%-- 	<img src="<c:url value="/resources/adminpanel/admin/layout4/img/logo-big.png"/>"  --%>
<!-- 	alt=""/> -->
<!-- 	</a> -->
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<form:form class="login-form" method="post" action="login" modelAttribute="loginBean">
		<h3 class="form-title center">MOBIL SAHA</h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span>
			Kullanici adı,sifre ve mail giriniz. </span>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Username</label>
			<div class="input-icon">
				<i class="fa fa-user"></i>
				<form:input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" path=""/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
				<form:input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" path=""/>
			</div>
		</div>
		
		
		
		<div class="form-actions">
<!-- 			<label class="checkbox"> -->
<!-- 			<input type="checkbox" name="remember" value="1"/> Remember me </label> -->
			<button type="submit" class="btn blue pull-right">
			Giris <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
 
	</form:form>
	<!-- END LOGIN FORM -->
 
 
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	 2015 &copy; depotakip - Zeytinburnu Belediyesi / Bilgi Islem Mudurlugu.
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<c:url value="/resources/adminpanel/global/plugins/respond.min.js"/>" ></script>
<script src="<c:url value="/resources/adminpanel/global/plugins/excanvas.min.js"/>" ></script> 
<![endif]-->
<script src="<c:url value="/resources/adminpanel/global/plugins/jquery.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/adminpanel/global/plugins/jquery-migrate.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/adminpanel/global/plugins/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/adminpanel/global/plugins/jquery.blockui.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/adminpanel/global/plugins/uniform/jquery.uniform.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/adminpanel/global/plugins/jquery.cokie.min.js"/>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<c:url value="/resources/adminpanel/global/plugins/jquery-validation/js/jquery.validate.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/adminpanel/global/plugins/backstretch/jquery.backstretch.min.js"/>" type="text/javascript"></script>
<script type="text/javascript" src="<c:url value="/resources/adminpanel/global/plugins/select2/select2.min.js"/>" ></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<c:url value="/resources/adminpanel/global/scripts/metronic.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/adminpanel/admin/layout/scripts/layout.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/adminpanel/admin/layout/scripts/demo.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/adminpanel/admin/pages/scripts/login-soft.js"/>" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {     
  Metronic.init(); // init metronic core components
  Layout.init(); // init current layout
  Login.init();
  Demo.init();
       // init background slide images
       $.backstretch([
        "<c:url value='/resources/adminpanel/admin/pages/media/bg/1.jpg'/>",
        "<c:url value='/resources/adminpanel/admin/pages/media/bg/2.jpg'/>",
        "<c:url value='/resources/adminpanel/admin/pages/media/bg/3.jpg'/>",
        "<c:url value='/resources/adminpanel/admin/pages/media/bg/4.jpg'/>"
        ], {
          fade: 1000,
          duration: 8000
    }
    );
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>