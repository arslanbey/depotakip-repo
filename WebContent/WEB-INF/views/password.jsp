
<%@include file="include.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="header.jsp"%>

<div class="page-content-main" id="idPageContent">

	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<%@include file="sidebar.jsp"%>
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<!-- BEGIN PAGE HEADER-->
				<!-- BEGIN PAGE HEAD -->
				<div class="page-head">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>
							Şifre Ayarları <small> </small>
						</h1>
					</div>
					<!-- END PAGE TITLE -->
					<!-- BEGIN PAGE TOOLBAR -->

					<!-- END PAGE TOOLBAR -->
				</div>
				<!-- END PAGE HEAD -->
				<!-- BEGIN PAGE BREADCRUMB -->
				<ul class="page-breadcrumb breadcrumb">
					<!-- 					<li><a href="/depotakip/dashboard">Ana Sayfa</a> <i -->
					<!-- 						class="fa fa-circle"></i></li> -->
					<li><a href="/depotakip/profil">Şifre Ayarları</a> <i
						class="fa fa-circle"></i></li>

				</ul>
				<!-- END PAGE BREADCRUMB -->
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
					<div id="deneme"></div>
					<form:form class="create-user-form" method="post"
									action="changePassword" >
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-edit"></i>Şifre
								</div>
								<div class="tools">
									<a href="javascript:;" class="collapse" data-original-title=""
										title=""> </a> <a href="javascript:;" class="reload"
										data-original-title="" title=""> </a>
								</div>
							</div>

							<div class="portlet-body">
							<input type="number" class="form-control"
										style="display:none;" id="userId" name="userId" hidden="true" value=""
										/>

								<div style="margin: 5px;">
									<label class="profil" for="surName">Eski Şifre</label> <input
										type="password" class="form-control textboxx" id="oldpassword"
										name="oldpassword" required="required"  />
								</div>
								<div style="margin: 5px;">
									<label class="profil" for="surName">Yeni Şifre</label> <input
										type="password" class="form-control textboxx" id="newpassword"
										name="newpassword" required="required"  />
								</div>
								<div style="margin: 5px;">
									<label class="profil" for="surName">Yeni Şifre(Tekrar)</label> <input
										type="password" class="form-control textboxx" id="renewpassword"
										name="renewpassword" required="required"   />
								</div>
						
						



								<div class="form-actions noborder" style="margin-top: 25px;">
<!-- 									<button type="button" data-dismiss="modal" class="btn default">İptal</button> -->
									<input type="button"
														class="btn blue  changePassword"  value="Değiştir" />
								</div>



							</div>
						</div>
						</form:form>
					</div>
					
								<div id="changePasswordModal" class="modal fade" tabindex="-1"
									data-backdrop="static" data-keyboard="false">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true"></button>
												<h4 class="modal-title">Confirmation</h4>
											</div>
											<div class="modal-body">
												<p id="changePasswordText">
																									 
												</p>
											</div>
											<div class="modal-footer">
												<button type="button" data-dismiss="modal"
													class="btn default">Cancel</button>
												<button id="changePasswordButton" type="button"
													data-dismiss="modal" class="btn green">Continue</button>
											</div>
										</div>
									</div>
								</div>
								

				</div>
			</div>
			<!-- END CONTENT -->
		</div>
	</div>
	<!-- END CONTAINER -->
</div>

<%@include file="footer.jsp"%>



