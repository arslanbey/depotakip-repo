<%@include file="include.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
	<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
	<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
		<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
		<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
		<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->

		<c:choose>
			<c:when test="${user.departmentId==0 }">
				<!-- Depo işleri kullanıcı girişi -->
				<ul class="page-sidebar-menu " data-keep-expanded="false"
					data-auto-scroll="true" data-slide-speed="200">
					<li><a href="/depotakip/dashboard" id="fnDashboardPage">
							<i class="icon-home"></i> <span class="title">Anasayfa</span>
					</a></li>

					<li><a href="javascript:;" id="fnUserManagementPage"> <i
							class="fa fa-user-md"></i> <span class="title">Profil
								Yönetimi</span>
					</a>
						<ul class="sub-menu">
							<li><a href="/depotakip/profile"> <i class="fa fa-user"></i>
									Kişisel Bilgiler
							</a></li>
							<li><a href="/depotakip/password"> <i
									class="fa fa-unlock-alt"></i> Şifre Değiştir
							</a></li>

						</ul></li>
					<!-- 					<li><a href="javascript:;" id="fnStockManagementPage"> <i -->
					<!-- 							class="fa fa-users"></i> <span class="title">Müşteri -->
					<!-- 								Yönetimi</span> -->
					<!-- 					</a> -->
					<!-- 						<ul class="sub-menu"> -->
					<!-- 							<li><a href="/depotakip/musteriList"> <i -->
					<!-- 									class="fa fa-unlock-alt"></i> Müşteri Bilgileri -->
					<!-- 							</a></li> -->

					<!-- 						</ul></li> -->

					<li><a href="javascript:;" id="fnStockManagementPage"> <i
							class="fa fa-diamond"></i> <span class="title">Ürün
								Yönetimi</span>
					</a>
						<ul class="sub-menu">
							<li><a href="/depotakip/fn_MaterialList"> <i
									class="fa fa-unlock-alt"></i> Ürün Bilgileri
							</a></li>

						</ul></li>

					<li><a href="javascript:;" id="fnCompanyManagementPage"> <i
							class="fa fa-building-o"></i> <span class="title">Tedarikçi
								Yönetimi</span>
					</a>
						<ul class="sub-menu">
							<li><a href="/depotakip/fn_CompanyManagement"> <i
									class="fa fa-building"></i> Şirket Listesi
							</a></li>

						</ul></li>

					<li><a href="javascript:;" id="fnStockManagementPage"> <i
							class="fa fa-archive"></i> <span class="title">İşlemler</span>
					</a>
						<ul class="sub-menu">

							<li><a href="/depotakip/islemList"> <i
									class="fa fa-money"></i> Ürün Alış - Satış
							</a></li>

							<!-- 							<li><a href="/depotakip/materialStockList"> <i -->
							<!-- 									class="fa fa-truck"></i> Ürün Satış -->
							<!-- 							</a></li> -->

						</ul></li>
				 
				</ul>
			</c:when>
			<c:otherwise>
				<ul class="page-sidebar-menu " data-keep-expanded="false"
					data-auto-scroll="true" data-slide-speed="200">
					<li><a href="/depotakip/dashboard" id="dashboardPage"> <i
							class="icon-home"></i> <span class="title">Anasayfa</span>
					</a></li>

					<li><a href="javascript:;" id="userManagementPage"> <i
							class="fa fa-cog"></i> <span class="title">Profil Yönetimi</span>
					</a>
						<ul class="sub-menu">
							<li><a href="/depotakip/profile"> <i class="fa fa-cog"></i>
									Kişisel Bilgiler
							</a></li>
							<li><a href="/depotakip/password"> <i
									class="fa fa-unlock-alt"></i> Şifre Değiştir
							</a></li>

						</ul></li>
					<c:if test="${user.roleId==1}">
						<li><a href="/depotakip/users" id="userManagementPage"> <i
								class="icon-users"></i> <span class="title">Kullanıcı
									Yönetimi</span>
						</a></li>
					</c:if>
			 
				</ul>

			</c:otherwise>
		</c:choose>

		<!-- END SIDEBAR MENU -->
	</div>
</div>
<!-- 			</div> -->
<!-- END SIDEBAR -->