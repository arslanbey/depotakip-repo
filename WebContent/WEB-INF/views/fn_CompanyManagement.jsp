
<%@include file="include.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="header.jsp"%>

<div class="page-content-main" id="idPageContent">

	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<%@include file="sidebar.jsp"%>
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<!-- BEGIN PAGE HEADER-->
				<!-- BEGIN PAGE HEAD -->
				<div class="page-head">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>
							Şirket Yönetimi <small> Şirket Listesi</small>
						</h1>
					</div>
					<!-- END PAGE TITLE -->
					<!-- BEGIN PAGE TOOLBAR -->

					<!-- END PAGE TOOLBAR -->
				</div>
				<!-- END PAGE HEAD -->
				<!-- BEGIN PAGE BREADCRUMB -->
				<ul class="page-breadcrumb breadcrumb">
					<!-- 					<li><a href="/depotakip/dashboard">Ana Sayfa</a> <i -->
					<!-- 						class="fa fa-circle"></i></li> -->
					<li><a href="/depotakip/fn_CompanyManagement">Şirket
							Yönetimi</a> <i class="fa fa-circle"></i></li>

				</ul>
				<!-- END PAGE BREADCRUMB -->
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-edit"></i>Şirket Tablosu
								</div>
								<div class="tools">
									<a href="javascript:;" class="collapse" data-original-title=""
										title=""> </a> <a href="javascript:;" class="reload"
										data-original-title="" title=""> </a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="table-toolbar">
									<div class="row">
										<div class="col-md-3 col-sm-4 col-xs-6">
											<!-- 											<div -->
											<!-- 												class="form-group form-md-line-input form-md-floating-label has-info comboboxbirim"> -->
											<!-- 												<select class="form-control comboform" id="selectdepartmentId" -->
											<!-- 													name="departmentId" required="required"> -->
											<!-- 													<option value="">Birim Seciniz</option> -->
											<!-- 													<option value="1">BELEDIYE BASKANLIGI</option> -->
											<!-- 													<option value="2">BASKAN YARDIMCILIGI</option> -->
											<!-- 													<option value="3">BILGI ISLEM</option> -->
											<!-- 													<option value="4">ISYERI RUHSAT</option> -->
											<!-- 													<option value="5">ZABITA</option> -->
											<!-- 													<option value="6">SOSYAL ISLER</option> -->
											<!-- 													<option value="7">IMAR - YAPI RUHSAT</option> -->
											<!-- 													<option value="8">FEN ISLERI</option>													 -->
											<!-- 													<option value="0">DIGER</option> -->
											<!-- 												</select> -->
											<!-- 											</div> -->
										</div>
										<div class="col-md-6 col-sm-4 "></div>
										<div class="col-md-3 col-sm-4 col-xs-6">
											<div class="btn-group pull-right">

												<a class=" btn green" data-target="#createCompanyModal"
													data-toggle="modal"> Şirket Ekle </a>

											</div>
										</div>

									</div>
								</div>
								<div id="sample_editable_1_wrapper"
									class="dataTables_wrapper no-footer">

									<div class="table-scrollable">
										<table
											class="table table-striped table-hover table-bordered dataTable no-footer"
											id="sample_editable_1" role="grid"
											aria-describedby="sample_editable_1_info">
											<thead>
												<tr role="row">
													<th class="sorting_asc" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-sort="ascending"
														aria-label="
									 malzeme
								: activate to sort column ascending"
														style="width: 274px;">Şirket Adı</th>


													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 Edit
								: activate to sort column ascending"
														style="width: 83px;">Düzenle</th>
													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 Delete
								: activate to sort column ascending"
														style="width: 121px;">Sil</th>
												</tr>
											</thead>
											<tbody id="tbodyid">

												<c:set var="i" scope="page" value="0" />
												<c:forEach var="company" items="${companyList}">
													<c:choose>
														<c:when test="${i mod 2 eq 0}">
															<tr id="${company.cId}" class="odd">
														</c:when>
														<c:otherwise>
															<tr id="${company.cId}" class="even">
														</c:otherwise>
													</c:choose>
													<td>${company.name}</td>

													<td class="center"><input type="button"
														class="btn btn-success  updateCompany"
														data-company="${company.cId}"
														data-companyname="${company.name}" value="Düzenle" /></td>
													<td class="center "><input type="button"
														class="btn btn-danger  deleteCompany"
														data-company="${company.cId}"
														data-companyName="${company.name}" value="Sil" /></td>
													</tr>
													<c:set var="i" scope="page" value="${i+1}" />
												</c:forEach>
											</tbody>
										</table>
									</div>

								</div>
								<!-- /.modal -->
								<div id="createCompanyModal" class="modal fade" tabindex="-1"
									data-width="400">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true"></button>
												<h3 class="modal-title">Şirket Ekle</h3>
											</div>
											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
														<form:form class="create-company-form" method="post"
															action="addFnCompany" modelAttribute="companyBean">
															<div class="form-body">
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">


																	<input type="text" class="form-control" id="name"
																		name="name" required="required" /> <label for="name">Şirket
																		Adı</label>
																</div>


															</div>
															<div class="form-actions noborder"
																style="margin-top: 25px;">
																<button type="button" data-dismiss="modal"
																	class="btn default">İptal</button>
																<button type="submit" class="btn blue">Kaydet</button>
															</div>
														</form:form>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div id="deleteCompanyModal" class="modal fade" tabindex="-1"
									data-backdrop="static" data-keyboard="false">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true"></button>
												<h4 class="modal-title">Uyarı</h4>
											</div>
											<div class="modal-body">
												<p id="deleteCompanyText">
													<!-- 												 Would you like to continue with some arbitrary task? -->
												</p>
											</div>
											<div class="modal-footer">
												<button type="button" data-dismiss="modal"
													class="btn default">İptal</button>
												<button id="deleteCompanyButton" type="button"
													data-dismiss="modal" class="btn green">Devam et</button>
											</div>
										</div>
									</div>
								</div>

								<div id="updateCompanyModal" class="modal fade" tabindex="-1"
									data-width="400">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true"></button>
												<h3 class="modal-title">DÜZENLE</h3>
											</div>
											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
														<form:form class="update-company-form" method="post"
															action="updateCompany" modelAttribute="companyBean">
															<div class="form-body">
																<form:input type="number" class="form-control"
																	style="display:none;" id="companyId" name="id" hidden="true"
																	path="" />
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<form:input type="text" class="form-control"
																		id="companyname" name="name" required="required"
																		path="" />
																	<label for="companyname">Şirket Adi</label>
																</div>
															</div>

															<div class="form-actions noborder"
																style="margin-top: 25px;">
																<button type="submit" class="btn blue">Değiştir</button>
															</div>

														</form:form>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
							<!-- END EXAMPLE TABLE PORTLET-->
						</div>
					</div>
					<!-- END PAGE CONTENT -->
				</div>
			</div>
			<!-- END CONTENT -->
		</div>
	</div>
	<!-- END CONTAINER -->
</div>

<%@include file="footer.jsp"%>



