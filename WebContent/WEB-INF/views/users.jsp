
<%@include file="include.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="header.jsp"%>

<div class="page-content-main" id="idPageContent">

	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<%@include file="sidebar.jsp"%>
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<!-- BEGIN PAGE HEADER-->
				<!-- BEGIN PAGE HEAD -->
				<div class="page-head">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>
							Kullanıcı Yönetimi <small> Kullanici Listesi</small>
						</h1>
					</div>
					<!-- END PAGE TITLE -->
					<!-- BEGIN PAGE TOOLBAR -->

					<!-- END PAGE TOOLBAR -->
				</div>
				<!-- END PAGE HEAD -->
				<!-- BEGIN PAGE BREADCRUMB -->
				<ul class="page-breadcrumb breadcrumb">
					<!-- 					<li><a href="/depotakip/dashboard">Ana Sayfa</a> <i -->
					<!-- 						class="fa fa-circle"></i></li> -->
					<li><a href="/depotakip/users">Kullanıcı Yönetimi</a> <i
						class="fa fa-circle"></i></li>

				</ul>
				<!-- END PAGE BREADCRUMB -->
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-edit"></i>Kullanıcı Tablosu
								</div>
								<div class="tools">
									<a href="javascript:;" class="collapse" data-original-title=""
										title=""> </a> <a href="javascript:;" class="reload"
										data-original-title="" title=""> </a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="table-toolbar">
									<div class="row">
										<div class="col-md-3 col-sm-4 col-xs-6">
											<div
												class="form-group form-md-line-input form-md-floating-label has-info comboboxbirim">
												<select class="form-control comboform" id="selectdepartmentId"
													name="departmentId" required="required">
													<option value="">Birim Seciniz</option>
													<option value="1">BELEDIYE BASKANLIGI</option>
													<option value="2">BASKAN YARDIMCILIGI</option>
													<option value="3">BILGI ISLEM</option>
													<option value="4">ISYERI RUHSAT</option>
													<option value="5">ZABITA</option>
													<option value="6">SOSYAL ISLER</option>
													<option value="7">IMAR - YAPI RUHSAT</option>
													<option value="8">FEN ISLERI</option>													
													<option value="0">DIGER</option>
												</select>
											</div>
										</div>
										<div class="col-md-6 col-sm-4 "></div>
										<div class="col-md-3 col-sm-4 col-xs-6">
											<div class="btn-group pull-right">
												<!-- 												<button id="sample_editable_1_new" class="btn green"> -->
												<!-- 													Add New <i class="fa fa-plus"></i> -->
												<!-- 												</button> -->
												<a class=" btn green" data-target="#createUserModal"
													data-toggle="modal"> Kullanıcı Ekle </a>

											</div>
										</div>




									</div>
								</div>
								<div id="sample_editable_1_wrapper"
									class="dataTables_wrapper no-footer">

									<div class="table-scrollable">
										<table
											class="table table-striped table-hover table-bordered dataTable no-footer"
											id="sample_editable_1" role="grid"
											aria-describedby="sample_editable_1_info">
											<thead>
												<tr role="row">
													<th class="sorting_asc" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-sort="ascending"
														aria-label="
									 Username
								: activate to sort column ascending"
														style="width: 274px;">Kullanıcı Adı</th>
													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 Name
								: activate to sort column ascending"
														style="width: 120px;">Ad</th>
													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 SurName
								: activate to sort column ascending"
														style="width: 120px;">Soyad</th>
													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 role activate to sort column ascending"
														style="width: 120px;">Rol</th>
													<!-- 													<th class="sorting" tabindex="0" -->
													<!-- 														aria-controls="sample_editable_1" rowspan="1" colspan="1" -->
													<!-- 														aria-label=" -->
													<!-- 									 Points -->
													<!-- 								: activate to sort column ascending" -->
													<!-- 														style="width: 119px;">Points</th> -->
													<!-- 													<th class="sorting" tabindex="0" -->
													<!-- 														aria-controls="sample_editable_1" rowspan="1" colspan="1" -->
													<!-- 														aria-label=" -->
													<!-- 									 Notes -->
													<!-- 								: activate to sort column ascending" -->
													<!-- 														style="width: 152px;">Notes</th> -->
													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 Edit
								: activate to sort column ascending"
														style="width: 83px;">Düzenle</th>
													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 Delete
								: activate to sort column ascending"
														style="width: 121px;">Sil</th>
												</tr>
											</thead>
											<tbody id="tbodyid">

												<c:set var="i" scope="page" value="0" />
												<c:forEach var="user" items="${userList}">
													<c:choose>
														<c:when test="${i mod 2 eq 0}">
															<tr id="${user.id}" class="odd">
														</c:when>
														<c:otherwise>
															<tr id="${user.id}" class="even">
														</c:otherwise>
													</c:choose>
													<td>${user.userName}</td>
													<td>${user.name}</td>
													<td>${user.surName}</td>
													<c:if test="${user.roleId == 1}">
														<td>Yönetici</td>
													</c:if>
													<c:if test="${user.roleId == 2}">
														<td>Kullanıcı</td>
													</c:if>

													<td class="center"><input type="button"
														class="btn btn-success  editUser" data-userid="${user.id}"
														data-userName="${user.userName}" data-name="${user.name}"
														data-surname="${user.surName}"
														data-roleid="${user.roleId}"
														data-departmant="${user.departmentId}" value="Düzenle" /></td>
													<td class="center "><input type="button"
														class="btn btn-danger  deleteUser" data-user="${user.id}"
														data-userName="${user.userName}" value="Sil" /></td>
													</tr>
													<c:set var="i" scope="page" value="${i+1}" />
												</c:forEach>
											</tbody>
										</table>
									</div>

								</div>
								<!-- /.modal -->
								<div id="createUserModal" class="modal fade" tabindex="-1"
									data-width="400">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true"></button>
												<h3 class="modal-title">KULLANICI EKLE</h3>
											</div>
											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
														<form:form class="create-user-form" method="post"
															action="createNewUser" modelAttribute="userBean">
															<div class="form-body">
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" class="form-control" id="name"
																		name="name" required="required" /> <label for="name">Ad</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" class="form-control" id="surName"
																		name="surName" required="required" /> <label
																		for="surName">Soyad</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" class="form-control" id="userName"
																		name="userName" required="required" /> <label
																		for="userName">Kullanıcı Adi</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="password" class="form-control"
																		id="password" name="password" required="required" />
																	<label for="password">Sifre</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-info">
																	<select class="form-control" id="departmentId"
																		name="departmentId" required="required">
																		<option value="">Birim Seciniz</option>
																		<option value="1">BELEDIYE BASKANLIGI</option>
																		<option value="2">BASKAN YARDIMCILIGI</option>
																		<option value="3">BILGI ISLEM</option>
																		<option value="4">ISYERI RUHSAT</option>
																		<option value="5">ZABITA</option>
																		<option value="6">SOSYAL ISLER</option>
																		<option value="7">IMAR - YAPI RUHSAT</option>	
																		<option value="8">FEN ISLERI</option>													
																		<option value="0">DIGER</option>
																	</select>
																</div>

																<div
																	class="form-group form-md-line-input form-md-floating-label has-info">
																	<select class="form-control" id="roleId" name="roleId"
																		required="required">
																		<option value="">Role Seciniz</option>
																		<option value="1">Yönetici</option>
																		<option value="2">Kullanici</option>
																	</select>
																</div>
															</div>
															<div class="form-actions noborder"
																style="margin-top: 25px;">
																<button type="button" data-dismiss="modal"
																	class="btn default">İptal</button>
																<button type="submit" class="btn blue">Tamam</button>
															</div>
														</form:form>
													</div>
												</div>

											</div>
											<!-- 											<div class="modal-footer"> -->
											<!-- 												<button type="button" data-dismiss="modal" class="btn">Close</button> -->
											<!-- 												<button type="button" class="btn blue">Create</button> -->
											<!-- 											</div> -->
										</div>
									</div>
								</div>
								<div id="deleteUserModal" class="modal fade" tabindex="-1"
									data-backdrop="static" data-keyboard="false">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true"></button>
												<h4 class="modal-title">Uyarı</h4>
											</div>
											<div class="modal-body">
												<p id="deleteUserText">
													<!-- 												 Would you like to continue with some arbitrary task? -->
												</p>
											</div>
											<div class="modal-footer">
												<button type="button" data-dismiss="modal"
													class="btn default">İptal</button>
												<button id="deleteUserButtonUsers" type="button"
													data-dismiss="modal" class="btn green">Devam et</button>
											</div>
										</div>
									</div>
								</div>



								<div id="editUserModal" class="modal fade" tabindex="-1"
									data-width="400">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true"></button>
												<h3 class="modal-title">DÜZENLE</h3>
											</div>
											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
														<form:form class="create-user-form" method="post"
															action="editUser" modelAttribute="userBean">
															<div class="form-body">

																<form:input type="number" class="form-control"
																	style="display:none;" id="userId" name="userId"
																	hidden="true" path="" />


																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<form:input type="text" class="form-control"
																		id="uUserName" name="userName" required="required"
																		path="" />
																	<label for="uUserName">Kullanıcı Adi</label>
																</div>


																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<form:input type="text" class="form-control" id="uname"
																		name="name" required="required" path="" />
																	<label for="name">Ad</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<form:input type="text" class="form-control"
																		id="surname" name="surName" required="required"
																		path="" />
																	<label for="surName">Soyad</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-info">
																	<select class="form-control" id="usersDepartmentId"
																		name="departmentId" required="required">
																		<option value="">Birim Seciniz</option>
																		<option value="1">BELEDIYE BASKANLIGI</option>
																		<option value="2">BASKAN YARDIMCILIGI</option>
																		<option value="3">BILGI ISLEM</option>
																		<option value="4">ISYERI RUHSAT</option>
																		<option value="5">ZABITA</option>
																		<option value="6">SOSYAL ISLER</option>
																		<option value="7">IMAR - YAPI RUHSAT</option>	
																		<option value="8">FEN ISLERI</option>													
																		<option value="0">DIGER</option>



																	</select>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-info">
																	<select class="form-control" id="usersRoleid" name="roleId"
																		required="required">
																		<option value="">Role Seciniz</option>
																		<option value="1">Yönetici</option>
																		<option value="2">Kullanici</option>
																	</select>
																</div>

																<!-- 																<div -->
																<!-- 																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
																<!-- 																	<input type="password" class="form-control" -->
																<!-- 																		id="password" name="password" required="required" /> -->
																<!-- 																	<label for="password">Sifre</label> -->
																<!-- 																</div> -->


																<!-- 																<div -->
																<!-- 																	class="form-group form-md-line-input form-md-floating-label has-info"> -->
																<!-- 																	<select class="form-control" id="roleId" name="roleId" -->
																<!-- 																		required="required"> -->
																<!-- 																		<option value="">Role Seciniz</option> -->
																<!-- 																		<option value="1">Yönetici</option> -->
																<!-- 																		<option value="2">Kullanici</option> -->
																<!-- 																	</select> -->
																<!-- 																</div> -->
															</div>
															<div class="form-actions noborder"
																style="margin-top: 25px;">
																<!-- 																<button type="button" data-dismiss="modal" -->
																<!-- 																	class="btn default">İptal</button> -->
																<button type="submit" class="btn blue">Değiştir</button>
															</div>
														</form:form>
													</div>
												</div>

											</div>
											<!-- 											<div class="modal-footer"> -->
											<!-- 												<button type="button" data-dismiss="modal" class="btn">Close</button> -->
											<!-- 												<button type="button" class="btn blue">Create</button> -->
											<!-- 											</div> -->
										</div>
									</div>
								</div>



							</div>
							<!-- END EXAMPLE TABLE PORTLET-->
						</div>
					</div>
					<!-- END PAGE CONTENT -->
				</div>
			</div>
			<!-- END CONTENT -->
		</div>
	</div>
	<!-- END CONTAINER -->
</div>

<%@include file="footer.jsp"%>



