
<%@include file="include.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="header.jsp"%>

<div class="page-content-main" id="idPageContent">

	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<%@include file="sidebar.jsp"%>
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<!-- BEGIN PAGE HEADER-->
				<!-- BEGIN PAGE HEAD -->
				<div class="page-head">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>
							Kullanıcı Ayarları <small> </small>
						</h1>
					</div>
					<!-- END PAGE TITLE -->
					<!-- BEGIN PAGE TOOLBAR -->

					<!-- END PAGE TOOLBAR -->
				</div>
				<!-- END PAGE HEAD -->
				<!-- BEGIN PAGE BREADCRUMB -->
				<ul class="page-breadcrumb breadcrumb">
					<!-- 					<li><a href="/depotakip/dashboard">Ana Sayfa</a> <i -->
					<!-- 						class="fa fa-circle"></i></li> -->
					<li><a href="/depotakip/profile">Kullanıcı Ayarları</a> <i
						class="fa fa-circle"></i></li>

				</ul>
				<!-- END PAGE BREADCRUMB -->
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->

				<div class="row">
					<c:choose>
						<c:when test="${ fromEditProfile }">



							<c:if test="${ isEdited}">
								<div class="alert alert-Success fade in">
									<i class="fa-lg fa fa-check"></i>
									<p style="display: inline-block;">Kullanıcı Bilgileriniz
										Başarıyla Güncellendi.</p>
									<button type="button" class="close" data-dismiss="alert"
										aria-hidden="true"></button>
								</div>
							</c:if>

							<c:if test="${ !isEdited }">
								<div class="alert alert-Danger fade in">
									<i class="fa-lg fa fa-warning"></i>
									<p style="display: inline-block;">İşlem Başarısız</p>
									<button type="button" class="close" data-dismiss="alert"
										aria-hidden="true"></button>
								</div>
							</c:if>
						</c:when>



					</c:choose>


					<div class="col-md-12">
						<form:form class="create-user-form" method="post"
							action="editProfile" modelAttribute="userBean">
							<div class="portlet box blue">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-edit"></i>Kullanıcı Bilgileri
									</div>
									<div class="tools">
										<a href="javascript:;" class="collapse" data-original-title=""
											title=""> </a> <a href="javascript:;" class="reload"
											data-original-title="" title=""> </a>
									</div>
								</div>

								<div class="portlet-body">

									<form:input type="number" class="form-control"
										style="display:none;" id="userId" name="userId" hidden="true"
										value="${user.id}" path="" />

									<div class="form-grup">
										<label class="col-md-2 col-sm-2 col-xs-4 profil" for="surName">Kullanıcı
											Adı</label>
										<div class="col-md-10 col-sm-10 col-xs-8">
											<form:input type="text" class="form-control  textboxx"
												id="userName" name="userName" required="required"
												value="${user.userName }" path="" />
										</div>
									</div>
									<div class="form-grup">
										<label class="col-md-2 col-sm-2 col-xs-4 profil" for="surName">Ad</label>
										<div class="col-md-10 col-sm-10 col-xs-8">
											<form:input type="text" class="form-control  textboxx"
												id="name" name="name" required="required"
												value="${user.name }" path="" />
										</div>
									</div>
									<div class="form-grup">
										<label class="col-md-2 col-sm-2 col-xs-4 profil" for="surName">Soyad</label>
										<div class="col-md-10 col-sm-10 col-xs-8">
											<form:input type="text" class="form-control  textboxx"
												id="surName" name="surName" required="required"
												value="${user.surName }" path="" />
										</div>
									</div>
<!-- 									<div class="form-grup"> -->

<!-- 										<label class="col-md-2 col-sm-2 col-xs-4 profil" -->
<!-- 											for="udepartmentId">Birim</label> -->
<!-- 										<div class="col-md-10 col-sm-10 col-xs-8"> -->
<!-- 											<select class="form-control  textboxx" id="udepartmentid" -->
<!-- 												name="departmentId" required="required"> -->
<!-- 												<option value="">Birim Seciniz</option> -->

<%-- 												<c:choose> --%>
<%-- 													<c:when test="${user.departmentId==1}"> --%>
<!-- 														<option value="1" selected>BELEDIYE BASKANLIGI</option> -->
<!-- 														<option value="2">BASKAN YARDIMCILIGI</option> -->
<!-- 														<option value="3">BILGI ISLEM</option> -->
<!-- 														<option value="4">ISYERI RUHSAT</option> -->
<!-- 														<option value="5">ZABITA</option> -->
<!-- 														<option value="6">SOSYAL ISLER</option> -->
<!-- 														<option value="7">IMAR - YAPI RUHSAT</option> -->
<!-- 														<option value="8">FEN ISLERI</option> -->
<!-- 														<option value="0">DIGER</option> -->
<%-- 													</c:when> --%>
<%-- 													<c:when test="${user.departmentId==2}"> --%>
<!-- 														<option value="1">BELEDIYE BASKANLIGI</option> -->
<!-- 														<option value="2" selected>BASKAN YARDIMCILIGI</option> -->
<!-- 														<option value="3">BILGI ISLEM</option> -->
<!-- 														<option value="4">ISYERI RUHSAT</option> -->
<!-- 														<option value="5">ZABITA</option> -->
<!-- 														<option value="6">SOSYAL ISLER</option> -->
<!-- 														<option value="7">IMAR - YAPI RUHSAT</option> -->
<!-- 														<option value="8">FEN ISLERI</option> -->
<!-- 														<option value="0">DIGER</option> -->
<%-- 													</c:when> --%>

<%-- 													<c:when test="${user.departmentId==3}"> --%>
<!-- 														<option value="1">BELEDIYE BASKANLIGI</option> -->
<!-- 														<option value="2">BASKAN YARDIMCILIGI</option> -->
<!-- 														<option value="3" selected>BILGI ISLEM</option> -->
<!-- 														<option value="4">ISYERI RUHSAT</option> -->
<!-- 														<option value="5">ZABITA</option> -->
<!-- 														<option value="6">SOSYAL ISLER</option> -->
<!-- 														<option value="7">IMAR - YAPI RUHSAT</option> -->
<!-- 														<option value="8">FEN ISLERI</option> -->
<!-- 														<option value="0">DIGER</option> -->
<%-- 													</c:when> --%>

<%-- 													<c:when test="${user.departmentId==4}"> --%>
<!-- 														<option value="1">BELEDIYE BASKANLIGI</option> -->
<!-- 														<option value="2">BASKAN YARDIMCILIGI</option> -->
<!-- 														<option value="3">BILGI ISLEM</option> -->
<!-- 														<option value="4" selected>ISYERI RUHSAT</option> -->
<!-- 														<option value="5">ZABITA</option> -->
<!-- 														<option value="6">SOSYAL ISLER</option> -->
<!-- 														<option value="7">IMAR - YAPI RUHSAT</option> -->
<!-- 														<option value="8">FEN ISLERI</option> -->
<!-- 														<option value="0">DIGER</option> -->
<%-- 													</c:when> --%>

<%-- 													<c:when test="${user.departmentId==5}"> --%>
<!-- 														<option value="1">BELEDIYE BASKANLIGI</option> -->
<!-- 														<option value="2">BASKAN YARDIMCILIGI</option> -->
<!-- 														<option value="3">BILGI ISLEM</option> -->
<!-- 														<option value="4">ISYERI RUHSAT</option> -->
<!-- 														<option value="5" selected>ZABITA</option> -->
<!-- 														<option value="6">SOSYAL ISLER</option> -->
<!-- 														<option value="7">IMAR - YAPI RUHSAT</option> -->
<!-- 														<option value="8">FEN ISLERI</option> -->
<!-- 														<option value="0">DIGER</option> -->
<%-- 													</c:when> --%>

<%-- 													<c:when test="${user.departmentId==6}"> --%>
<!-- 														<option value="1">BELEDIYE BASKANLIGI</option> -->
<!-- 														<option value="2">BASKAN YARDIMCILIGI</option> -->
<!-- 														<option value="3">BILGI ISLEM</option> -->
<!-- 														<option value="4">ISYERI RUHSAT</option> -->
<!-- 														<option value="5">ZABITA</option> -->
<!-- 														<option value="6" selected>SOSYAL ISLER</option> -->
<!-- 														<option value="7">IMAR - YAPI RUHSAT</option> -->
<!-- 														<option value="8">FEN ISLERI</option> -->
<!-- 														<option value="0">DIGER</option> -->
<%-- 													</c:when> --%>

<%-- 													<c:when test="${user.departmentId==7}"> --%>
<!-- 														<option value="1">BELEDIYE BASKANLIGI</option> -->
<!-- 														<option value="2">BASKAN YARDIMCILIGI</option> -->
<!-- 														<option value="3">BILGI ISLEM</option> -->
<!-- 														<option value="4">ISYERI RUHSAT</option> -->
<!-- 														<option value="5">ZABITA</option> -->
<!-- 														<option value="6">SOSYAL ISLER</option> -->
<!-- 														<option value="7" selected>IMAR - YAPI RUHSAT</option> -->
<!-- 														<option value="8">FEN ISLERI</option> -->
<!-- 														<option value="0">DIGER</option> -->
<%-- 													</c:when> --%>
<%-- 													<c:when test="${user.departmentId==8}"> --%>
<!-- 														<option value="1">BELEDIYE BASKANLIGI</option> -->
<!-- 														<option value="2">BASKAN YARDIMCILIGI</option> -->
<!-- 														<option value="3">BILGI ISLEM</option> -->
<!-- 														<option value="4">ISYERI RUHSAT</option> -->
<!-- 														<option value="5">ZABITA</option> -->
<!-- 														<option value="6">SOSYAL ISLER</option> -->
<!-- 														<option value="7">IMAR - YAPI RUHSAT</option> -->
<!-- 														<option value="8" selected>FEN ISLERI</option> -->
<!-- 														<option value="0">DIGER</option> -->
<%-- 													</c:when> --%>
<%-- 													<c:otherwise> --%>
<!-- 														<option value="1">BELEDIYE BASKANLIGI</option> -->
<!-- 														<option value="2">BASKAN YARDIMCILIGI</option> -->
<!-- 														<option value="3">BILGI ISLEM</option> -->
<!-- 														<option value="4">ISYERI RUHSAT</option> -->
<!-- 														<option value="5">ZABITA</option> -->
<!-- 														<option value="6">SOSYAL ISLER</option> -->
<!-- 														<option value="7">IMAR - YAPI RUHSAT</option> -->
<!-- 														<option value="8">FEN ISLERI</option> -->
<!-- 														<option value="0" selected>DIGER</option> -->

<%-- 													</c:otherwise> --%>
<%-- 												</c:choose> --%>



<!-- 											</select> -->

<!-- 										</div> -->
<!-- 									</div> -->
									<!-- 									<div style="margin: 5px;"> -->
									<!-- 										<label class="profil" for="uroleid">Birim</label> <select -->
									<!-- 											class="form-control textboxx" id="uroleid" name="roleId" -->
									<!-- 											required="required"> -->
									<!-- 											<option value="">Role Seciniz</option> -->
									<%-- 											<c:choose> --%>
									<%-- 												<c:when test="${user.roleId==1}"> --%>
									<!-- 													<option value="1" selected>Yönetici</option> -->
									<!-- 													<option value="2">Kullanici</option> -->
									<%-- 												</c:when> --%>
									<%-- 												<c:when test="${user.roleId==2}"> --%>
									<!-- 													<option value="1">Yönetici</option> -->
									<!-- 													<option value="2" selected>Kullanici</option> -->
									<%-- 												</c:when> --%>
									<%-- 												<c:otherwise> --%>
									<!-- 													<option value="1">Yönetici</option> -->
									<!-- 													<option value="2">Kullanici</option> -->
									<%-- 												</c:otherwise> --%>
									<%-- 											</c:choose> --%>


									<!-- 										</select> -->




									<!-- 									</div> -->



									<div class="form-actions noborder mobildiv"
										style="margin-bottom: 50px;">

										<input type="submit" class="btn blue pull-right"
											data-user="${user.id}" data-userName="${user.userName}"
											value="Değiştir" />

									</div>

								</div>
						</form:form>
					</div>

				</div>

				<div id="editProfileModal" class="modal fade" tabindex="-1"
					data-backdrop="static" data-keyboard="false">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									aria-hidden="true"></button>
								<h4 class="modal-title">Uyarı</h4>
							</div>
							<div class="modal-body">
								<p id="editProfileText">
									<!-- 												 Would you like to continue with some arbitrary task? -->
								</p>
							</div>
							<div class="modal-footer">
								<button type="button" data-dismiss="modal" class="btn default">İptal</button>
								<button id="editProfileButton" type="button"
									data-dismiss="modal" class="btn green">Devam Et</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END CONTENT -->
		</div>
	</div>
	<!-- END CONTAINER -->
</div>




<%@include file="footer.jsp"%>



