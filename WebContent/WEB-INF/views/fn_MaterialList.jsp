
<%@include file="include.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@include file="header.jsp"%>

<div class="page-content-main" id="idPageContent">

	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<%@include file="sidebar.jsp"%>
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">

			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->

				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<!-- BEGIN PAGE HEADER-->
				<!-- BEGIN PAGE HEAD -->
				<div class="page-head">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>
							Ürün Yönetimi <small> Ürün Listesi</small>
						</h1>
					</div>
					<!-- END PAGE TITLE -->
					<!-- BEGIN PAGE TOOLBAR -->

					<!-- END PAGE TOOLBAR -->
				</div>
				<!-- END PAGE HEAD -->
				<!-- BEGIN PAGE BREADCRUMB -->
				<ul class="page-breadcrumb breadcrumb">
					<!-- 					<li><a href="/depotakip/dashboard">Ana Sayfa</a> <i -->
					<!-- 						class="fa fa-circle"></i></li> -->
					<li><a href="/depotakip/fn_MaterialList">Ürün Yönetimi</a> <i
						class="fa fa-circle"></i></li>

				</ul>
				<!-- END PAGE BREADCRUMB -->
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-edit"></i>Ürün Listesi
								</div>
								<div class="tools">
<!-- 									<a href="javascript:;" class="collapse" data-original-title="" -->
<!-- 										title=""> </a>  -->
										<a href="javascript:;" class="reload"
										data-original-title="" title=""> </a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="table-toolbar">
									<div class="row">
										<div class="col-md-3 col-sm-4 col-xs-6">
											<!-- 											<div -->
											<!-- 												class="form-group form-md-line-input form-md-floating-label has-info comboboxbirim"> -->
											<!-- 												<select class="form-control comboform" id="selectdepartmentId" -->
											<!-- 													name="departmentId" required="required"> -->
											<!-- 													<option value="">Birim Seciniz</option> -->
											<!-- 													<option value="1">BELEDIYE BASKANLIGI</option> -->
											<!-- 													<option value="2">BASKAN YARDIMCILIGI</option> -->
											<!-- 													<option value="3">BILGI ISLEM</option> -->
											<!-- 													<option value="4">ISYERI RUHSAT</option> -->
											<!-- 													<option value="5">ZABITA</option> -->
											<!-- 													<option value="6">SOSYAL ISLER</option> -->
											<!-- 													<option value="7">IMAR - YAPI RUHSAT</option> -->
											<!-- 													<option value="8">FEN ISLERI</option>													 -->
											<!-- 													<option value="0">DIGER</option> -->
											<!-- 												</select> -->
											<!-- 											</div> -->
										</div>
										<div class="col-md-6 col-sm-4 "></div>
										<div class="col-md-3 col-sm-4 col-xs-6">
											<div class="btn-group pull-right">

												<a class=" btn green" data-target="#createMaterialModal"
													data-toggle="modal"> Ürün Ekle </a>
												<!-- 												<a class=" btn green" > Excel'e aktar </a> -->
												<!--  <input type="button" class=" btn green"  onclick="tableToExcel()" value="Export to Excel"> -->


											</div>
										</div>

									</div>
								</div>
								<div id="sample_editable_1_wrapper"
									class="dataTables_wrapper no-footer">



									<div class="table-scrollable">

										<table
											class="table table-striped table-hover table-bordered dataTable no-footer"
											id="material_list_table" role="grid"
											aria-describedby="sample_editable_1_info">
											<thead>


												<tr role="row">
													<th class="sorting_asc" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-sort="ascending"
														aria-label="
									 malzeme
								: activate to sort column ascending"
														style="width: 274px;">Ürün Adı</th>
													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 birim
								: activate to sort column ascending"
														style="width: 120px;">Birim</th>
													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 kdv
								: activate to sort column ascending"
														style="width: 120px;">Kdv Oranı</th>
													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 stopajOran
								: activate to sort column ascending"
														style="width: 120px;">Stopaj Oranı</th>

													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 aciklama
								: activate to sort column ascending"
														style="width: 120px;">Aciklama</th>
													<!-- 													<th class="sorting" tabindex="0" -->
													<!-- 														aria-controls="sample_editable_1" rowspan="1" colspan="1" -->
													<!-- 														aria-label=" -->
													<!-- 									 birimFiyat -->
													<!-- 								: activate to sort column ascending" -->
													<!-- 														style="width: 120px;">Birim Fiyat</th> -->
													<th class="sorting" tabindex="0"
														aria-controls="sample_editable_1" rowspan="1" colspan="1"
														aria-label="
									 stok
								: activate to sort column ascending"
														style="width: 120px;">Stok</th>


													<c:if test="${user.roleId == 1}">


														<th class="sorting updateMaterial" tabindex="0"
															aria-controls="sample_editable_1" rowspan="1" colspan="1"
															aria-label="
									 Edit
								: activate to sort column ascending"
															style="width: 83px;">Düzenle</th>
													</c:if>
													<c:if test="${user.roleId == 2}">
													</c:if>
													<c:if test="${user.roleId == 1}">
														<th class="sorting deleteMaterial" tabindex="0"
															aria-controls="sample_editable_1" rowspan="1" colspan="1"
															aria-label="
									 Delete
								: activate to sort column ascending"
															style="width: 121px;">Sil</th>
													</c:if>
													<c:if test="${user.roleId == 2}">
													</c:if>
												</tr>

											</thead>
											<tbody id="tbodyid">

												<c:set var="i" scope="page" value="0" />
												<c:forEach var="material" items="${materialList}">
													<c:choose>
														<c:when test="${i mod 2 eq 0}">
															<tr id="${material.materialId}" class="odd">
														</c:when>
														<c:otherwise>
															<tr id="${material.materialId}" class="even">
														</c:otherwise>
													</c:choose>
													<td>${material.malzeme}</td>
													<td>${material.birim}</td>
													<td>${material.kdvOran}</td>
													<td>${material.stopajOran}</td>
													<td>${material.aciklama}</td>
													<%-- 													<td>${material.birimFiyat}</td> --%>
													<td>${material.stok}</td>

													<c:if test="${user.roleId == 1}">
														<td class="center"><input type="button"
															class="btn btn-success  updateMaterial"
															data-materialId="${material.materialId}"
															data-malzeme="${material.malzeme}"
															data-birim="${material.birim}"
															data-kdvOran="${material.kdvOran}"
															data-stopajOran="${material.stopajOran}"
															data-aciklama="${material.aciklama}"
															data-stok="${material.stok}" value="Düzenle" /></td>
													</c:if>
													<c:if test="${user.roleId == 2}">
													</c:if>
													<c:if test="${user.roleId == 1}">
														<td class="center "><input type="button"
															class="btn btn-danger  deleteMaterial"
															data-materialId="${material.materialId}" value="Sil" /></td>
													</c:if>
													<c:if test="${user.roleId == 2}">
													</c:if>
													</tr>
													<c:set var="i" scope="page" value="${i+1}" />
												</c:forEach>
											</tbody>
										</table>


									</div>

								</div>
								<!-- /.modal -->
								<div id="createMaterialModal" class="modal fade" tabindex="-1"
									data-width="400">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true"></button>
												<h3 class="modal-title">ÜRÜN EKLE</h3>
											</div>
											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
														<form:form class="create-material-form" method="post"
															action="createNewMaterial" modelAttribute="materialBean">
															<div class="form-body">
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" class="form-control" id="malzeme"
																		name="malzeme" required="required" /> <label
																		for="malzeme">Ürün Adı</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" class="form-control" id="birim"
																		name="birim" required="required" /> <label
																		for="birim">Birim</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="number" step="0.01"
																		placeholder="KDV oranını giriniz. Örn 8"
																		class="form-control" id="kdvOran" name="kdvOran"
																		required="required" /> <label for="kdvOran">Kdv Oran</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="number" step="0.01"
																		placeholder="Stopaj oranını giriniz. Örn 2"
																		class="form-control" id="stopajOran" name="stopajOran"
																		required="required" /> <label for="stopajOran">Stopaj Oran</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" class="form-control" id="aciklama"
																		name="aciklama" value="" /> <label for="aciklama">Acıklama</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="number" step="0.01"
																		placeholder="Stok miktarını giriniz. Örn 12.34"
																		class="form-control" id="stok" name="stok"
																		required="required" /> <label for="stok">Stok</label>
																</div>
																<!-- 																<div -->
																<!-- 																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
																<!-- 																	<input  type="number" step="0.01"   placeholder="Birim fiyatı giriniz. Örn 12.50" class="form-control" id="birimFiyat" -->
																<!-- 																		name="birimFiyat" required="required" /> <label -->
																<!-- 																		for="birimFiyat">Birim Fiyat</label> -->
																<!-- 																</div> -->

															</div>
															<div class="form-actions noborder"
																style="margin-top: 25px;">
																<button type="button" data-dismiss="modal"
																	class="btn default">İptal</button>
																<button type="submit" class="btn blue">Kaydet</button>
															</div>
														</form:form>
													</div>
												</div>

											</div>
											<!-- 											<div class="modal-footer"> -->
											<!-- 												<button type="button" data-dismiss="modal" class="btn">Close</button> -->
											<!-- 												<button type="button" class="btn blue">Create</button> -->
											<!-- 											</div> -->
										</div>
									</div>
								</div>
								<div id="deleteMaterialModal" class="modal fade" tabindex="-1"
									data-backdrop="static" data-keyboard="false">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true"></button>
												<h4 class="modal-title">Uyarı</h4>
											</div>
											<div class="modal-body">
												<p id="deleteMaterialText">
													<!-- 												 Would you like to continue with some arbitrary task? -->
												</p>
											</div>
											<div class="modal-footer">
												<button type="button" data-dismiss="modal"
													class="btn default">İptal</button>
												<button id="deleteMaterialButton" type="button"
													data-dismiss="modal" class="btn green">Devam et</button>
											</div>
										</div>
									</div>
								</div>



								<div id="updateMaterialModal" class="modal fade" tabindex="-1"
									data-width="400">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true"></button>
												<h3 class="modal-title">DÜZENLE</h3>
											</div>

											<div class="modal-body">
												<div class="row">
													<div class="col-md-12">
														<form:form class="update-material-form" method="post"
															action="updateMaterial" modelAttribute="materialBean">
															<div class="form-body">


																<input type="hidden" class="form-control"
																	id="materialIdU" name="materialId" required="required" />
																<label for="materialIdU"></label>


																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" class="form-control" id="malzemeU"
																		name="malzeme" required="required" /> <label
																		for="malzemeU">Ürün Adı</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" class="form-control" id="birimU"
																		name="birim" required="required" /> <label
																		for="birimU">Birim</label>
																</div>
																	<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="number" step="0.01"
																		placeholder="KDV oranını giriniz. Örn 8"
																		class="form-control" id="kdvOranU" name="kdvOran"
																		required="required" /> <label for="kdvOranU">Kdv Oran</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="number" step="0.01"
																		placeholder="Stopaj oranını giriniz. Örn 2"
																		class="form-control" id="stopajOranU" name="stopajOran"
																		required="required" /> <label for="stopajOranU">Stopaj Oran</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="text" class="form-control" id="aciklamaU"
																		name="aciklama" value="" /> <label for="aciklamaU">Acıklama</label>
																</div>
																<div
																	class="form-group form-md-line-input form-md-floating-label has-success">
																	<input type="number" step="0.01"
																		placeholder="Stok miktarını giriniz. Örn 12.34"
																		class="form-control" id="stokU" name="stok"
																		required="required" /> <label for="stokU">Stok</label>
																</div>
																<!-- 																<div -->
																<!-- 																	class="form-group form-md-line-input form-md-floating-label has-success"> -->
																<!-- 																	<input  type="number" step="0.01"   placeholder="Birim fiyatı giriniz. Örn 12.50" class="form-control" id="birimFiyatU" -->
																<!-- 																		name="birimFiyat" required="required" /> <label -->
																<!-- 																		for="birimFiyatU">Birim Fiyat</label> -->
																<!-- 																</div> -->

															</div>
															<div class="form-actions noborder"
																style="margin-top: 25px;">
																<button type="button" data-dismiss="modal"
																	class="btn default">İptal</button>
																<button type="submit" class="btn blue">Güncelle</button>
															</div>
														</form:form>
													</div>
												</div>

											</div>

										</div>
									</div>
								</div>



							</div>
							<!-- END EXAMPLE TABLE PORTLET-->
						</div>
					</div>
					<!-- END PAGE CONTENT -->
				</div>
			</div>
			<!-- END CONTENT -->
		</div>

	</div>
	<!-- END CONTAINER -->
</div>

<%@include file="footer.jsp"%>



