// Global variables
var userHolder;

var workOrderHolder;

var materialStockEntryHolder;

var userName;

var companyHolder;

var companyName;

var materialHolder;

var materialStockHolder;

var musteriHolder;

var islemHolder;

var stopajInfoHolder;

$(document)
		.ready(
				function() {
					// console.log("Metronic ready");
					Metronic.init(); // init metronic core componets
					Layout.init(); // init layout
					Demo.init(); // init demo features
					// Index.init(); // init index page
					Tasks.initDashboardWidget(); // init tash dashboard
					// widget

					$.ajax({
						url : "/depotakip/materialReport",
						type : "GET",
						success : function(response) {

							// console.log("material report response" +
							// response)
							musteriReport = response;

							// alert("Musteri silme işlemi başarılı.");
							// location.href = "/depotakip/musteriList";

						},
						error : function(response) {
							console.log(response);
							// alert("Error : " + response);
							// document.getElementById("deleteUserError").innerHTML
							// =
							// response.responseText;

						}

					});

					Highcharts
							.chart(
									'container1',
									{
										chart : {
											plotBackgroundColor : null,
											plotBorderWidth : null,
											plotShadow : false,
											type : 'pie'
										},
										title : {
											text : 'ÜRÜN STOK MİKTARI, 22 MAYIS, 2017'
										},
										tooltip : {
											pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
										},
										plotOptions : {
											pie : {
												allowPointSelect : true,
												cursor : 'pointer',
												dataLabels : {
													enabled : true,
													format : '<b>{point.name}</b>: {point.percentage:.1f} %',
													style : {
														color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
																|| 'black'
													}
												}
											}
										},
										series : [ {
											name : 'ÜRÜN MİKTARI',
											colorByPoint : true,
											data : [ {
												name : 'FINDIK',
												y : 72.05
											}, {
												name : 'BUGDAY',
												y : 12.80,
												sliced : true,
												selected : true
											}, {
												name : 'ÇELTİK',
												y : 15.15
											} ]
										} ]
									});

					Highcharts
							.chart(
									'container2',
									{
										chart : {
											plotBackgroundColor : null,
											plotBorderWidth : null,
											plotShadow : false,
											type : 'pie'
										},
										title : {
											text : 'ÜRÜN Alış Satış Oranı, 22 MAYIS, 2017'
										},
										tooltip : {
											pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
										},
										plotOptions : {
											pie : {
												allowPointSelect : true,
												cursor : 'pointer',
												dataLabels : {
													enabled : true,
													format : '<b>{point.name}</b>: {point.percentage:.1f} %',
													style : {
														color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
																|| 'black'
													}
												}
											}
										},
										series : [ {
											name : 'Alış Satış Oranı',
											colorByPoint : true,
											data : [ {
												name : 'ALIŞ',
												y : 65.00
											}, {
												name : 'SATIŞ',
												y : 35.00,
												sliced : true,
												selected : true
											} ]
										} ]
									});

				});

/*
 * $(".select-mahalle1").change(function() {
 * 
 * var bId = $(".select-mahalle1").find(':selected').data('bolge'); alert("bolge : " +
 * bId); enableBolge(bId);
 * 
 * });
 * 
 * $(".select-mahalle2").change(function() {
 * 
 * var bId = $(".select-mahalle2").find(':selected').data('bolge');
 * 
 * enableBolge(bId);
 * 
 * });
 */

$(document).on('click', '#dashboardPage', function(event) {
	$(".page-content-main").load("dashboard", {
		"" : ""
	}, function() {
		// //console.log("load dashboard");
		// $(reloader());
		// location.reload();
	});

});

$(document)
		.on(
				'click',
				'.editProfile',
				function(event) {

					$("#editProfileModal").modal("show");

					document.getElementById("editProfileText").innerHTML = "Are you sure to delete user: "
							+ $(this).data("username");

					userHolder = $(this).data("user");

				});

$(document)
		.on(
				'click',
				'.logoutUser',
				function(event) {

					$("#logoutUserModal").modal("show");

					document.getElementById("logoutUserText").innerHTML = "Are you sure to log out user: "
							+ $(this).data("username");

					userHolder = $(this).data("user");

				});

$(document).on('click', '.editUser', function(event) {

	$("#editUserModal").modal("show");

	// document.getElementById("deleteUserText").innerHTML = "Are you sure to
	// delete user: " + $(this).data("username");

	userHolder = $(this).data("userid");
	userName = $(this).data("username");
	var namename = $(this).data("name");
	var surname = $(this).data("surname");
	var depid = $(this).data("departmant");
	var rolid = $(this).data("roleid");

	$("#userId").val(userHolder);
	$("#uUserName").val(userName);
	$("#uname").val(namename);
	$("#surname").val(surname);
	$("#usersRoleid").val(rolid);
	$("#usersDepartmentId").val(depid);

});

$(document).on('click', '.updateCompany', function(event) {

	$("#updateCompanyModal").modal("show");

	companyHolder = $(this);

	var cName = companyHolder.data("companyname");

	var id = companyHolder.data("company");

	// console.log("cName : " + cName);

	document.getElementById("companyname").value = cName;

	document.getElementById("companyId").value = id;
	// $("#name").val(companyHolder.data("companyname"));

});

$(document)
		.on(
				'click',
				'.deleteCompany',
				function(event) {

					$("#deleteCompanyModal").modal("show");
					companyHolder = $(this);
					document.getElementById("deleteCompanyText").innerHTML = "Şirketi ve tüm kayıtlarını silmek istediğinize emin misiniz?";

				});

$("#deleteCompanyButton").click(function() {
	// console.log("deleteCompanyButton tıklandı");
	// console.log(companyHolder.data("company"));
	$.ajax({
		url : "/depotakip/deleteCompany",
		type : "POST",
		data : {
			companyId : companyHolder.data("company")
		},
		success : function(response) {

			$("#deleteCompanyModal").modal("hide");

			alert("Silme işlemi başarılı.");
			location.href = "/depotakip/fn_CompanyManagement";

		},
		error : function(response) {
			// console.log(response);
			alert("Error : " + response);
			// document.getElementById("deleteUserError").innerHTML =
			// response.responseText;

		}

	});

});

function showCompany(e) {

	var selected = e.value;

	console.log(selected);

	if (selected == 1) {// Alış
		$(".companySelect").hide();

	} else if (selected == 2) { // Satış
		$(".companySelect").show();

	} else {

		$(".companySelect").hide();

	}

}

$(document).on('click', '.updateMaterial', function(event) {

	$("#updateMaterialModal").modal("show");
	materialHolder = $(this);

	var materialId = $(this).data("materialid");
	var malzeme = $(this).data("malzeme");
	var aciklama = $(this).data("aciklama");
	var birim = $(this).data("birim");
	var stok = $(this).data("stok");
	var stopajOran = $(this).data("stopajoran");
	var kdvOran = $(this).data("kdvoran");

	// var birimFiyat = $(this).data("birimfiyat");

	$("#materialIdU").val(materialId);
	$("#malzemeU").val(malzeme);
	$("#aciklamaU").val(aciklama);
	$("#birimU").val(birim);
	$("#kdvOranU").val(kdvOran);
	$("#stopajOranU").val(stopajOran);
	$("#stokU").val(stok);
	// $("#birimFiyatU").val(birimFiyat);
});

$(document).on('click', '.updateMusteri', function(event) {

	$("#updateMusteriModal").modal("show");
	musteriHolder = $(this);

	$("#musteriIdU").val($(this).data("musteriid"));
	$("#adU").val($(this).data("ad"));
	$("#babaAdiU").val($(this).data("babaadi"));
	$("#dogumYeriU").val($(this).data("dogumyeri"));
	$("#koyU").val($(this).data("koy"));
	$("#dogumTarihiU").val($(this).data("dogumtarihi"));
	$("#hesapNoU").val($(this).data("hesapno"));
	$("#vergiNoU").val($(this).data("vergino"));
	$("#vergiDaireU").val($(this).data("vergidaire"));

});

$(document)
		.on(
				'click',
				'.deleteMusteri',
				function(event) {

					$("#deleteMusteriModal").modal("show");
					musteriHolder = $(this);
					document.getElementById("deleteMusteriText").innerHTML = "Müsteri ve müşteriye ait tüm kayıtlarını silmek istediğinize emin misiniz?";

				});

$("#deleteMusteriButton").click(function() {
	// console.log("deleteMaterial tıklandı");
	// console.log(materialHolder.data("materialid"));
	$.ajax({
		url : "/depotakip/deleteMusteri",
		type : "POST",
		data : {
			musteriId : musteriHolder.data("musteriid")
		},
		success : function(response) {

			$("#deleteMusteriModal").modal("hide");
			musteriHolder = null;

			alert("Musteri silme işlemi başarılı.");
			location.href = "/depotakip/musteriList";

		},
		error : function(response) {
			// console.log(response);
			alert("Error : " + response);
			// document.getElementById("deleteUserError").innerHTML =
			// response.responseText;

		}

	});

});

//
// $("#exportToPdfButton").click(function() {
//
// // location.href = "/depotakip/deleteIslem/"
// // + islemHolder;
//
// $.ajax({
// url : "/depotakip/exportToPdf",
// type : "POST",
// data : {
//
// },
// success : function(response) {
//
// // $("#deleteMaterialModal").modal("hide");
// //document.getElementById(islemHolder).remove();
// //islemHolder = null;
//
// alert("Pdf oluşturma işlemi başarılı.");
// // location.href = "/depotakip/fn_MaterialList";
//
// },
// error : function(response) {
// // console.log(response);
// alert("Error : " + response);
// // document.getElementById("deleteUserError").innerHTML =
// // response.responseText;
//
// }
//
// });
//
// });

$(document)
		.on(
				'click',
				'.deleteIslem',
				function(event) {

					$("#deleteIslemModal").modal("show");

					document.getElementById("deleteIslemText").innerHTML = "İşlem silmek istediğinize emin misiniz?";

					islemHolder = $(this).data("islemid");
					// var id = $(this).data("id");

				});

$("#deleteIslemButton").click(function() {

	// location.href = "/depotakip/deleteIslem/"
	// + islemHolder;

	$.ajax({
		url : "/depotakip/deleteIslem/" + islemHolder,
		type : "POST",
		data : {

		},
		success : function(response) {

			// $("#deleteMaterialModal").modal("hide");
			document.getElementById(islemHolder).remove();
			islemHolder = null;

			alert("Silme işlemi başarılı.");
			// location.href = "/depotakip/fn_MaterialList";

		},
		error : function(response) {
			// console.log(response);
			alert("Error : " + response);
			// document.getElementById("deleteUserError").innerHTML =
			// response.responseText;

		}

	});

});

$(document).on('click', '.createIslemButton', function(event) {

	$("#createIslemModal").modal("show");

	$('select[id=materialId]').val(0);
	$('select[id=islemTuru]').val(0);

	$("#islemId").val(0);
	$("#fisNo").val("");
	$("#miktar").val("");
	$("#birimFiyat").val("");
	$("#tutar").val("");
	$("#stopaj").val("");
	$("#yekun").val("");

	$("#mAd").val("");
	$("#mBabaAd").val("");
	$("#mKoy").val("");

	$("#mHesapNo").val("");
	$("#mDogumYeri").val("");
	$("#mTel").val("");
	$("#mDogumTarihi").val("");

	$('#tarih').val("");

});

$(document).on('click', '.updateIslem', function(event) {

	$("#createIslemModal").modal("show");
	islemHolder = $(this);

	// Disabled change material and islemTuru select box

	var islem = $(this).data("islemobj")

	var iString = islem.replace(/[']/g, "\"");
	var iObj = JSON.parse(iString);

	$('select[id=materialId]').val(iObj.materialId);
	$('select[id=islemTuru]').val(iObj.islemTuru);

//	$('.materialSelect').prop('disabled', true);
//	$('.materialSelect').selectpicker('refresh');

//	$('.islemTuruSelect').prop('disabled', true);
//	$('.islemTuruSelect').selectpicker('refresh');
	
	if (iObj.islemTuru == 2) {
		$(".companySelect").show();
		$("select[id=companyId]").val(iObj.companyId);

	} else {
		$(".companySelect").hide();
	}

	$("#islemId").val(iObj.islemId);
	$("#fisNo").val(valueControl(iObj.fisNo));
	$("#miktar").val(valueControl(iObj.miktar));
	$("#birimFiyat").val(valueControl(iObj.birimFiyat));
	$("#tutar").val(valueControl(iObj.tutar));
	$("#stopaj").val(valueControl(iObj.stopaj));
	$("#yekun").val(valueControl(iObj.yekun));

	$("#mAd").val(iObj.mAd);
	$("#mBabaAd").val(iObj.mBabaAd);
	$("#mKoy").val(iObj.mKoy);

	$("#mHesapNo").val(valueControl(iObj.mHesapNo));
	$("#mDogumYeri").val(iObj.mDogumYeri);
	$("#mTel").val(valueControl(iObj.mTel));
	$("#mDogumTarihi").val(valueControl(iObj.mDogumTarihi));

	var tarih = $(this).data("tarih");
	var dbDate = toDate(tarih);
	$('#tarih').val(dbDate);

});

function valueControl(value){
	if(value == 0){
		return "";
	}else{
		return value;
	}
	
}
function toDate(dateStr) {
	var parts = dateStr.split("-");

	var years = (parts[2]).slice(-4);
	var month = (parts[1]).slice(-2);

	var day = parts[0];
	var dateString = years + "-" + (month) + "-" + (day);

	return dateString;
}

$(document)
		.on(
				'click',
				'.deleteMaterialStockEntry',
				function(event) {

					$("#deleteMaterialStockEntryModal").modal("show");
					// materialStockEntryHolder = $(this);
					document.getElementById("deleteMaterialStockEntryText").innerHTML = "Ürün ve tüm kayıtlarını silmek istediğinize emin misiniz?";
					materialStockEntryHolder = $(this).data(
							"materialstockentryid");
				});

$("#deleteMaterialStockEntryButton").click(
		function() {
			// console.log("deleteMaterialStockEntry tıklandı");
			// console.log("materialstockentryid" + materialStockEntryHolder);

			// location.href = "/depotakip/deleteMaterialStockEntry/" +
			// materialStockEntryHolder;

			$.ajax({
				url : "/depotakip/deleteMaterialStockEntry/"
						+ materialStockEntryHolder,
				type : "POST",
				data : {

				},
				success : function(response) {

					// $("#deleteMaterialModal").modal("hide");
					document.getElementById(materialStockEntryHolder).remove();
					materialStockEntryHolder = null;

					// alert("Silme işlemi başarılı.");
					// location.href = "/depotakip/fn_MaterialList";

				},
				error : function(response) {
					// console.log(response);
					alert("Error : " + response);
					// document.getElementById("deleteUserError").innerHTML =
					// response.responseText;

				}

			});

		});

$(document)
		.on(
				'click',
				'.deleteMaterialStock',
				function(event) {

					$("#deleteMaterialStokModal").modal("show");
					// materialStockEntryHolder = $(this);
					document.getElementById("deleteMaterialStokText").innerHTML = "Ürün ve tüm kayıtlarını silmek istediğinize emin misiniz?";
					materialStockHolder = $(this).data("materialstockid");
				});

$("#deleteMaterialStokButton").click(function() {
	// console.log("deleteMaterialStock tıklandı");
	// console.log("materialstockid" + materialStockHolder);

	$.ajax({
		url : "/depotakip/deleteMaterialStock/" + materialStockHolder,
		type : "POST",
		data : {

		},
		success : function(response) {

			// $("#deleteMaterialModal").modal("hide");
			document.getElementById(materialStockHolder).remove();
			materialStockHolder = null;

			// alert("Silme işlemi başarılı.");
			// location.href = "/depotakip/fn_MaterialList";

		},
		error : function(response) {
			// console.log(response);
			alert("Error : " + response);
			// document.getElementById("deleteUserError").innerHTML =
			// response.responseText;

		}

	});

});

$(document)
		.on(
				'click',
				'.deleteMaterial',
				function(event) {

					$("#deleteMaterialModal").modal("show");
					materialHolder = $(this);
					document.getElementById("deleteMaterialText").innerHTML = "Ürün ve tüm kayıtlarını silmek istediğinize emin misiniz?";

				});

$("#deleteMaterialButton").click(function() {
	// console.log("deleteMaterial tıklandı");
	// console.log(materialHolder.data("materialid"));
	$.ajax({
		url : "/depotakip/deleteMaterial",
		type : "POST",
		data : {
			materialId : materialHolder.data("materialid")
		},
		success : function(response) {

			$("#deleteMaterialModal").modal("hide");
			materialHolder = null;

			alert("Silme işlemi başarılı.");
			location.href = "/depotakip/fn_MaterialList";

		},
		error : function(response) {
			// console.log(response);
			alert("Error : " + response);
			// document.getElementById("deleteUserError").innerHTML =
			// response.responseText;

		}

	});

});

var filePathPDF;
$(document).on('click', '.stopajButton', function(event) {

	stopajInfoHolder = $(this);

	var materialId = $("#materialId option:selected").val();

	var islemTuru = $("#islemTuru option:selected").val();

	var tarih1 = $("#tarih1").val();

	var tarih2 = $("#tarih2").val();

	$.ajax({
		url : "/depotakip/createStopajReport",
		type : "POST",
		data : {
			materialId : materialId,
			islemTuru : islemTuru,
			tarih1 : tarih1,
			tarih2 : tarih2

		},
		success : function(response) {
			filePathPDF = response;

			$("#showStopajModal").modal("show");

			// alert( response);
			// alert("Stopaj listesi pdf olarak kaydedilmiştir.");
			// location.href = "/depotakip/fn_MaterialList";

		},
		error : function(response) {
			// console.log(response);
			alert("Error : " + response);
			// document.getElementById("deleteUserError").innerHTML =
			// response.responseText;

		}

	});

	// alert(materialId + " - " +islemTuru +" - " +tarih1 +" - " +tarih2);

});

// user delete modal.
$("#downloadStopajButton").click(function() {
	// console.log("deleteUserButtonUsers butona tıklandı");

	$("#showStopajModal").modal("hide");
	window.location.href = "/depotakip/downloadStopaj";

});

$(document).on(
		'keyup change',
		'#miktar',
		function(evt) {

			console.log(" Miktar- " + this.value);

			document.getElementById('tutar').value = (document
					.getElementById('birimFiyat').value)
					* this.value;

			document.getElementById('stopaj').value = (document
					.getElementById('tutar').value) * 0.02;

			var stopaj = document.getElementById('stopaj').value;
			var tutar = document.getElementById('tutar').value;

			document.getElementById('yekun').value = parseFloat(stopaj)
					+ parseFloat(tutar);

		});

$(document).on(
		'keyup change',
		'#birimFiyat',
		function(evt) {

			console.log(" birimFiyat- " + this.value);

			document.getElementById('tutar').value = (document
					.getElementById('miktar').value)
					* this.value;

			document.getElementById('stopaj').value = (document
					.getElementById('tutar').value) * 0.02;

			var stopaj = document.getElementById('stopaj').value;
			var tutar = document.getElementById('tutar').value;

			document.getElementById('yekun').value = parseFloat(stopaj)
					+ parseFloat(tutar);

		});

$(document).on('click', '.updateMaterialStock', function(event) {

	$("#updateMaterialStokModal").modal("show");
	materialStockHolder = $(this);

	var id = $(this).data("id");
	var materialId = $(this).data("materialid");
	var irsaliyenumara = $(this).data("numara");
	var aciklamasi = $(this).data("aciklama");
	var cikis = $(this).data("cikis");
	var company = $(this).data("companyid");
	// var digercikis = $(this).data("digercikis");
	var mahallesi = $(this).data("mahalle");
	var sokak = $(this).data("sokak");
	var no = $(this).data("no");

	$("#materialStockId").val(id);
	$("#materialIdU").val(materialId);
	$("#irsaliyeNoU").val(irsaliyenumara);
	$("#aciklamaU").val(aciklamasi);
	$("#cikisU").val(cikis);
	$("#companyIdU").val(company);
	// $("#digerCikisU").val(digercikis);
	$("#mahalleU").val(mahallesi);
	$("#noU").val(no);
	$("#sokakU").val(sokak);

});

function exportToExcel(intable) {
	intable = document.getElementById(intable);
	this.table = intable.cloneNode(true);
	var cform = document.createElement("form");
	cform.style.display = "none";
	cform.setAttribute("method", "POST");
	cform.setAttribute("action", "exporttoexcel.jsp");
	cform.setAttribute("name", "ExcelForm");
	cform.setAttribute("id", "ExcelForm");
	cform.setAttribute("enctype", "MULTIPART/FORM-DATA");
	cform.encoding = "multipart/form-data";
	var ta = document.createElement("textarea");
	ta.name = "ExcelTable";
	var tabletext = this.table.outerHTML;
	ta.defaultValue = tabletext;
	ta.value = tabletext;
	cform.appendChild(ta);
	intable.parentNode.appendChild(cform);
	cform.submit();
	// clean up
	ta.defaultValue = null;
	ta = null;
	tabletext = null;
	this.table = null;
}
/*
 * function enableBolge(bolge) { // console.log(bolge + ". Bolge ");
 * 
 * if (bolge == "1") { // console.log(bolge + ". Bolge ");
 * document.getElementsByName('cikis1').disabled = false
 * document.getElementByName('cikis2').disabled = true } else if (bolge == "2") { //
 * console.log(bolge + ". Bolge "); document.getElementByName('cikis1').disabled =
 * true document.getElementByName('cikis2').disabled = false } }
 */

