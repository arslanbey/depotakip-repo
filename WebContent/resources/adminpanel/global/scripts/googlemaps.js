var map;

function getCoordinates(address, callback) {
	var coordinates;
	geocoder.geocode({
		address : address
	}, function(results, status) {
		for (var i = 0; i < results.length; i++) {
			coords_obj = results[i].geometry.location;
			coordinates.push([ coords_obj.nb, coords_obj.ob ]);
		}
		callback(coordinates);
	});
}

function initializeGoogleMap() {

	// $("#map-canvas").empty();
	// $('#map-canvas').attr('style','');
	console.log("initializeGoogleMap() is started.");
	// console.log("searching address :." +searchingAdress );
	// var ruhsatAdres = getUrlVars()["ruhsatAdres"];
	// ruhsatAdres = decodeURIComponent(ruhsatAdres);
	// console.log("adres="+adres);

	// console.log("ruhsatAdres1="+ruhsatAdres);
	var geocoder = new google.maps.Geocoder();
	var mapProp = {
		center : new google.maps.LatLng(40.987284, 28.908360),
		zoom : 15,
		mapTypeId : google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map-canvas"), mapProp);

	// listen for the window resize event & trigger Google Maps to update too
	  $(window).resize(function() {
	    // (the 'map' here is the result of the created 'var map = ...' above)
	    google.maps.event.trigger(map, "resize");
	    console.log('map resized!!!!');
	  });
	// if(searchingAdress !==""){
	// geocoder.geocode( { 'address': "Zeytinburnu "}, function(results, status)
	// {
	// if (status == google.maps.GeocoderStatus.OK) {
	// console.log("map results length : " +results.length );
	// for(var i =0; i<results.length ;i++){
	//
	// var marker = new google.maps.Marker({
	// map: map,
	// position: results[0].geometry.location
	// });
	// map.setCenter(marker.getPosition());
	// var infoWindow = new google.maps.InfoWindow({
	// content: ruhsatAdres
	// });
	// marker.addListener('click', function() {
	// infoWindow.open(marker.get('map'), marker);
	// });
	// }
	// console.log('map succesfully initalized!');
	// } else {
	// // alert('Adres bulunamadi. : ' + searchingAdress);
	// console.log('Geocode aranan adres bulunamadi! Durum= ' + status);
	// }
	// });

	// }else{

	// alert("Ruhsat kaydinin adres bilgisi bulunmamaktadir.");

	// }
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var geolocate = new google.maps.LatLng(position.coords.latitude,
					position.coords.longitude);
			var marker = new google.maps.Marker({
				map : map,
				position : geolocate,
				icon : "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
			});
			var infoWindow = new google.maps.InfoWindow({
				content : "Buradasiniz"
			});
			marker.addListener('click', function() {
				infoWindow.open(marker.get('map'), marker);
			});
			map.setCenter(marker.getPosition());
		}, function(error) {

			console.log("failedGeolocation");
			console.log(error);
		});
	}

	$('#map-canvas').css('height', getMapPageHeight() - 100);
//	$('#map-canvas').css('width', getMapPageWidth() - 10);

	


} 

// $('map-page').on("pageshow", function() {
// var latLng = map.getCenter();
// google.maps.event.trigger(map,'resize');
// map.setCenter(latLng);
// });

function getMapPageHeight() {
	return $(window).height();
}

function getMapPageWidth() {
	return $(".page-content").width();
}

$(document).on('click', '.clientLocations', function(event) {

	console.log("clientLocations button clicked.");

	addUserMarkersToMap();

});

$(document).on('click', '.deleteMarkers', function(event) {

	console.log("deleteMarkers button clicked.");

});

function addUserMarkersToMap() {

	var e = document.getElementById("select-user");
	var userId = e.options[e.selectedIndex].value;
	console.log(userId);

	// var requestUrl =
	// "http://10.100.1.93:8080/MobilSahaWS/locationService/getClientLocation/"+userId+"/";
	// console.log("Geolocation request : " + requestUrl);

	$.ajax({
		url : "/depotakip/getUserLocation",
		type : "POST",
		data : {
			userId : userId
		},
		error : function(xhr, ajaxOptions, thrownError) {
			console.log("Send location to server Error : " + thrownError);
		},
		success : function(location) {
			console.log(location);
			var jObj = JSON.parse(location);
			console.log(jObj.latitude);
			addClientMarkerToMap(jObj);
			// TODO location infos add map
			// var results = location;
			//	           
			// for(var k in results) {
			// addClientMarkerToMap(results[k]);
			// }

		},
		complete : function(event, request, settings) {
			console.log("Ajax service call completed for geolocation.");
		},
	// timeout: 3000 // sets timeout to 3 seconds
	});

	return false;
}

function addClientMarkerToMap(location) {

	var geolocate = new google.maps.LatLng(location.latitude,
			location.longitude);
	var marker = new google.maps.Marker({
		map : map,
		position : geolocate,
		icon : "http://maps.google.com/mapfiles/ms/icons/red-dot.png"
	});
	var infoWindow = new google.maps.InfoWindow({
		content : "User : " + location.userName + " <br/>Date : "
				+ location.crDate
	});
	marker.addListener('click', function() {
		infoWindow.open(marker.get('map'), marker);
	});
	map.setCenter(marker.getPosition());
}

google.maps.event.addDomListener(window, 'load', initializeGoogleMap);

//google.maps.event.addDomListener(window, 'resize', initializeGoogleMap);


// google.maps.event.addDomListener(window, 'load', initialize);
