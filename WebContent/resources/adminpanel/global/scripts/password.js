$(document).on('click',
    '.changePassword',
    function(event) {

        $("#changePasswordModal").modal("show");

        document.getElementById("changePasswordText").innerHTML = "Şifrenizi değişikliğini onaylıyormusunuz?";

    });
$(document).on('click', '#changePasswordButton', function(event) {
    console.log("changePasswordButton button clicked.");

    // var e = document.getElementById("personalSelect");
    // var userId = e.options[e.selectedIndex].value;

    var oldpassword = $("#oldpassword").val();
    var newpassword = $("#newpassword").val();
    var renewpassword = $("#renewpassword").val();

    $.ajax({
        type: "POST",
        url: "/depotakip/changePassword",
        data: {
            oldpassword: oldpassword,
            newpassword: newpassword,
            renewpassword: renewpassword
        },
        success: function(data) {
            // console.log(data);
            if (data == "Success") {
                Metronic.alert({
                    container: "#deneme", // alerts parent container(by
                    // default
                    // placed after the page breadcrumbs)
                    place: "prepent", // append or prepent in container
                    type: "Success", // alert's type
                    message: "Şifreniz başarıyla değiştirilmiştir.", // alert's
                    // message
                    close: 1, // make alert closable
                    reset: 1, // close all previouse alerts first
                    focus: 1, // auto scroll to the alert after shown
                    closeInSeconds: 10, // auto close after defined seconds
                    icon: "check" // put icon before the message
                });

            } else {
                Metronic.alert({
                    container: "#deneme", // alerts parent container(by
                    // default
                    // placed after the page breadcrumbs)
                    place: "prepent", // append or prepent in container
                    type: "Danger", // alert's type
                    message: "Şifreniz Değiştirilemedi.", // alert's message
                    close: 1, // make alert closable
                    reset: 1, // close all previouse alerts first
                    focus: 1, // auto scroll to the alert after shown
                    closeInSeconds: 10, // auto close after defined seconds
                    icon: "warning" // put icon before the message
                });
            }

        },
        error: function(textStatus) {
            alert("Something really bad happened " + textStatus);
            console.log(textStatus);

        },
        complete: function(jqXHR, textStatus) {

        }

    });

    // $("#sendPushNotificationModal").modal("show");

});