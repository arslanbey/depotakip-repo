$(document).on(
		'click',
		'#tableuserreport',
		function(event) {
			console.log("reloada tıklandı");

			// var e = document.getElementById("personalSelect");
			// var userId = e.options[e.selectedIndex].value;

			// var message = $("#pushNotificationMessage").val();

			$.ajax({
				type : "GET",
				url : "/depotakip/userReport",
				data : {},
				success : function(data, textStatus, jqXHR) {

					console.log(data);
					$("#tbodyuserreport").empty();
					var jObj = JSON.parse(data);
					console.log(jObj);

					for (var i = 0; i < jObj.length; i++) {
						var htmltable = "";
						if ((i % 2) == 0) {

							htmltable += "<tr id=" + jObj[i].id
									+ " \"\ class=\"odd\" > ";
						} else {
							htmltable += "<tr id=" + jObj[i].id
									+ " \"\ class=\"even\" > ";
						}
						htmltable += "<td>" + (i+1) +" </td>";
						htmltable += "<td>" + jObj[i].userName + " </td>";
						htmltable += "<td>" + jObj[i].name + " "+ jObj[i].surName +"</td>";
						
						if (jObj[i].roleId == 1) {
							htmltable += "<td>Yönetici</td>";
						} else if (jObj[i].roleId == 2) {
							htmltable += "<td>Kullanıcı</td>";
						} 
						htmltable += "<td>" + jObj[i].lastLoginDate + "</td>";

						$("#tbodyuserreport").append(htmltable);
					}

				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert("Something really bad happened " + textStatus);

				},
				complete : function(jqXHR, textStatus) {

				}

			});

			// $("#sendPushNotificationModal").modal("show");

		});
